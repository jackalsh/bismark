TARGET = bismark

SOURCES = $(wildcard src/*.cpp)
CSOURCES = $(wildcard src/*.c)

OBJECTS = $(SOURCES:%.cpp=%.o)
OBJECTS += $(CSOURCES:%.c=%.o)

### ARCHITECTURE, disable this, if your processor don't have this instruction sets
#ARCHFLAGS = -mmmx -msse -msse2

### OPTIMIZATIONS, enable if you want fast version
OPTIMIZATIONFLAGS = -Ofast -funroll-loops -fschedule-insns2 -fexpensive-optimizations -frerun-cse-after-loop -fthread-jumps -fstrength-reduce -ffast-math -finline-functions -fomit-frame-pointer -fforce-addr -falign-functions -falign-loops -falign-labels -falign-jumps

### WARNINGS, disable if you don't want see it
WARNINGFLAGS = -Wextra

### DEBUG, enable if you want
#DEBUGFLAGS = -gd

SIMILARFLAGS = $(OPTIMIZATIONFLAGS) $(ARCHFLAGS) $(WARNINGFLAGS) $(DEBUGFLAGS)

CFLAGS = $(SIMILARFLAGS)
CXXFLAGS = $(SIMILARFLAGS)

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) -o $(TARGET) $(OBJECTS)

clean: $(OBJECTS)
	$(RM) $(OBJECTS)

.PHONY: all clean

