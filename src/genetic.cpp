#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
using namespace std;

#include <time.h>

#include "uci_protocol.h"
#include "board.h"
#include "epd_container.h"
#include "genetic.h"

using namespace genetic;

/////////////////////////////////////genetic_eval_data/////////////////////////////////////////

long int genetic_eval_data::unique_id_counter = 0;

genetic_eval_data::genetic_eval_data():eval_data(),id(++unique_id_counter)
{
	set_ELO(0);
	set_privileged_status( eval_type_pair_uint (ordinary,0) );
	set_initial_status(false);
}

genetic_eval_data::genetic_eval_data(const eval_data& arg):eval_data(arg),id(++unique_id_counter)
{
	set_ELO(0);
	set_privileged_status( eval_type_pair_uint (ordinary,0) );
	set_initial_status(false);
}

genetic_eval_data::genetic_eval_data(const genetic_eval_data& arg):eval_data(arg),id(++unique_id_counter)
{
	set_ELO(0);
	set_privileged_status( eval_type_pair_uint (ordinary,0) );
	set_initial_status(false);
}

void genetic_eval_data::load_to_board(board& board_arg)
{
	board_arg.set_eval_data( *(this));
}

void genetic_eval_data::set_id(long id_arg)
{ 
	id = id_arg; 
}

long genetic_eval_data::get_id() const 
{ 
	return id; 
}

void genetic_eval_data::set_ELO(int val) 
{ 
	ELO = val;
}

void genetic_eval_data::add_to_ELO(int add) 
{
	ELO += add;
}

int genetic_eval_data::get_ELO() const 
{
	return ELO;
}

void genetic_eval_data::set_privileged_status(const eval_type_pair_uint &status)
{
	priveleged_status = status;
}

const eval_type_pair_uint& genetic_eval_data::get_privileged_status()
{
	return priveleged_status;
}

void genetic_eval_data::set_initial_status(bool is_initial_arg)
{
	is_initial = is_initial_arg;
}

bool genetic_eval_data::get_initial_status() const
{
	return is_initial;
}

////////////////////algorithms////////////////////////////////////////////////////////////////

bool genetic::operator < (const genetic_eval_data& left,const genetic_eval_data& right)
{
  return(left.get_ELO() < right.get_ELO());
}

void genetic::score_victory(genetic_eval_data& winner,genetic_eval_data& looser )
{
	float difR = float(winner.get_ELO()-looser.get_ELO());
	float Ew = 1.0F/(1.0F + pow(10.0F,difR/T));
	float El = 1.0F - Ew;
	winner.add_to_ELO(int(K*(1.0F-Ew)));
	looser.add_to_ELO(int(K*(0.0F-El)));
}

void genetic::score_draw(genetic_eval_data& first,genetic_eval_data& second)
{
	float difR = float(first.get_ELO()-second.get_ELO());
	float Ef = 1.0F/(1.0F + pow(10.0F,difR/T));
	float Es = 1.0F - Ef;
	first.add_to_ELO(int(K*(0.5F-Ef)));
	second.add_to_ELO(int(K*(0.5F-Es)));
}

bool genetic::operator < (const shared_eval_data& first,const shared_eval_data& second)
{
	return(first->get_ELO() < second->get_ELO());
}

////////////////////////////////tournament///////////////////////////////////////////////

tournament::tournament(board& board_arg,const shared_eval_data& judge_arg):board_ptr(&board_arg)
{
	set_margin_win(Default_Margin_Win);
	set_max_game_len(Default_Max_Game_Len);
	set_depth_search(Default_Depth_Search);
	set_num_of_rounds(1);
	set_start_FEN(start_position_FEN);

	set_judge(judge_arg);
}

void tournament::add_engine(const shared_eval_data& engine_arg)
{
	assert(engine_arg);
	engines_list.push_back(engine_arg);
}

void tournament::add_engine(const list<shared_eval_data>& engines_arg)
{
	std::list<shared_eval_data>::const_iterator it;
	for(it = engines_arg.begin(); it != engines_arg.end(); ++it)
	{
		assert(*it);
		engines_list.push_back(*it);
	}
}

void tournament::set_judge(const shared_eval_data& judge_arg)
{
	assert(judge_arg);
	judge = judge_arg;
}

shared_eval_data tournament::get_judge()
{
	assert(judge);
	return judge;
}

void tournament::set_margin_win(int margin_arg)
{
	assert(margin_arg >= 0); //else engine wins if he loses on material
	margin_win = margin_arg;
}

int tournament::get_margin_win() const
{
	return margin_win;
}

void tournament::set_max_game_len(unsigned int full_moves)
{
	max_game_len = full_moves;
}

unsigned int tournament::get_max_game_len() const
{
	return max_game_len;
}

void tournament::set_depth_search(unsigned int half_moves)
{
	depth_search = half_moves;
}

unsigned int tournament::get_depth_search() const
{
	return depth_search;
}

void tournament::set_num_of_rounds(unsigned int rounds)
{
	num_of_rounds = rounds;
}

unsigned int tournament::get_num_of_rounds() const
{
	return num_of_rounds;
}

void tournament::set_start_FEN(const string& fen)
{
	start_FEN = fen;
}

string tournament::get_start_FEN() const
{
	return start_FEN;
}

bool tournament::is_good()
{
	if(!judge)
	{
		cout << "error in tournament initialization: judge engine is corrupted" << endl;
		return false;
	}
	std::list<shared_eval_data>::iterator it;
	for(it = engines_list.begin(); it != engines_list.end(); ++it)
		if(!(*it)) 
		{
			cout << "error in tournament initialization: one of engines is corrupted" << endl;
			return false;
		}
	return true;
}

bool tournament::init_play()
{
	if(!is_good()) 
	{
		cout << "error in tournament initialization: failed" << endl;
		return false;
	}
	std::list<shared_eval_data>::iterator it;
	for(it = engines_list.begin(); it != engines_list.end(); ++it)
	{
		(*it)->set_ELO(genetic::Initial_ELO_rating);
	}
	return true;
}

void tournament::remove_all_engines()
{
	engines_list.clear();
}

void tournament::play()
{
	if(!init_play()) 
	{
		cout << "error: tournament is failed to initialize" << endl;
		return;
	}

	std::list<shared_eval_data>::iterator it_1,it_2;
	unsigned int game_count = 0;
	unsigned int total_games = engines_list.size() * engines_list.size()*get_num_of_rounds();

	for(unsigned int round_index = 1; round_index <= get_num_of_rounds(); ++round_index)
	{
		for(it_1 = engines_list.begin(); it_1 != engines_list.end(); ++it_1)
			for(it_2 = engines_list.begin(); it_2 != engines_list.end(); ++it_2)
			{
				if( (game_count%10) == 0 )
					cout << game_count << "/" << total_games << " (" << (float(game_count)/float(total_games))*100 << "%)" << endl;
				two_engine_battle(*it_1,*it_2);
				++game_count;
			}
	}
}

void tournament::two_engine_battle(shared_eval_data& first,shared_eval_data& second)
{
	assert( first && second );
	int first_score,second_score,judge_score;

	board_ptr->init_game_FEN(get_start_FEN());
	for(unsigned int move_index = 0 ; move_index < get_max_game_len() ; ++move_index)
	{
		first->load_to_board(*board_ptr);
		first_score = board_ptr->fw_search_and_move(false,depth_search,0);
		second->load_to_board(*board_ptr);
		second_score = board_ptr->fw_search_and_move(false,depth_search,0);

		if( (move_index%5)==0 || board_ptr->is_score_checkmate(first_score) || board_ptr->is_score_checkmate(second_score))
		{ 
			board_ptr->set_eval_data(*judge);
			judge_score = board_ptr->fw_search(false,1,0); //it's enough to do 1 depth search to check for checkmate 
			//and recaptures till the end with quiescence search
			//for now it's turn of first player
			if(judge_score > get_margin_win())
			{
				//game.to_console();
				genetic::score_victory(*first,*second);
				return;
			}
			else if(judge_score < - margin_win)
			{
				//game.to_console();
				genetic::score_victory(*second,*first);
				return;
			}
		}
	}

	//unsound situation - draw
	genetic::score_draw(*first,*second);
}

list<shared_eval_data> tournament::get_best(unsigned int num)
{
 assert(num <= engines_list.size());

 engines_list.sort();    //in ascending order by default
 engines_list.reverse(); //in descending order

 list<shared_eval_data> ret_list;
 std::list<shared_eval_data>::iterator it;
 unsigned int count = 0;
 for(it = engines_list.begin(); it != engines_list.end() && count < num; ++it)
 {
	 ret_list.push_back(*it);
	 count ++;
 }
 return ret_list;
}

void tournament::save_log()
{
	ofstream log;
	log.open(log_filename.c_str());
	if(!log)
	{
		cout <<"IO error! Can't open log file for saving";
		return ;
	}

	engines_list.sort();
	engines_list.reverse();
	list<shared_eval_data>::iterator it;
	unsigned int index = 1;
	for(it = engines_list.begin();it != engines_list.end();++it)
	{
		log <<  index << ". id: " << (*it)->get_id()  << ", : ELO: " << (*it)->get_ELO() << endl;
		++index ;
	}
	log.close();
}

///////////////////////////////evolution//////////////////////////////////////////////////

evolution::evolution(board& board_arg)
{
	//randomizing mutation for auto tuning. It allows use the same process independly having
	//couple of instances of program running.
	init_genrand64(time(NULL));

	board_ptr = &board_arg;
	set_num_of_privileged(Default_Priveleged_Num);
	set_num_of_mutated(Default_Mutated_Num);
	set_margin_win(Default_Margin_Win);
	set_epd_usage(false);

	//load mutation ranges file
	mutation_ranges_eval.reset(new genetic_eval_data());
	if(mutation_ranges_eval->load_from_configure_file(mutation_ranges_filename) == false)
	{
		cout << "Error: can't load file for mutation ranges at " << mutation_ranges_filename << endl;
		return;
	}

	//load judge eval file
	judge_eval.reset(new genetic_eval_data());
	if(judge_eval->load_from_configure_file(judge_eval_filename) == false)
	{
		cout << "Error: can't load file for judge evaluation at " << judge_eval_filename << endl;
		return;
	}

	//register new engines
	register_engine(mutation_ranges_eval);
	register_engine(judge_eval);
}

bool evolution::register_engine(const shared_eval_data& engine_arg)
{
	assert(engine_arg);
	if(engine_arg)
	{
		total_engines.push_back(engine_arg);
		return true;
	}
	return false;
}

void evolution::set_judge(shared_eval_data judge_eval_arg)
{
	assert(judge_eval_arg);
	judge_eval = judge_eval_arg;
}

shared_eval_data evolution::get_judge()
{
	assert(judge_eval);
	return judge_eval;
}

void evolution::set_mutation_ranges(const shared_eval_data& mutation_ranges_arg )
{
	assert(mutation_ranges_arg);
	mutation_ranges_eval = mutation_ranges_arg;
}

shared_eval_data evolution::get_mutation_ranges()
{
	assert(mutation_ranges_eval);
	return mutation_ranges_eval;
}

void evolution::set_num_of_privileged(unsigned int num)
{
	priveleged_num = num;
}

unsigned int evolution::get_num_of_privileged() const
{
	return priveleged_num;
}

void evolution::set_num_of_mutated(unsigned int num)
{
	mutated_num = num;
}

unsigned int evolution::get_num_of_mutated() const
{
	return mutated_num;
}

void evolution::set_margin_win(int margin_arg)
{
	assert(margin_arg >= 0); //else engine wins if he loses on material
	margin_win = margin_arg;
}

int evolution::get_margin_win() const
{
	return margin_win;
}

void evolution::set_epd_usage(bool status)
{
	is_use_epd_flag = status;
}

bool evolution::is_use_epd() const
{
	return is_use_epd_flag;
}

bool evolution::user_input()
{
	unsigned int utemp_val; int temp_val; string temp_str;
	cout << "enter number of privileged engines :" << endl;
	cin >> utemp_val;set_num_of_privileged(utemp_val) ;
	cout << "enter number of clones per engine :" << endl;
	cin >> utemp_val;set_num_of_mutated(utemp_val) ;
	cout << "enter margin (in centipawns) for declaring win/loose in the games" << endl;
	cin >> temp_val;set_margin_win(temp_val) ;
	cout << "do you want use custom initial positions (each one per iteration)? type 'y' for accept" << endl;
	cin >> temp_str; if(temp_str == string("y")) 
		set_epd_usage(true);
	else
		set_epd_usage(false);


	if(is_use_epd())
	{
		cout << "loading EPD file " << epd_filename << " for initial positions" << endl;
		epd.clear();
		if(!epd.load_epd(epd_filename.c_str()) || epd.size() != 0)
		{
			cout << "Error: EPD file " << epd_filename << " can't be loaded" << endl;
			return false;
		}

		//EPD can be contained of groups of positions close to each another or from the same category.
		//We shuffle them to ensure more randomness
		epd.random_shuffle();
	}

	return true;
}

bool evolution::evolution_cycle()
{
	cout << "Initial ELO: " << genetic::Initial_ELO_rating << ", T = " << genetic::T << ",K = " << genetic::K <<endl;
	cout << "the more games the longer each iteration will" << endl << endl;

	cout << "changing hash size for short controls" << endl;
	board_ptr->change_hash_size(DEFAULT_HASH_BITSIZE_SHORT_CONTROLS);

	tournament arena(*board_ptr,judge_eval);

	cout << "first initialize of privileged engines" << endl;
	//declare privileged engines
	shared_eval_data initial_eval(new genetic_eval_data(board_ptr->get_eval_data())); register_engine(initial_eval);
	initial_eval->set_initial_status(true);
	//scored_genetic_eval_data initial_eval(game.get_eval_data());

	list<shared_eval_data> privileged_list;
	for(unsigned int i = 0;i<get_num_of_privileged();++i)
	{
		//we begin our evolution from current engine
		privileged_list.push_back(initial_eval);
	}

	for(unsigned int iteration = 1;;++iteration)
	{
		cout << " starting iteration " << iteration << endl;
		arena.remove_all_engines();

		if(is_use_epd())
		{ //when EPD test is finished we back to it's beginning
			string FEN_str = epd.get((iteration-1)%epd.size()).get_FEN();
			arena.set_start_FEN(FEN_str);
		}

		//
		if(iteration % 2 != 0) //on mutation step
		{
			cout << "mutation step: creating mutations from privileged engines" << endl;
			std::list<shared_eval_data> mutated  = get_mutated_from(privileged_list);
			arena.add_engine(privileged_list);
			arena.add_engine(mutated);
			arena.set_num_of_rounds(1);
		}
		else					//on privileged match
		{
			cout << "privileged step: playing only with privileged engines" << endl;
			std::list<shared_eval_data> all_priviliged  = get_all_privileged();
			arena.add_engine(all_priviliged);
			arena.set_num_of_rounds(10);
		}

		//Adding initial engine
		arena.add_engine(initial_eval);

		cout << "starting playing between engines" << endl;
		arena.play();

		//updating new priveleged list;
		privileged_list = arena.get_best(get_num_of_privileged());

		cout << "saving best engines parameters to autotuning// " << endl;
		unsigned int index_save = 0;

		std::list<shared_eval_data>::const_iterator it;
		for(it = privileged_list.begin();it != privileged_list.end();++it)
		{
			//saving best configuration to file
			string filename = (string("autotuning\\best_conf_") + int_to_string(index_save+1) + string(".txt"));
			(*it)->save_to_configure_file(filename);
			//store(or update) engine's status as privileged on this iteration
			(*it)->set_privileged_status(eval_type_pair_uint(privileged,iteration));
			//check if one of the best engines is initial one - evolution ineffective?
			if((*it)->get_initial_status() == true)
				cout << "initial engine between the best engines - evolution may be ineffective?" << endl;

			++index_save;
		}
		arena.save_log();
		this->save_log();

				//TODO: when we exit?
	}

	//restoring state
	cout << "changing hash size for ordinary controls" << endl;
	board_ptr->change_hash_size(DEFAULT_HASH_BITSIZE);
	board_ptr->set_eval_data(*initial_eval);
}

list<shared_eval_data> evolution::get_all_privileged()
{
	std::list<shared_eval_data> ret_list;
	std::list<shared_eval_data>::const_iterator it;
	for(it = total_engines.begin(); it != total_engines.end(); ++it)
	{
		if( (*it)->get_privileged_status().get_type() == privileged)
			ret_list.push_back(*it);
	}
	return ret_list;
}

list<shared_eval_data> evolution::get_mutated_from(list<shared_eval_data> source)
{
	std::list<shared_eval_data> ret_list;
	std::list<shared_eval_data>::const_iterator it;
	for(it = source.begin();it != source.end();++it)
		for(unsigned int j = 0;j<get_num_of_mutated();++j)
		{
			shared_eval_data temp_mutate(new genetic_eval_data(*(*it))); 
			temp_mutate->mutate(*get_mutation_ranges());
			register_engine(temp_mutate);
			ret_list.push_back(temp_mutate);
		}

	return ret_list;
}

void evolution::save_log()
{
	ofstream log;
	log.open("autotuning\\log_all.txt");
	if(!log)
	{
		cout <<"IO error! Can't open log file for saving";
		return ;
	}

	list<shared_eval_data>::iterator it;
	unsigned int index = 1;
	for(it = total_engines.begin();it != total_engines.end();++it)
	{
		log << setw(4) <<  index << ". id: " << setw(5) << (*it)->get_id()  << ";";
		if((*it)->get_initial_status() == true)
			log << "initial;";
		if((*it)->get_privileged_status().get_type() == privileged)
		{
			log << "privileged-" << (*it)->get_privileged_status().get_value() << ";";
		}
		log << endl;
		++index ;
	}
	log.close();
}