/////////////////////////////////////////////////////////////////////////////
//This file defines constants used throughout the entire project
//You should not change these constants 
//Change them only if clearly understand what it would lead.
//
//This file also declares couple of small classes and functions.
/////////////////////////////////////////////////////////////////////////////

#ifndef BASIC_H
#define	BASIC_H

//#ifndef NDEBUG
//	#define NDEBUG
//#endif
#include <assert.h>

#include <ostream>
#include <string>
#include <stdint.h>

//////INTERNAL CONSTANTS. DON'T CHANGE !!! /////////

#define BOARD_SIZE 8

#define NO_PIECE 0 //00 000

#define PAWN   1   //00 001
#define KNIGHT 2   //00 010
#define BISHOP 3   //00 011
#define ROOK   4   //00 100
#define QUEEN  5   //00 101
#define KING   6   //00 110

#define WHITE 8    //01 000
#define BLACK 16   //10 000

#define KING_SIDE 1
#define QUEEN_SIDE 2

#define COLOR_BITS 24 //11 000
#define DIGNI_BITS 7  //00 111

#define PIECE_ID_MAX 32     //11 111
#define PIECE_DIGNITY_MAX 7 //00 111
#define MAX_TREE_DEPTH 200  //if tree grows beyond this depth will fault
#define MAX_MOVES_FROM_ONE_POSITION 100 //nothing will happen if overridden but will reallocate array - consumes speed

#define BIT uint64_t

#ifndef NULL
    #define NULL _null
#endif

#define ORDINARY 0           //0 00
#define SILENT_ONLY 1        //0 01
#define CAPTURE_ONLY 2       //0 10
#define FOLLOWING_ALLOWED 4  //1 00

#define INFINITY_EVAL 10000000
#define CHECK_INVALID (2*INFINITY_EVAL)

#define MINUS_CHECK_INVALID (-CHECK_INVALID)

#define DEFAULT_HASH_BITSIZE 22  //22 is default ~100 MB 
#define DEFAULT_HASH_BITSIZE_SHORT_CONTROLS 16

/////declaration of boolean flags and macroses to work with different modes of search/////
#define INITIAL 0               //00000
#define NULL_MOVE_WINDOW 1      //00001
#define PVS_NULL_WINDOW  2      //00010
#define NULL_MOVE_ROOT   4      //00100

//check if this node is ordinary
#define IS_PV_NODE(value)   ((value & (NULL_MOVE_WINDOW | PVS_NULL_WINDOW)) == 0)
//check if this node is null window verification of null move search
#define IS_NULL_MOVE(value) ( (value & NULL_MOVE_WINDOW)!=0)
//check if this node is null window verification of PVS search
#define IS_PVS_NULL(value)  ( (value & PVS_NULL_WINDOW)!=0)
#define IS_NULL_MOVE_ROOT(value) ( (value & NULL_MOVE_ROOT)!=0 )
//#define IS_NULL_SEARCH(value) ( (value & (NULL_MOVE_WINDOW | PVS_NULL_WINDOW)) != 0 )

#define MAKE_NULL_MOVE(value) (value | (NULL_MOVE_WINDOW | NULL_MOVE_ROOT))
#define DISABLE_NULL_MOVE_ROOT(value) (value = (value & (~NULL_MOVE_ROOT)))
#define MAKE_PVS_NULL(value) (value | PVS_NULL_WINDOW)

const BIT bit_files[8] = { 72340172838076673 ,   // h-file
						   144680345676153346U ,  // g-file
						   289360691352306692U ,  // f-file
						   578721382704613384U ,  // e-file
						   1157442765409226768U , // d-file
						   2314885530818453536U , // c-file
						   4629771061636907072U , // b-file
						   9259542123273814144U };// a-file

//all bites upper than row N are ones, otherwise 0
const BIT up_bits[8] =  {  18446744073709551360U ,
					       18446744073709486080U ,
						   18446744073692774400U ,
						   18446744069414584320U ,
						   18446742974197923840U ,
						   18446462598732840960U ,
						   18374686479671623680U ,
						   0U                      };

const BIT down_bits[8] = { 0U                    ,
	                       255U                  ,
						   65535U                ,
						   16777215U             ,
						   4294967295U           ,
						   1099511627775U        ,
						   281474976710655U      ,
						   72057594037927935U      };

//some constants are too heavy to initialize directly but easy calculated
//so we prefer to initialize them once(INIT()) and use everywhere
extern BIT outpost_side_bits[BOARD_SIZE][BOARD_SIZE][2]; //0-WHITE;1-BLACK
extern BIT outpost_front_bits[BOARD_SIZE][BOARD_SIZE][2];//0-WHITE;1-BLACK
//absolute piece's weights
extern int absolute_piece_values[PIECE_ID_MAX];
void init();




//name for evaluation file on default
const std::string default_config_filename = "config_tmp.txt";
//name for user custom evaluation file. 
//If this file is found it loads to engine over the default configuration.
const std::string default_custom_eval_filename = "autotuning//custom.txt";

/////////////////////////////////////////////////////////////////////////////
//this is basic class to store and manipulate with coordinates on
//the board. This is slow class because of computing bitboard
//on construction. Avoid creating coords in search algorithms and
//use precompilated values
/////////////////////////////////////////////////////////////////////////////


class coord
{ 
public:
	int x; // "a"=7 "b"=6 "c"=5 "d"=4 "e"=3 "f"=2 "g"=1 "h"=0
	int y; // "1"=0 "2"=1 "3"=2 "4"=3 "5"=4 "6"=5 "7"=6 "8"=7
	BIT bit;
        //we store coord as (x,y) on chess board and also as bitboard format
        //for fast bit operations. Because all coords will be generated on
        //program start, it will not slow down the algorithms

	coord();
	coord(int arg_x,int arg_y);
	void to_console();
};

std::ostream &operator<<(std::ostream &cout,coord &obj);

/////////////////////////////////////////////////////////////////////////////
//using for identification of castle moves and present all information
//needed about castling.
/////////////////////////////////////////////////////////////////////////////

class castling
{
public:
	castling(coord *rook_from_arg,coord *rook_to_arg,BIT attack_restr_arg,BIT move_restr_arg);
	//this bit holds bitspaces of squares if attacked by enemy spaces makes all castling impossible
	//we include FROM bit of king castling in this group and TO bit as well(unnecessary, but accomplish generality)
	BIT attack_restr_bit;
	//this bit holds bitspaces of squares if occupied with any pieces makes all castling impossible
	BIT move_restr_bit;
	//coord of rook before castling
	coord *rook_from;
	//coord of rook after castling
	coord *rook_to;  
};


/////////////////////////////////////////////////////////////////////////////
//basic class to store moves. FROM,TO are coordinates of move.
//bit silent_only is equal to->bit if this move is silent only
//bit capture_only is equal to->bit if this move is capture only
//castle_info differs from NULL only if this move is castling
//with information castle_info points about
/////////////////////////////////////////////////////////////////////////////

class move_el
{
public:
	coord * from;
	coord *to;
	move_el(coord* from_m,coord* to_m,int mode = ORDINARY);
    //if this move is silent only and can not be capture(pawn forward moves,enpassan) holds TO position
    BIT silent_only;
	//if this move is capture only move(pawn moves like e2d3)
    BIT capture_only;
	castling *castle_info;
	//if this move is pawn's double move holds TO coord. NULL on else
	coord* pawn_double_move; 
	// if enpassan move holds coord for capturing piece. NULL on else
	coord* enpassan_info; 
	//dignity of promoted piece or NO_PIECE if it's not a promoting move
	int pawn_promoting; 
	//if enabled, we enable mechanism of following piece for moving at least once
	//it's needed for castling legality checking - castle is disabled if king or rook moved before
	//so this mechanism must be enabled for them only. For any other piece this mechanism is ignored
	bool is_enable_move_following; 

};


std::ostream &operator<<(std::ostream &out,move_el &obj);

//checks if those moves are equal. Only from-to squares are checked! And not other bits
bool is_equal(move_el* first,move_el* second);

/////////////////////////////////////////////////////////////////////////////
//this class is interface around move_el with additional information of 
//scoring this move.
/////////////////////////////////////////////////////////////////////////////

class scored_move_el
{
private:
    move_el *p_m;
    int score;
public:
    scored_move_el(){set_move_el(NULL);set_score(0);}
    scored_move_el(move_el* p_m_arg){set_move_el(p_m_arg);set_score(0);} //we don't initialize SCORE for performance ??don't change!?
    scored_move_el(move_el* p_m_arg,int score_arg){set_move_el(p_m_arg);set_score(score_arg);}

    int get_score() const {return score;}
    void set_score(int score_arg) {score = score_arg;}

    move_el* get_move_el(){return p_m;}    
    void set_move_el(move_el* p_m_arg) {p_m = p_m_arg;}
};

bool comparison_by_score_bigger(const scored_move_el& left,const scored_move_el& right);
bool comparison_by_score_lower(const scored_move_el& left,const scored_move_el& right);


/////////////////////////////////////////////////////////////////////////////
//others mainly common functions
/////////////////////////////////////////////////////////////////////////////
void bit_to_console(BIT bit); //TODO: this is obsolete function no more used in the program.

//gives random number in the range [-range,-range+1,...,0,...,range-1,range]
int random_pos_neg_range(unsigned int range);

////////////////work with strings////////////////////////////////////////////
int string_to_int(std::string str);
std::string int_to_string(int num);
int get_N_of_words(std::string str);
std::string get_Nth_word(std::string str,int N);

bool is_file(char ch);
unsigned int letter_to_file_index(char ch);
bool is_rank(char ch);
unsigned int letter_to_rank_index(char ch);
char index_to_file_letter(int file_index);
char index_to_rank_letter(int rank_index);

void remove_spaces_from(std::string& str);

#endif	/* BASIC_H */

