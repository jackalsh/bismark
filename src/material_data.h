#ifndef _MATERIAL_DATA
#define _MATERIAL_DATA

//king is not a part of the material

class material_data
{
private:
	int white_material;
	int black_material;
public:
	material_data();

	void add_to_white(int addition) {white_material+=addition;}
	void add_to_black(int addition) {black_material+=addition;}
	int get_white() { return white_material; }
	int get_black() { return black_material; }

	void clear();
};

#endif