#include <iostream>
using namespace std;
#include <assert.h>

#include "principal_variation.h"

line::line()
{
	lin.reserve(MAX_TREE_DEPTH);
	score = 0;
	iteration = 0;
}

move_el* line::get_first_move()
{
	if(!lin.empty() && lin[0]!=NULL)
		return lin[0];
	else
		return NULL;
}

void line::to_console()
{
	for(unsigned int i = 0;i<lin.size();++i)
		cout << *(lin[i]) << " ";
	cout <<endl;
}


///////////////////////////////////////////////////////////////////

principal_variation::principal_variation()
{
	clear();
}

void principal_variation::clear()
{
	for(unsigned int i = 0;i<MAX_TREE_DEPTH;i++)
	{
		for(unsigned int j = 0;j<MAX_TREE_DEPTH;j++)
		{
			pv_mat[i][j] = NULL;
		}
		pv_len[i] = 0;
	}

	set_root_store_depth(0);
	clear_followed();
	best_container.clear();
}

void principal_variation::init_new_search(unsigned int root_store_depth_arg)
{
	clear();
	set_root_store_depth(root_store_depth_arg);
}

void principal_variation::set_root_store_depth(unsigned int root_store_depth_arg)
{
	root_store_depth = root_store_depth_arg;
}

unsigned int principal_variation::get_root_store_depth() const
{
	return root_store_depth;
}

void principal_variation::store(unsigned int ply,move_el* move)
{
	assert(ply < MAX_TREE_DEPTH && ply >= get_root_store_depth());

	pv_mat[ply][ply] = move;
	for (unsigned int i = ply + 1; i < pv_len[ply + 1]; ++i)
		pv_mat[ply][i] = pv_mat[ply + 1][i];
	pv_len[ply] = pv_len[ply+1];
}

void principal_variation::init_at_node(unsigned int ply)
{
	assert(ply < MAX_TREE_DEPTH && ply >= get_root_store_depth());

	pv_len[ply] = ply;
}

void principal_variation::to_console_current()
{
	unsigned int root_depth = get_root_store_depth();
	for(unsigned int i = root_depth;i<pv_len[root_depth];++i)
	{
		assert(pv_mat[root_depth][i] != NULL);
		cout << *(pv_mat[root_depth][i]) << " ";
	}
	cout <<endl;
}


void principal_variation::add_to_container(int score_arg, int iteration_arg)
{
	line temp_line;
	unsigned int root_depth = get_root_store_depth();
	for(unsigned int i = root_depth;i<pv_len[root_depth];++i)
	{
		assert(pv_mat[root_depth][i] != NULL);
		temp_line.lin.push_back(pv_mat[root_depth][i]);
	}
	temp_line.score = score_arg;
	temp_line.iteration = iteration_arg;

	best_container.push_back(temp_line);
}

bool principal_variation::is_any_lines() 
{
	return (!best_container.empty());
}

unsigned int principal_variation::get_num_of_lines() 
{
	return (best_container.size());
}

line principal_variation::get_last_line()
{
	if(is_any_lines())
	  return best_container[get_num_of_lines()-1];
	else
	  return line();
}

line principal_variation::get(unsigned int index)
{
	assert(is_any_lines());
	assert(index<get_num_of_lines() );

	return best_container[index];
}

void principal_variation::clear_followed()
{
	for(unsigned int i = 0;i<MAX_TREE_DEPTH;++i)
		pv_follow[i] = NULL;
}

void principal_variation::set_followed(const line& line_arg)
{
	clear_followed();

	vector<move_el*>::const_iterator it;
	unsigned int index = get_root_store_depth();
	for(it = line_arg.lin.begin();it!=line_arg.lin.end();++it,++index)
	{
		assert( (*it) != NULL && index < MAX_TREE_DEPTH);
		pv_follow[index] = *it;
	}
}

bool principal_variation::compare_with_followed(unsigned int ply,scored_move_el& move)
{
	assert(ply < MAX_TREE_DEPTH && ply >= get_root_store_depth());

	if(pv_follow[ply]==NULL) return false;
	if(pv_follow[ply]->from->bit == move.get_move_el()->from->bit &&
	   pv_follow[ply]->to->bit == move.get_move_el()->to->bit )
	        return true;
	return false;
}