
#ifndef _CONTROL_GAME
#define _CONTROL_GAME

#define INFINITE_SEARCH 0

//////////////////////////////////////////////////////////////////////////////
//Handles time control of the engine 
//////////////////////////////////////////////////////////////////////////////
class control
{
private:
	unsigned int begin_time;
	unsigned int end_think_time;
	unsigned int nods_searched;
    bool stop_now_flag ;

    unsigned int get_ms();

	bool communication_search_mode;
public:
	control();

	void init_new_search(unsigned int ms = INFINITE_SEARCH); //0 is infinite
	void add_to_nods(unsigned int nods_arg){nods_searched+=nods_arg;}
	void dec_from_nods(unsigned int nods_arg){nods_searched-=nods_arg;}
	bool is_stop_alerted(){return stop_now_flag;}
	void stop_the_search(){stop_now_flag = true;}
	unsigned int get_nods_searched(){return nods_searched;}
	unsigned int get_nods_per_second();
	unsigned int get_time_from_begin();

	//in communication mode engine don't communicate with GUI
	void disable_communication_mode();
	void enable_communication_mode();
	bool is_communication_mode_enabled();

	void checkup();
};

#endif