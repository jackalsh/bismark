
#include <algorithm>
#include <assert.h>
#include <iostream>
#include "move_storage.h"


////////////////////////////////////////////////////////////////////////////////////

move_storage::move_storage()
{
    moves_d_size = MAX_TREE_DEPTH;
    reserved_for_moves = MAX_MOVES_FROM_ONE_POSITION;
    
    moves_d.resize(moves_d_size);
    for(unsigned int i = 0;i<moves_d.size();i++)
        moves_d[i].reserve(reserved_for_moves);

	hist_d.resize(moves_d_size);

    total_clear_data();
}

void move_storage::set_store_depth(unsigned int depth)
{
    assert(depth<moves_d_size);
    cur_depth = depth;
}

void move_storage::inc_store_depth()
{
	assert(cur_depth+1<moves_d_size);
	cur_depth++;
}

void move_storage::dec_store_depth()
{
	assert(cur_depth>0);
	cur_depth--;
}

unsigned int move_storage::get_store_depth()
{
    assert(cur_depth<moves_d_size);
    return cur_depth;
}

void move_storage::set_root_depth(unsigned int depth)
{
	assert(depth<moves_d_size);
	root_depth = depth;
}

void move_storage::set_this_depth_as_root()
{
	assert(cur_depth<moves_d_size);
	set_root_depth(get_store_depth());
}

unsigned int move_storage::get_root_depth() const
{
	assert(root_depth<moves_d_size);
	return root_depth;
}

void move_storage::add_move(move_el* el)
{
    assert(cur_depth<moves_d_size);
    assert(moves_d[cur_depth].size()<reserved_for_moves);
    moves_d[cur_depth].push_back(el);//el here converted to scored_move_el by copy constructor
}

void move_storage::add_move(move_el* el,int score_arg)
{
    assert(cur_depth<moves_d_size);
    assert(moves_d[cur_depth].size()<reserved_for_moves);
    moves_d[cur_depth].push_back(scored_move_el(el,score_arg));
}

scored_move_el& move_storage::get(unsigned int index)
{
    assert(cur_depth<moves_d_size);
    assert(index<moves_d[cur_depth].size());
    return moves_d[cur_depth][index];
}

unsigned int move_storage::num_moves_this_depth()
{
    assert(cur_depth<moves_d_size);
    return moves_d.at(cur_depth).size();
}

void move_storage::clear_stored()
{
  assert(cur_depth<moves_d_size);
  moves_d.at(cur_depth).clear(); 
}

///////////////////////////////////////////////////////////////////////////////////////////
//this function deletes all stored data from last search
///////////////////////////////////////////////////////////////////////////////////////////
void move_storage::total_clear_data()
{
	for(unsigned int i = 0;i<moves_d_size;i++)
	{
		set_store_depth(i);
		clear_stored();
		hist_d[i] = hist_el();
	}
	set_store_depth(0);
	set_root_depth(0);
	alert_check = false;
}

void move_storage::enable_alert_check()
{
    alert_check = true; 
}

void move_storage::disable_alert_check()
{
    alert_check = false;
}

bool move_storage::is_alert_checked()
{
    return alert_check;
}

void move_storage::sort_current_depth()
{
  assert(cur_depth<moves_d_size);
  sort(moves_d[cur_depth].begin(),moves_d[cur_depth].end(),comparison_by_score_bigger);
}

hist_el& move_storage::get_hist()
{
    assert(cur_depth<moves_d_size);
	return hist_d[cur_depth];
}

hist_el& move_storage::get_hist_previous()
{
	assert(cur_depth<moves_d_size && cur_depth!=0);
	return hist_d[cur_depth-1];
}

hist_el& move_storage::get_hist_next()
{
	assert(cur_depth<moves_d_size-1);
	return hist_d[cur_depth+1];
}

bool move_storage::is_hash_reps()
{
	assert(cur_depth<moves_d_size);
	BIT cur_hash = hist_d[cur_depth].get_hash();
	for(unsigned int i = hist_d[cur_depth].get_fifty();i<cur_depth;++i)
	{
		if(hist_d[i].get_hash() == cur_hash)
			return true;
	}
	return false;
}

void hist_el::to_console()
{
	cout << "hash: " << get_hash() << endl;
	cout << "fifty: " << get_fifty() << endl;
	if(get_capture() != NULL)
	{
		cout << "piece " ;
		get_capture()->to_console();
		cout << " is captured captured" << endl;
	}
	if(is_moved_at_past())
		cout << "piece moved at past" << endl;
	else
		cout << "piece don't moved at past" << endl;

	if(get_double_pawn_move() != NULL)
	{
		cout << "pawn move enabled enpassan on " << get_double_pawn_move() ;
	}
	bit_to_console(get_restriction_bit());  cout << endl;

	cout << "--------------------------" << endl;
}

void move_storage::to_console()
{
	cout << "internal data in move storage module:" << endl;
	cout << "current ply depth is " << get_store_depth() << endl;
	for(unsigned int i = 0; i <= get_store_depth(); ++i )
	{
		cout << "--------------------------" << endl;
		cout << "depth : " << i << endl;
		hist_d[i].to_console();
	}
}