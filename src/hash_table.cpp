#include <iostream>

#include "hash_table.h"


////////////////////////////////////////////////////////////////////////////////////////////

hash_table::hash_table()
{
	//initializes random generator;
	init_genrand64(436346436346346);

	//initialize hash array
	for(unsigned int id = 0;id<PIECE_ID_MAX;id++)
		for(unsigned int pos_y = 0;pos_y<BOARD_SIZE;pos_y++)
		   for(unsigned int pos_x = 0;pos_x<BOARD_SIZE;pos_x++)
			  for(unsigned int col = 0;col<2;col++)
				{
					random_table[id][pos_y][pos_x][col] = get_random();
				}

	black_turn_random = get_random();

	for(unsigned int pos_y = 0;pos_y<BOARD_SIZE;pos_y++)
		for(unsigned int pos_x = 0;pos_x<BOARD_SIZE;pos_x++)
			for(unsigned int digni = NO_PIECE; digni<PIECE_DIGNITY_MAX;++digni)
			{
				if(pos_y == 7) //inverse white queen with white pawn
				   promoting_pawn_inverse[digni][pos_y][pos_x] = random_table[WHITE|digni][pos_y][pos_x][0/*WHITE*/] ^
															     random_table[WHITE|PAWN ][pos_y][pos_x][0/*WHITE*/];
				else if(pos_y == 0) //inverse black queen with white pawn
				   promoting_pawn_inverse[digni][pos_y][pos_x] = random_table[BLACK|digni][pos_y][pos_x][1/*BLACK*/] ^
															     random_table[BLACK|PAWN ][pos_y][pos_x][1/*BLACK*/];
				else promoting_pawn_inverse[digni][pos_y][pos_x] = BIT(0);

			}
	
}

//for more secure 64 bit full coverrage
BIT hash_table::get_mixed_random()
{
  BIT rnd = BIT(0);

  for(unsigned int i = 0; i<32 ;i++)
	rnd ^= (get_random() << i);

  return rnd;
}


bool hash_table::rebuild_table(unsigned int bit_addr)
{
 unsigned int num = BIT(1)<<bit_addr;
 if(num == BIT(0) || bit_addr == 0)
 {
	 cout << "info string bad hash size" << endl;
	 return false;
 }
 assert(num!=0 && bit_addr!=0);
 try
 {
   table.resize(num);
 }
 catch (exception& )
 {
     cout << "info string unable to allocate hash in memory" << endl;
	 return false;
 }
 clear_table();

 mask = BIT(0);
 for(unsigned int i = 0;i<bit_addr;++i)
   mask |= (1<<i);

 return true;
}

void hash_table::clear_table()
{
	vector<hash_table_el>::iterator it = table.begin();
	for(;it!=table.end();++it)
	{
		(*it).set_key(BIT(0)); //it means this hash element is unused
		(*it).set_score(0);
		(*it).set_depth(0);
		(*it).set_mode(inside);
		(*it).set_move(NULL);
	}
}

unsigned int hash_table::get_fill_promill()
{
	unsigned int count = 0;
	vector<hash_table_el>::iterator it = table.begin();
	for(;it!=table.end();++it)
	{
		if((*it).get_key()!=BIT(0))
			count++;
	}
	float ratio = float(count)/float(table.size());
	return (unsigned int)(ratio*1000);
}

void hash_table::to_console()
{
	vector<hash_table_el>::iterator it = table.begin();
	for(;it!=table.end();++it)
	{
		if((*it).get_key()!=BIT(0))
		{
			move_el* move = (*it).get_move();
			cout << "key: " << (*it).get_key() << "(" << ((*it).get_key() & mask) <<"),";
			cout << "depth: " << (*it).get_depth() << ",";
			if(move!=NULL)
				cout << "move: " << *(move) << ",";
			else
				cout << "move: NO ,";
			cout << "mode: " << (*it).get_mode() << ",";
			cout << "score: " << (*it).get_score() <<endl;
		}
	}
}

void hash_table::to_console_info()
{
	vector<unsigned int> count_depths(MAX_TREE_DEPTH,0);
	unsigned int count_total = 0;
	unsigned int count_inside = 0;
	unsigned int count_bigger_eq = 0;
	unsigned int count_lower_eq = 0;
	vector<hash_table_el>::iterator it = table.begin();
	for(;it!=table.end();++it)
	{
		if((*it).get_key()!=BIT(0))
		{
			count_depths[(*it).get_depth()]++;
			count_total++;
			if((*it).get_mode() == inside) count_inside++;
			if((*it).get_mode() == bigger_eq) count_bigger_eq++;
			if((*it).get_mode() == lower_eq) count_lower_eq++;
		}
	}
	cout << "hash table info: " <<endl;
	cout << table.size() << " entries, " << count_total << " filled," << table.size()-count_total << " not filled";
	cout << "(" << int(100.0*(float(count_total)/float(table.size()))) << "%)" <<endl;
	cout << "depths statistics:" <<endl;
	for(unsigned int i = 0;i<MAX_TREE_DEPTH;++i)
		if(count_depths[i]!=0)
			cout << " depth " << i << " - " << count_depths[i] << " entries" << endl;
	cout << "inside: " << count_inside <<endl;
	cout << "bigger_eq: " << count_bigger_eq <<endl;
	cout << "lower_eq: " << count_lower_eq <<endl;
	cout << "end hash info" <<endl;
}