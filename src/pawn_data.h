#ifndef _PAWN_DATA
#define _PAWN_DATA

#include "basic.h"

class pawn_data
{
private:
	BIT white_bits;
	BIT black_bits;
public:
	pawn_data();

	void or_white_with(BIT bit) { white_bits |= bit;}
	BIT get_white_bit() { return white_bits;}
	void or_black_with(BIT bit) { black_bits |= bit;}
	BIT get_black_bit() { return black_bits;}

	bool is_white_pawn_on_file(unsigned int file);
	bool is_black_pawn_on_file(unsigned int file);

	void clear();
};

#endif