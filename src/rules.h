
#ifndef _RULES
#define _RULES

#include "basic.h"
#include "list_special.h"

typedef list_special<move_el*> moves_list; 

////////////////////////////////////////////////////////////////////////////////
//this class generates all chess rules in precompiled format. It uses
//special linked lists with PST tables to present all this data
////////////////////////////////////////////////////////////////////////////////
class rules
{
private:
        typedef list_special<move_el*> moves_list; 
	    moves_list all_comb[PIECE_ID_MAX][BOARD_SIZE][BOARD_SIZE];   

	    void generate_list_Rooks(moves_list& list,int x,int y);  
        void generate_list_Bishops(moves_list& list,int x,int y);
        void generate_list_Queens(moves_list& list,int x,int y);
        void generate_list_Knights(moves_list& list,int x,int y);
        void generate_list_Kings(moves_list& list,int x,int y,int color);

        void generate_list_Pawns(moves_list& list,int x,int y,int color);
		void register_as_promotion(moves_list& list,const move_el& marker,int mode); //for generating promotions
public:
	rules();

	friend class board;
     
};

void print_moves_list(moves_list &obj);

#endif