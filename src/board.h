/* 
 * File:   board.h
 * Author: jackalsh
 *
 * Created on December 15, 2011, 5:56 PM
 */

#ifndef BOARD_H
#define	BOARD_H

#include <vector>
#include <map>
#include <list>
#include <string>

#include "piece.h"
#include "move_storage.h"
#include "rules.h"
#include "hash_table.h"
#include "control.h"
#include "principal_variation.h"
#include "history_heuristic.h"
#include "killer_heuristic.h"
#include "eval_data.h"

using namespace std;

//square info
struct square
{
	piece *p_piece;			//piece in it
	coord* full_coord;		//predefined coordinate object
};

//////////////////////////////////////////////////////////////////
//The main class to present any chess game/position and do
//the search on it.
//////////////////////////////////////////////////////////////////
class board
{
private:
	square mat[BOARD_SIZE][BOARD_SIZE]; //stores information about square
                                        //position and pieces on it
	vector <piece *> pieces;            //all pieces in one vector
	int turn;   //INT useful for using  WHITE BLACK constants
                   
    BIT turn_side_bit;BIT not_turn_bit;
                //stores bits for occupied squares for white and black pieces

	piece *white_king;  //kings
	piece *black_king;

	BIT bit_from_xy(int x,int y);//this function is quick because all
                                 //precompiled values already stored in
                                 //mat[y][x]::square::coord object
	void do_move_low(move_el* move);
	void undo_move_low(move_el* move);

	//MOVE unlike DO_MOVE_LOW make some maintaining board routines whose are slow. Should be used by user only
    void move(move_el* move_arg); 
        
    move_storage m_store;     //this object encapsulates all staff search algorithm works with
    rules m_rules;            //objects holds all chess rules 
	hash_table m_hash;        //all Zobrist/hash table routines
	control m_control;        //provides time control of the engine
	principal_variation m_pv; //maintaining the principal variation routines
	history_heuristic m_hh;   //maintaining the history heuristic routines
	killer_heuristic m_kh;    //maintaining the killer heuristic routines
	eval_data m_eval;

	//checks for custom evaluation file "custom.txt" and if finds, loads it to the memory
	bool load_user_custom_eval();
        
	//Sort functions to provide good ordering.
	//This function used in main search.
	//IS_PV tell us we are still on principal variation from the lower depth.
	//HASH_BEST_MOVE is the good move from the hash table
    void sort_moves_by_importance(bool& is_pv,move_el* hash_best_move);
	//This function used in quiescence search.
	void sort_moves_by_importance_captures_only();

	BIT calculate_hashkey_for_current_position();
	void do_null_move_low();
	void undo_null_move_low();
        
	//THE SEARCHES
    int forced_alphabeta(int alpha,int beta); //Quiescence
	int alphabeta_best_line(int depth,bool is_make_move,int alpha,int beta,bool is_pv,int mode); //Main PVS
	int perft(int depth,bool is_full_info = false); //perft recursive search

	//Used to not fall in the zungzwang trap when using Null Move Heuristics
	bool is_zungzwang_danger();
public:
	board();
	void add_piece(int color,int digni,int x,int y);
	void clear_disabled_pieces();
	void clear_game();
	void init_game();
	void init_game_FEN(const string& FEN_str);
	void to_console();
	void to_console_info();
	string get_fen_notation();

	//those functions used only by slow routines
    void allow_castling_rights(int side,int direction); //should not be used by user directly
	bool check_castling_rights(int side,int direction);
	coord* find_double_moved_pawn(unsigned int vfile); //TODO: to private
	void move(int from_x,int from_y,int to_x,int to_y);

	void swap_sides();
	int  get_turn_color();
	void set_turn_color(int color);
        
    void generate_moves();			   //generate moves functions DON'T check if any king at CHECK
    void generate_moves_captures_only(); //for performance purposes. alpha-beta search automatically do this
    void print_available_moves();		 //as part of his routines.

	//those functions return actual score returned from the search function
	int fw_search(bool is_communication,unsigned int main_depth,unsigned int time_to_think = INFINITE_SEARCH);          //FULL WIDTH search
    int fw_search_and_move(bool is_communication,unsigned int main_depth,unsigned int time_to_think = INFINITE_SEARCH); //FULL WIDTH search with moving
	//perft version of the search. returns terminal count of leafs.
	int pf_search(unsigned int main_depth,bool is_full_info = false);

	int position_evaluation();
	//return true if SCORE is any type of checkmate
	bool is_score_checkmate(int score);

	//this function must be called only after FW_SEARCH was calculated best line.
	//before actual move was done
	bool is_best_move_SAN(string SAN_arg); //TODO: don't work now. Code changed since last use

	void store_in_hash_table(int score_arg,int depth_arg,move_el* move_arg,score_mode mode_arg);
	void restore_from_hash_table(int &alpha,int &beta,int depth_arg,move_el* &move_arg);

	void change_hash_size(unsigned int new_size);

	bool save_to_configure_file(string filename = default_config_filename);
	bool load_from_configure_file(string filename = default_config_filename);

	eval_data& get_eval_data(){return m_eval;} //TODO: more sophisticated way to change evaluation?
	//these functions actually copy the eval's objects so you can change, delete or do whatever you want
	//to argument after copying. It won't influence evaluation
	void set_eval_data(eval_data& data) {m_eval = data;} //you must make sync before loading!

	control& get_control_data(){return m_control;} //TODO: more sophisticated way 
};



#endif	/* BOARD_H */

