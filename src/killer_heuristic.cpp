#include <iostream>
#include <iomanip>
using namespace std;

#include "killer_heuristic.h"

//clears old data
void killer_heuristic::clear()
{
	for(unsigned int i = 0;i<MAX_TREE_DEPTH;++i)
	{
		 first_killers[i] = NULL;
		 second_killers[i] = NULL;
	}
}

//adds good killer move
void killer_heuristic::add_good_move(scored_move_el& move,unsigned int ply)
{
	assert(ply<MAX_TREE_DEPTH);
	assert(move.get_move_el() != NULL);

	if(is_equal(first_killers[ply] , move.get_move_el())) return; //preventing from two killer slots to contain the same move
	second_killers[ply] = first_killers[ply]; //moveing first killer to the second killer's place
	first_killers[ply] = move.get_move_el();  //set new move as the first killer
}

//checking if move is first killer on PLY depth 
bool killer_heuristic::is_first_killer(scored_move_el& move,unsigned int ply)
{ 
	assert(move.get_move_el() != NULL);
	if(is_equal(first_killers[ply],move.get_move_el()))
		 return true;
	return false;
}

//checking if move is second killer on PLY depth 
bool killer_heuristic::is_second_killer(scored_move_el& move,unsigned int ply)
{ 
	assert(move.get_move_el() != NULL);
	if(is_equal(second_killers[ply],move.get_move_el()))
		 return true;
	return false;
}

void killer_heuristic::to_console_info()
{
	cout << "killer heuristic" << endl;
	for(unsigned int i = 0;i<MAX_TREE_DEPTH;++i)
	{
	 if(first_killers[i] == NULL && second_killers[i] == NULL)
		 continue;

	 cout << "depth " << i << " :" ;
     cout <<  "1.";
	 if(first_killers[i] == NULL)
			 cout << setw(11) << "none" << " ";
		 else
			 cout << setw(5) << *(first_killers[i]) ;
	 cout << " 2.";
	 if(second_killers[i] == NULL)
			 cout << setw(11) << "none" << " ";
		 else
			 cout << setw(5) << *(second_killers[i]) ;
	 cout << endl;
	}

	
}

killer_heuristic::killer_heuristic()
{
	clear();
}