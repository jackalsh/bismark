#ifndef LIST_SPECIAL_H
#define	LIST_SPECIAL_H

#include <ostream>
#include <assert.h>

/////////////////////////////////////////////////////////////////////
//special class to work with linked lists with special element
/////////////////////////////////////////////////////////////////////

#define _NULL_EL 0

#define LINK_TO_NEXT 1
#define LINK_TO_END 2
#define NOTHING_SPECIAL 3

template<class T>
class list_special;

template<class T>
class element_list_special;

template <class T>
class iterator_list_special
{
    private:
        element_list_special<T> *element;
        
        void set_element(element_list_special<T> *el);
        element_list_special<T> *get_element();
    public:
        
        iterator_list_special(); 
        iterator_list_special<T>& operator ++(void );//prefix form
        iterator_list_special<T>& next();
        iterator_list_special<T>& next_stable();
        bool operator==(const iterator_list_special<T>& second);
        bool operator!=(const iterator_list_special<T>& second);
        T& get();
        
        friend class list_special<T>;
};

template <class T>
void iterator_list_special<T>::set_element(element_list_special<T> *el)
{
  element = el;  
}

template <class T>
element_list_special<T>* iterator_list_special<T>::get_element()
{
    return element;
}

template<class T>
iterator_list_special<T>::iterator_list_special()
{
  element = _NULL_EL;
}

template<class T>
iterator_list_special<T>& iterator_list_special<T>::operator ++(void )//prefix form
{
    this->element = this->get_element()->next_element;
    return *this;
}

template<class T>
iterator_list_special<T>& iterator_list_special<T>::next()
{
    this->element = this->get_element()->next_element; 
    return *this;
}

template<class T>
iterator_list_special<T>& iterator_list_special<T>::next_stable()
{
    this->element = this->get_element()->next_stable_element; 
    return *this;
}

template<class T>
bool iterator_list_special<T>::operator==(const iterator_list_special<T>& second)
{
    return (this->element == second.element);
}

template<class T>
bool iterator_list_special<T>::operator!=(const iterator_list_special<T>& second)
{
    return (this->element != second.element);
}

template<class T>
T& iterator_list_special<T>::get()
{
    assert(this->element != _NULL_EL);
    return this->element->stored_obj;
}

/////////////////////////////////////////////////////////

template<class T>
class element_list_special
{
    private:
        T stored_obj;
        
        element_list_special<T> *next_element;
        element_list_special<T> *next_stable_element;
        
    public:
    element_list_special(const T& data);
        
    friend class list_special<T>;
    friend class iterator_list_special<T>;
};

template<class T>
element_list_special<T>::element_list_special(const T& data)
{
    stored_obj = T(data);
    next_element = _NULL_EL;
    next_stable_element = _NULL_EL;
}

////////////////////////////////

template<class T>
class list_special {
private:
   iterator_list_special<T> begin_el;//iterator to first element
   iterator_list_special<T> end_el;  //iterator to last element
   iterator_list_special<T> null_el; //always points to NULL
   
   void push_back(element_list_special<T>* el);
public:
   list_special();
   iterator_list_special<T> begin();
   iterator_list_special<T> end();
    
   void push_back(const T& data);
   //this function can't be used when caller and other container are the same object
   void push_back(list_special<T>& other_container,int mode);
   
   bool is_empty();
   
   typedef iterator_list_special<T> iterator;
};

template<class T>
list_special<T>::list_special()
{
   begin_el.set_element(_NULL_EL);
   end_el.set_element(_NULL_EL);
   
   null_el.set_element(_NULL_EL);
}

template<class T>
iterator_list_special<T> list_special<T>::begin()
{
    return begin_el;
}

template<class T>
iterator_list_special<T> list_special<T>::end()
{
    return null_el;
}

template<class T>
void list_special<T>::push_back(element_list_special<T>* el) 
{//not for ordinary use!!!
    if(is_empty())
    {      
       begin_el.set_element(el);
       end_el.set_element(el);
    }
    else
    {
        end_el.get_element()->next_element = el;
        end_el.set_element(el);
    }
}

template<class T>
void list_special<T>::push_back(const T& data)
{
  element_list_special<T>*temp_el =  new element_list_special<T>(data);
  push_back(temp_el);
}

template<class T>
void list_special<T>::push_back(list_special<T>& other_container,int mode)
{
    assert(mode == LINK_TO_NEXT || mode == LINK_TO_END || mode == NOTHING_SPECIAL);
	assert(this != &other_container);
    if(other_container.is_empty() || this == &other_container) return;

    list_special<T>::iterator it;
            
	//set all special pointers to the last element from the list to be pointed to
	//the other container's first element
    for(it = this->begin();it!=this->end();++it)
        if(it.element->next_stable_element == _NULL_EL)
            it.element->next_stable_element = other_container.begin().get_element();
      
	//1.
	//in this mode special elements of other container point to the next in the list element
	//zero 0 is the NULL element points to the end of the list
	//order:   ABCDEF.pushback(GHIJK)
	//special: --0--0          -----)
	//result:  ABCDEFGHIJK
	//         --G--GHIJK0
    if(mode == LINK_TO_NEXT)
    {
       for(it = other_container.begin();it!=other_container.end();++it)
           it.get_element()->next_stable_element = it.get_element()->next_element;
    }

	//2.
	//in this mode special elements of other container points to the last null element of the list
	//order:   ABCDEF.pushback(GHIJK)
	//special: --0--0          -----)
	//result:  ABCDEFGHIJK
	//         --G--G00000 
    if(mode == LINK_TO_END)
    {
       for(it = other_container.begin();it!=other_container.end();++it)
           it.get_element()->next_stable_element = _NULL_EL;
    }

	//3.
	//NOTHING_SPECIAL does the following
	//order:   ABCDEF.pushback(GHIJK)
	//special: --0--0          -----)
	//result:  ABCDEFGHIJK
	//         --G--G----- 
    
	//Copying elements
    for(it = other_container.begin();it!=other_container.end();++it)
    {      
        this->push_back(it.get_element());
    }
    
	//Clearing container. So element were moved completely
    other_container.begin_el.set_element(_NULL_EL) ;
    other_container.end_el.set_element(_NULL_EL) ;
    
}

template<class T>
bool list_special<T>::is_empty()
{
  if(begin_el.get_element() == _NULL_EL && end_el.get_element() == _NULL_EL)
      return true;
  return false;
}

#endif	/* LIST_SPECIAL_H */

