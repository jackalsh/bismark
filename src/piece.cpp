#include <iostream>
using namespace std;

#include "piece.h"


///////////////piece////////////////////

 piece::piece()
 {
	 position = NULL;
	 info = NO_PIECE;
	 set_position(0);
	 set_moved_at_past_flag(true);
 }

 //use predefined constants for COLOR and DIGNITY arguments
 //from basic.h file to work with this function
 piece::piece(int color,int digni,coord* pos)
 {
	 set(color,digni);
	 set_position(pos);
	 set_moved_at_past_flag(true);
 }

 piece::piece(int color,int digni,int x,int y)
 {
	 coord *coordinate = new coord(x,y);
	 set(color,digni);
	 set_position(coordinate);
	 set_moved_at_past_flag(true);
 }

 void piece::set_color(int color)
 {
	 info = (info & DIGNI_BITS) | color;
 }

 void piece::set_digni(int digni)
 {
	 info = (info & COLOR_BITS) | digni;
 }

 void piece::set(int color,int digni)
 {
	 info = color | digni;
 }

 //this function disables piece without actual delete.
 //it's coordinate on the board no longer excists.
 //any data outside the piece object( like mat structure in the board object)
 //should be considered separately
  void piece::set_no_piece()
  {
	 position = NULL;
  }

 bool piece::is_color(int color)
 {
	 return ((info & COLOR_BITS)==color);
 }

 bool piece::is_digni(int digni)
 {
	 return ((info & DIGNI_BITS) == digni);
 }

bool piece::is(int color,int digni)
 {
	 return (info == (color | digni));
 }

bool piece::is_no_piece()
 {
   return (position == NULL);
 }

int piece::get_color()
 {
   return (info & COLOR_BITS);
 }

int piece::get_digni()
 {
	return  (info & DIGNI_BITS);
 }

 coord* piece::get_position()
 {
	return position;
 }

 BIT piece::get_bit_position()
 {
	 if(position != NULL)
		 return position->bit;
	 return (BIT)0;
 }

 int piece::get_x_position()
 {
	 if(position != NULL)
		 return position->x;
	 return (-1);
 }

 int piece::get_y_position()
 {
	 if(position != NULL)
		 return position->y;
	 return (-1);
 }

 void piece::set_position(coord* pos)
 {
   position = pos;
 }

 void piece::to_console()
 {
	 if(is_color(WHITE))
	    cout << "W";
	 if(is_color(BLACK))
	    cout << "B";
	 if(is_digni(PAWN))
		 cout << "P ";
	 if(is_digni(KNIGHT))
		 cout << "Kn";
	 if(is_digni(BISHOP))
		 cout << "B ";
	 if(is_digni(ROOK))
		 cout << "R ";
	 if(is_digni(QUEEN))
		 cout << "Q ";
	 if(is_digni(KING))
		 cout << "K ";
 }

 void piece::to_console_chess_position()
 {
	 if(get_position() == NULL)
		 cout << "none";
	 else
	     cout << *(get_position());
 }

//returns a number from internal data for identification of a piece
//can be used as index in PST tables.
int piece::get_ID()
 {
	return info;
 }


int get_absolute_value(int ID)
{
	assert(ID >= 0 && ID < PIECE_ID_MAX);
	return(absolute_piece_values[ID]);
}