#include <fstream>
#include <iostream>
#include <algorithm>
using namespace std;

#include "basic.h"
#include "epd_container.h"

///////////////////////position_info///////////////////////////////////
//position_info is an element of epd file contain all the information
//of a position and it's solutions. If there are many solution,
//each one can be scored differently.

position_info::position_info()
{
	clear();
}

void position_info::clear()
{
	FEN.clear();
	for(unsigned int i = 0;i<10;++i)
		c[i].clear();
	bm.clear();
}

string position_info::get_FEN()
{
	return FEN;
}

void position_info::set_FEN(string FEN_arg)
{
	FEN = FEN_arg;
}

string position_info::get_comment(unsigned int index) 
{
	assert(index<=9);
	if(index>9) 
	{
		cout << "EPD error: comment indexes must be in the interval 1-9" << endl;
		return string();
	}
	return c[index];
}

void position_info::set_comment(unsigned int index,string comment_arg)
{
	assert(index<=9);
	if(index>9) 
	{
		cout << "EPD error: comment indexes must be in the interval 1-9" << endl;
		return ;
	}
	c[index] = comment_arg;
}

void position_info::set_best_move(string best_move_arg)
{
	bm = best_move_arg;
}

string position_info::get_best_move()
{
	return bm;
}

void position_info::set_id(string id_arg)
{
	id = id_arg;
}

string position_info::get_id()
{
	return id;
}

void position_info::to_console()
{
  if(id.size() == 0)
      cout <<endl;
  else
	  cout << id << endl;
  cout << "FEN: " ;
  if(FEN.size() == 0)
	  cout << "unknown" << endl;
  else
	  cout << FEN << endl;
  if(bm.size() != 0)
	  cout << "best move: " << bm << endl;
  for(unsigned int i = 0;i<10;++i)
	  if(c[i].size() != 0)
		  cout << "comment " << i << ": " << c[0] << endl;
}

////////////////////////epd_container///////////////////////////////
//this class holds set of positions and it's solutions

epd_container::epd_container()
{
	clear();
}

void epd_container::clear()
{
	positions.clear();
}

void epd_container::add_position(position_info pos)
{
	positions.push_back(pos);
}

unsigned int epd_container::size()
{
	return positions.size();
}

position_info& epd_container::get(unsigned int index)
{
	assert(index < size());
	return positions.at(index);
}

bool epd_container::load_epd(const char filename[])
{
  ifstream input_file;
  input_file.open(filename);//TODO: only in
  if(!input_file)
  {
	//cout <<"IO error! Can't open " << filename << " file";
	return false;
  }
  string input_str;
  position_info pos_temp;
  while(!(input_file.eof()))
  {
	pos_temp.clear();
    getline(input_file,input_str);
	size_t begin = 0;size_t end;
	unsigned int delimiter = 0;
	while( (end = input_str.find(";",begin)) != string::npos)
	{
		string amomic_str = input_str.substr(begin,end-begin);

		if(delimiter == 0)
		{//FEN notation usually includes bm
			size_t bm_place = amomic_str.find("bm");
			if(bm_place != string::npos)
			{
				string best_move_temp = amomic_str.substr(bm_place+2);
				remove_spaces_from(best_move_temp);

				pos_temp.set_best_move(best_move_temp);
				pos_temp.set_FEN(amomic_str.substr(0,bm_place));
			}
			else
				pos_temp.set_FEN(amomic_str);

		}

		size_t special_place;
		special_place = amomic_str.find("c");
		if(special_place != string::npos)
		{
			unsigned int index;
			if(special_place+1 < amomic_str.size() && (index = (amomic_str[special_place+1]-'0'))<10)
				pos_temp.set_comment(index,amomic_str.substr(special_place+2));
		}

		special_place = amomic_str.find("id");
		if(special_place != string::npos)
		{
			pos_temp.set_id(amomic_str.substr(special_place+2));
		}

		begin = end+1;
		delimiter++;
	}

	add_position(pos_temp);
  }

  input_file.close();
  return true;
}

void epd_container::to_console()
{
	cout << "This EPD contain " << size() << " positions" << endl;
	for(unsigned int i = 0;i<size();++i)
	{
		cout << i+1 << ".";
		get(i).to_console();
		cout << endl;
	}
}

void epd_container::random_shuffle()
{
	std::random_shuffle(positions.begin(),positions.end());
}