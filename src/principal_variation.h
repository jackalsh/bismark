
#ifndef _PV
#define _PV

#include <vector>
#include "basic.h"

////////////////////////////////////////////////////////////////////////////////
//Class for working with any chess variation of moves.
//this class is used only for slow output to GUI
//and other mantaining routines
////////////////////////////////////////////////////////////////////////////////
class line
{
private:
	//keeps
	vector<move_el*> lin;   //moves in this line
	int score;              //score of this line
	unsigned int iteration; //on which iteration this line was calculated
public:
	line();

	move_el* get_first_move();
	void to_console();

	int get_score() const {return score;}
	int get_iteration() const {return iteration;}

	friend class principal_variation;
};

////////////////////////////////////////////////////////////////////////////////
//Class for working with principal variation of moves.
//This class is used in the search to follow the PV and
//sorting moves
////////////////////////////////////////////////////////////////////////////////

class principal_variation
{
	//alphabeta use PV_MAT and PV_LEN to build new principal variation
	move_el* pv_mat[MAX_TREE_DEPTH][MAX_TREE_DEPTH];
	unsigned int pv_len[MAX_TREE_DEPTH];

	unsigned int root_store_depth; 

	//this array of best lines contain all the lines we get iteratively from algorithm
	vector<line> best_container;

	//this array contains pv from previous iteration for following it in current iteration
	move_el* pv_follow[MAX_TREE_DEPTH];

	void set_root_store_depth(unsigned int root_store_depth_arg);
	unsigned int get_root_store_depth() const;
public:
	principal_variation();
	void clear();
	void init_new_search(unsigned int root_store_depth_arg);

	void store(unsigned int ply,move_el* move);
	void init_at_node(unsigned int ply);

	void to_console_current();

	void add_to_container(int score_arg, int iteration_arg);
	bool is_any_lines() ;
	unsigned int get_num_of_lines() ;
	line get_last_line();
	line get(unsigned int index);

	void clear_followed();
	void set_followed(const line& line_arg);
	bool compare_with_followed(unsigned int ply,scored_move_el& move);
};

#endif