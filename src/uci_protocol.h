
#ifndef _UCI_PROTOCOL
#define _UCI_PROTOCOL

#include "board.h"

namespace uci
{
	const string engine_name = "Bismark";
	const string engine_version = "1.4";
	const string author_name = "Evgeny Shtranvasser";

	class uci_protocol
	{
	private:
		board* board_ptr;

		void output_identification();
		void output_options();
		void output_uciok();
		void output_readyok();

		void quit_program();

		void input_ucinewgame();
		void input_position_startpos(const string& input_str);
		void input_position_fen(const string& input_str);
		void input_go(const string& input_str);
		void input_options(const string& input_str);

		void cycle_step();
		void cycle();

		uci_protocol():board_ptr(NULL){};
	public:
		uci_protocol(board& board_arg);		

	};

	//checks if output buffer not empty without actual reading from it
	bool is_input_available();
	//returns if GUI sends stop to engine's buffer
	bool is_search_interrupted();
	//output correct score in UCI format. Because all mate scores are relative to initial position, we need to know
	//depth to correct mate according to current position depth, i.e. ROOT_DEPTH. If not specifies, we assume
	//root depth == initial depth == 0
	void output_score(int score,unsigned int root_depth = 0);
}

#endif
