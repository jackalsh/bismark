

#ifndef _HASH_TABLE
#define _HASH_TABLE

#include <vector>
#include <map>
using namespace std;

#include "basic.h"

extern "C"
{
  #include "mt64.h"
}

enum score_mode
	{
		inside,		//value is inside alpha-beta window and this is exact score
		bigger_eq,  //value was failed high
		lower_eq	//value was failed low
	};

class hash_table_el
{
private:
	BIT key;
	int score;
	move_el* move;
	score_mode mode;
	int depth;
public:
	hash_table_el():key(BIT(0)),score(0),move(NULL),mode(inside),depth(0) {}
	hash_table_el(move_el* move_arg,score_mode mode_arg,int depth_arg):key(BIT(0)),score(0),move(move_arg),mode(mode_arg),depth(depth_arg) {}

	void set_key(BIT key_arg) { key = key_arg;}
	BIT get_key() const { return key;}

	void set_score(int score_arg) { score = score_arg;}
	int get_score() const { return score;}

	void set_move(move_el* move_arg) { move = move_arg;}
	move_el* get_move() { return move;}

	void set_mode(score_mode mode_arg) { mode = mode_arg;}
	score_mode get_mode() const { return mode;}

	void set_depth(int depth_arg) { depth = depth_arg;}
	int get_depth() const { return depth;}
}; //make assert for checking if depth<0

class hash_table
{
private:
	//piece id ; from ; to ; turn_color
	BIT random_table[PIECE_ID_MAX][BOARD_SIZE][BOARD_SIZE][2];
	BIT black_turn_random;
	BIT promoting_pawn_inverse[PIECE_DIGNITY_MAX][BOARD_SIZE][BOARD_SIZE];

	BIT get_random() { return genrand64_int64(); }
	BIT get_mixed_random(); //pretty slow and unused for now

	int mask;
	vector<hash_table_el> table;
public:
    hash_table();
	bool rebuild_table(unsigned int bit_addr);
	void clear_table();

	unsigned int get_fill_promill();
	void to_console();
	void to_console_info();

    friend class board;
};

#endif