
////////////////this module handles perft version of the search//////////////////

#include <iostream>
using namespace std;

#include "board.h"

int board::pf_search(unsigned int main_depth,bool is_full_info)
{
	assert (m_store.get_hist().get_hash() == calculate_hashkey_for_current_position());

	//initialize nods ant time counters
	m_control.init_new_search(INFINITE_SEARCH);
	m_control.disable_communication_mode();

	//all this staff is no used by perft so we just clearing all.
	//m_hash.clear_table();
	m_hh.clear();
	m_kh.clear();
	m_pv.clear();

	//those structures need to know the relative root depth to correctly maintain and output the data
	m_store.set_this_depth_as_root();

	unsigned long count = 0;
	//begin perft search
	count = perft(main_depth,is_full_info);

	return count;
}

int board::perft(int depth,bool is_full_info)
{
	//generates moves for current position
	generate_moves();
	if(m_store.is_alert_checked())
	{
		return (0); 
	}

	//counting leaf nodes
	if(depth<=0) 
	{
		return 1; ;
	}

	unsigned long count = 0,count_tmp;

	unsigned int size = m_store.num_moves_this_depth();

	if(is_full_info)
	{
		cout << "------perft for position-------" << endl;
		cout << this->get_fen_notation() << endl;
		to_console();
		cout << "total " << size << " moves in this position" << endl; 
	}

	for(unsigned int i = 0;i<size;i++)
	{
		//CUR_MOVE will hold the current working move
		scored_move_el& cur_move  = m_store.get(i);
		//make a move
		do_move_low(cur_move.get_move_el());

		count_tmp = perft(depth - 1,false);
		count += count_tmp;

		if(is_full_info)
		{
			cout << "* " << *cur_move.get_move_el() << ", count: " <<   count_tmp << endl;
		}

		//undo the move
		undo_move_low(cur_move.get_move_el());

	}

	//stalemate and mate are not part of the perft
	//return counted leafs
	return count;
}