#ifndef _EVAL_DATA
#define _EVAL_DATA

//#include <string>

#include "basic.h"
#include "piece.h"

#include "config_default.h"

#include "material_data.h"
#include "pawn_data.h"

//////////////////////////////////////////////////////////////////////////////////////
//The idea of this class is to provide ALL custom properties of the search and, if
//you want, incapsulate all the "magic" numbers in the code.
//All data stored in this class are devided in to two groups
//1.Independent data. Change it will change the proper option of the evaluation or the
//  search
//2.Built on data. Data that are built on GROUP 1 variables and precompillated for 
//  fast use
//////////////////////////////////////////////////////////////////////////////////////

class eval_data
{
private:
  //GROUP 1 data
	int piece_values[PIECE_ID_MAX];

	int DPP;           //double pawn penalty
	int PIP[5] ;	   //pawn islands penalty
    int PPB;           //pawn passed bonus
	int UPB;           //unstoppable pawn bonus
	int CTEKB;         //close to enemy king bonus 
	int KnOB;          //Knight outpost bonus
	int RSOFB;         //Rook on semi opened file bonus
	int ROFB;          //Rook on opened bonus
    int BPB;           //Bishop pair bonus

	int MG_bonuses[PIECE_DIGNITY_MAX][BOARD_SIZE][BOARD_SIZE];
	int EG_bonuses[PIECE_DIGNITY_MAX][BOARD_SIZE][BOARD_SIZE];

  //GROUP 2 data
	//contain precomputed data for max possible value for the material
    int max_material;
	//Pythagorean array used for fast calculating distance between two squares
	//int pithagorean_dist[BOARD_SIZE][BOARD_SIZE][BOARD_SIZE][BOARD_SIZE];
	//Tropism array used to calculate danger when enemy pieces close to king. Closest squares get the max number
	int tropizm_to_enemy_king[BOARD_SIZE][BOARD_SIZE][BOARD_SIZE][BOARD_SIZE]; 
	
  //those variables will be possibly integrated with pawn hash
	material_data material;
	pawn_data pawns;
public:
	int get_double_pawn_penalty(){return DPP;}
	void set_double_pawn_penalty(int penalty){DPP = penalty;}
	void add_double_pawn_penalty(int addition){DPP += addition;}

	int get_pawn_islands_penalty(unsigned int num_islands) {assert(num_islands<5);return PIP[num_islands];}
	void set_pawn_islands_penalty(unsigned int num_islands,int penalty) {assert(num_islands<5); PIP[num_islands] = penalty;}
	void add_pawn_islands_penalty(unsigned int num_islands,int addition) {assert(num_islands<5); PIP[num_islands] += addition;}

	int get_pawn_passed_bonus(){return PPB;}
	void set_pawn_passed_bonus(int bonus){PPB = bonus;}
	void add_pawn_passed_bonus(int addition){PPB += addition;}

	int get_unstopable_pawn_bonus(){return UPB;}
	void set_unstopable_pawn_bonus(int bonus){UPB = bonus;}
	void add_unstopable_pawn_bonus(int addition){UPB += addition;}

	int get_close_to_enemy_king_bonus(){return CTEKB;}
	void set_close_to_enemy_king_bonus(int bonus){CTEKB = bonus;}
	void add_close_to_enemy_king_bonus(int addition){CTEKB += addition;}
	
	int get_knight_outpost_bonus(){return KnOB;}
	void set_knight_outpost_bonus(int bonus){KnOB = bonus;}
	void add_knight_outpost_bonus(int addition){KnOB += addition;}

	int get_rook_on_semi_opened_bonus(){return RSOFB;}
	void set_rook_on_semi_opened_bonus(int bonus){RSOFB = bonus;}
	void add_rook_on_semi_opened_bonus(int addition){RSOFB += addition;}

	int get_rook_on_opened_bonus(){return ROFB;}
	void set_rook_on_opened_bonus(int bonus){ROFB = bonus;}
	void add_rook_on_opened_bonus(int addition){ROFB += addition;}

	int get_bishop_pair_bonus(){return BPB;}
	void set_bishop_pair_bonus(int bonus){BPB = bonus;}
	void add_bishop_pair_bonus(int addition){BPB += addition;}

	//this function synchronize internal state of GROUP 2 with GROUP 1
	void sync();
public:

	void init_default();
	eval_data();

	int get_value_by_id(unsigned int id) ;
	int get_value(piece* p) ;

	//creates config file according to it's internal state
	//this config can be connected to project instead of CONFIG_DEFAULT.H
	bool save_to_configure_file(string filename = default_config_filename);
	bool load_from_configure_file(string filename = default_config_filename);

	void mutate(eval_data& mutate_ranges_factor);

	friend class board;
};

#endif