#include "pawn_data.h"


pawn_data::pawn_data()
{
	clear();
}

void pawn_data::clear()
{
	white_bits = BIT(0);
	black_bits = BIT(0);
}

bool pawn_data::is_white_pawn_on_file(unsigned int file)
{
	return( (white_bits & bit_files[file]) != BIT(0));
}

bool pawn_data::is_black_pawn_on_file(unsigned int file)
{
	return( (black_bits & bit_files[file]) != BIT(0));
}