
#include <iostream>
#include <sstream>
#include <iostream>

#include "basic.h"

extern "C"
{
	#include "mt64.h"
}

BIT outpost_side_bits[BOARD_SIZE][BOARD_SIZE][2];
BIT outpost_front_bits[BOARD_SIZE][BOARD_SIZE][2];
int absolute_piece_values[PIECE_ID_MAX];

/////////////////// coord  ///////////////////////////////////////////////////////////

//Система координат в программе: нулевая координата (0,0) это поле h1.
//для горизонтальной состовляющей изменения происходят справа налево вплоть до 7.
//для вертикальной снизу вверх.Таким образом поле a8 есть (7,7).
//Битбоардные координаты получаются из обычных согласно bit = (BIT) 1 << (8*Y+X);

coord::coord()
{
	x=0;y=0;
    bit=0;
}

coord::coord(int arg_x,int arg_y)
{
	x = arg_x;y=arg_y;
    bit = (BIT) 1 << (8*arg_y+arg_x);
}

void coord::to_console()
{
   std::cout << index_to_file_letter(x) <<  index_to_rank_letter(y);
}

std::ostream &operator<<(std::ostream &cout,coord &obj)
{
  obj.to_console();
  return cout;
}


/////////////////// castling //////////////////////////////////////////////////////

castling::castling(coord *rook_from_arg,coord *rook_to_arg,BIT attack_restr_arg,BIT move_restr_arg)
{
  rook_from = rook_from_arg;
  rook_to = rook_to_arg;
  attack_restr_bit = attack_restr_arg;
  move_restr_bit = move_restr_arg;
}

////////////////// move_el ////////////////////////////////////////////////////////

move_el::move_el(coord* from_m,coord* to_m,int mode)
{
	assert( ((mode & SILENT_ONLY) && (mode & CAPTURE_ONLY)) == false );

    from = from_m;to = to_m;
    silent_only = 0;
    capture_only = 0;
    if(mode & SILENT_ONLY)
        silent_only = to->bit;
    if(mode & CAPTURE_ONLY)
        capture_only = to->bit;
	castle_info = NULL;
	if(mode & FOLLOWING_ALLOWED) //following is always disabled by default
		is_enable_move_following = true;
	else
		is_enable_move_following = false;
	pawn_double_move = NULL;
	enpassan_info = NULL;
	pawn_promoting = NO_PIECE;
}

//if even one of pointers are NULL returns false
//it compares only from and to squares and not any other masks.
bool is_equal(move_el* first,move_el* second)
{
	if(first == NULL || second == NULL)
		return false;
	if(first->from->bit == second->from->bit &&
	   first->to->bit == second->to->bit)
	       return true;
	return false;
}

std::ostream &operator<<(std::ostream &out,move_el &obj)
{
  out << *(obj.from)<<*(obj.to);
  switch(obj.pawn_promoting)
  {
	case NO_PIECE: break;
	case QUEEN: out<<"Q";break;
	case KNIGHT: out<<"N";break;
	case BISHOP: out<<"B";break;
	case ROOK: out<<"R";break;
  }
  return out;
}

////////////////// scored_move_el ////////////////////////////////////////////////////

bool comparison_by_score_bigger(const scored_move_el& left,const scored_move_el& right)
{
    return(left.get_score()>right.get_score());
}

bool comparison_by_score_lower(const scored_move_el& left,const scored_move_el& right)
{
    return(left.get_score()<right.get_score());   
}

////////////////// other functions ////////////////////////////////////////////////////

void bit_to_console(BIT bit)
{
  bool ones[64];
  BIT index = (BIT) 1;
  for(int i=0;i<64;i++)
  {
	  ones[63-i] = ((((BIT)1<<i)&bit)!=(BIT)0)?true:false;
  }
  for(int i =0;i<64;i++)
  {
	  if(i%8==0) std::cout << std::endl;
	  if(ones[i])
		  std::cout << "1";
	  else
		  std::cout << "0";
  }
  std::cout << std::endl;  
}

//gives random number in the range [-range,-range+1,...,0,...,range-1,range]
int random_pos_neg_range(unsigned int range)
{
	int ret = int((genrand64_int64()%(range*2 + 1)) - int(range));
	assert( ret >= -int(range) && ret <= int(range));

	return ( ret );
}

//initialization of other constants
void init()
{
	for(int i =0;i<BOARD_SIZE;i++)
     	for(int j =0;j<BOARD_SIZE;j++)
		{
			BIT right = (j > 0) ? bit_files[j-1] : BIT(0);
			BIT left = (j < 7) ? bit_files[j+1] : BIT(0);
			outpost_side_bits[i][j][0 /*WHITE*/] = up_bits[i] & (left | right);
			outpost_side_bits[i][j][1 /*BLACK*/] = down_bits[i] & (left | right);
			outpost_front_bits[i][j][0/*WHITE*/] = up_bits[i] & bit_files[j];
			outpost_front_bits[i][j][1/*BLACK*/] = down_bits[i] & bit_files[j];
		}

	for(int i =  0; i < PIECE_ID_MAX; ++i)
		absolute_piece_values[i] = 0;
	absolute_piece_values[WHITE|PAWN] = absolute_piece_values[BLACK|PAWN] = 1;
	absolute_piece_values[WHITE|KNIGHT] = absolute_piece_values[BLACK|KNIGHT] = 3;
	absolute_piece_values[WHITE|BISHOP] = absolute_piece_values[BLACK|BISHOP] = 3;
	absolute_piece_values[WHITE|ROOK] = absolute_piece_values[BLACK|ROOK] = 5;
	absolute_piece_values[WHITE|QUEEN] = absolute_piece_values[BLACK|QUEEN] = 9;
	absolute_piece_values[WHITE|KING] = absolute_piece_values[BLACK|KING] = 0;
}

////////////////work with strings//////////////////////////

int string_to_int(std::string str)
{
	std::istringstream converter(str);
	int ret;
	converter >> ret;
	return ret;
}

std::string int_to_string(int num)
{
	std::stringstream converter;
	converter << num;
	return converter.str();
}

std::string get_Nth_word(std::string str,int N)
{
	str = str + " ";
        size_t old_index = 0;
	size_t index = 0;
	int count_word = 0;
	std::string temp;
	size_t len = str.size();
	while(index<len)
	{
		if(str[index]==' ') 
		{
         count_word ++;
		 temp = str.substr(old_index,index-old_index);
		 if(count_word == N) return temp;
		 while(index<len && str[index] == ' ') index++;
		 old_index = index;
		}
		index++;
	}
	return std::string();
}

int get_N_of_words(std::string str)
{
	str = str + " ";
	size_t old_index = 0;
	size_t index = 0;
	int count_word = 0;
	size_t len = str.size();
	while(index<len)
	{
		if(str[index]==' ') 
		{
		 count_word ++;
		 while(index<len && str[index] == ' ') index++;
		 old_index = index;
		}
		index++;
	}
	return count_word;
}

bool is_file(char ch)
{
	return (ch >= 'a' && ch <= 'h');
}

unsigned int letter_to_file_index(char ch)
{
	assert(is_file(ch));
	return ( 7 - (ch - 'a'));
}

bool is_rank(char ch)
{
	return (ch >= '1' && ch <= '8');
}

unsigned int letter_to_rank_index(char ch)
{
	assert(is_rank(ch));
	return (ch - '1');
}

char index_to_file_letter(int file_index)
{
	return ((char) (7 - file_index + 'a')) ;
}

char index_to_rank_letter(int rank_index)
{
	return ((char) (rank_index+'1'));
}

void remove_spaces_from(std::string& str)
{
	for(size_t i = 0;i<str.size();++i)
		if(str[i] == ' ') str.erase(i,1);
}

