#include <vector>
#include <string>
using namespace std;

#ifndef _EPD_CONTAINER
#define _EPD_CONTAINER

class position_info
{
private:
	string FEN;
	string c[10];
	string bm;
	string id;
public:
	position_info();
	void clear();

	string get_FEN();
	void set_FEN(string FEN_arg);

	string get_comment(unsigned int index) ;
	void set_comment(unsigned int index,string comment_arg);

	void set_best_move(string best_move_arg);
	string get_best_move();

	void set_id(string id_arg);
	string get_id();

	void to_console();
};

class epd_container
{
private:
	vector<position_info> positions;
public:
	epd_container();
	void clear();
	void add_position(position_info pos);
	unsigned int size();
	position_info& get(unsigned int index);

	void random_shuffle();
	bool load_epd(const char filename[]);
	void to_console();
};

#endif