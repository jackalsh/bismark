
#include "basic.h"

#ifndef _HISTORY_HEURISTIC
#define _HISTORY_HEURISTIC

//////////////////////////////////////////////////////////////////
//This class handless all the history heuristic routines and 
//tables.
//////////////////////////////////////////////////////////////////
class history_heuristic
{
private:
	unsigned int mat[BOARD_SIZE][BOARD_SIZE][BOARD_SIZE][BOARD_SIZE];
public:
	void clear();
    void add_good_move(move_el* move,int depth);
	unsigned int get_move_value(move_el* move);

	void overflow_repair(); //devides all elements in the table by two.

	void to_console_info();

	history_heuristic(void);
};

#endif