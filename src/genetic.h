
#ifndef _GENETIC
#define _GENETIC

#include <list>
#include <tr1/memory>
#include "board.h"
#include "epd_container.h"

namespace genetic
{
	//ELO rating constants
	const int Initial_ELO_rating = 2000;
	const float K = 20.0F;
	const float T = 400.0F;

	////////////////////////////////////////////////////////////////////////////////
	//will be used in GENERIC_EVAL_DATA to store engine status
	////////////////////////////////////////////////////////////////////////////////
	enum eval_type {
		initial,    //all the population started from this engine
		privileged, //privileged engine
		ordinary    //ordinary
	};

	template<typename T>
	class eval_type_pair
	{
	private:
		eval_type type;
		T value; 
	public:
		eval_type_pair(){}
		eval_type_pair(eval_type type_arg,T value_arg):type(type_arg),value(value_arg){}
		eval_type get_type() const { return type;}
		T get_value() const { return value; }
	};

	typedef eval_type_pair<unsigned int> eval_type_pair_uint;

	////////////////////////////////////////////////////////////////////////////////
	//will be used in GENERIC_EVAL_DATA to store wins/loses/draws
	////////////////////////////////////////////////////////////////////////////////

	class result_statistics
	{
	private:
		unsigned int wins;
		unsigned int loses;
		unsigned int draws;
	public:
		result_statistics():wins(0),loses(0),draws(0){}
		void clear() {wins = 0; loses = 0; draws = 0;}

		void score_win(){++wins;}
		void score_lose(){++loses;}
		void score_draw(){++draws;}

		unsigned int get_wins(){return wins;}
		unsigned int get_loses(){return loses;}
		unsigned int get_draws(){return draws;}

		void to_console(){cout << "w:" << get_wins() << ";d:" << get_draws() <<";l:" << get_loses();}
	};

	////////////////////////////////////////////////////////////////////////////////
	//evaluation object needs more information and statistics 
	//collecting when working with genetics algorithms
	//so we enlarge our class to work with.
	//All functions in those algorithms uses this class
	//when some eval data loaded to board's eval, it casted to
	//simple eval
	//This class also have method generic algorithms should load evaluation
	//to board with.
	//
	//Every time we create/copy generic_eval_data, new ID is set to 
	//this evaluation to identify it. So even equal evaluations will
	//have different Id's
	////////////////////////////////////////////////////////////////////////////////

	class genetic_eval_data : public eval_data
	{
	private:
		long int id;
		int ELO;

		eval_type_pair_uint priveleged_status; // value of the pair will store iteration on which this engine was privileged
		bool is_initial;

		//result_statistics score

		static long int unique_id_counter;
		void set_id(long id_arg);
	public:
		genetic_eval_data();
		genetic_eval_data(const eval_data& arg);
		genetic_eval_data(const genetic_eval_data& arg);

		void set_ELO(int val) ;
		void add_to_ELO(int add) ;
		int get_ELO() const ;

		void set_privileged_status(const eval_type_pair_uint &status);
		const eval_type_pair_uint& get_privileged_status();

		void set_initial_status(bool is_initial_arg);
		bool get_initial_status() const;

		void load_to_board(board& board_arg);

		long get_id() const ;
	};

	////////////////////algorithms with genetic_eval_data///////////////////////////////////
	//compare two engines by their ELO rating
	bool operator < (const genetic_eval_data& left,const genetic_eval_data& right);
	//change ELO ratings of two engines according to game results
	void score_victory(genetic_eval_data& winner,genetic_eval_data& looser );
	void score_draw(genetic_eval_data& first,genetic_eval_data& second);

	//////////////////////////tournament////////////////////////////////////////////////////
	//This class allows creating tournaments in easy way
	//The first and simplest scheme to play is each engine plays with any 
	//other(including itself) just once as white and once as black
	//for more complicated mechanisms think about virtual function support
	////////////////////////////////////////////////////////////////////////////////////////

	typedef std::tr1::shared_ptr<genetic_eval_data> shared_eval_data;

	const int Default_Margin_Win = 400;           // centipawns needed to claim a victory
	const unsigned int Default_Max_Game_Len = 20; //it means x2 half moves
	const unsigned int Default_Depth_Search = 2;
	const string start_position_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ";

	class tournament
	{
	private:
		std::list<shared_eval_data> engines_list;
		shared_eval_data judge;

		int margin_win;
		unsigned int max_game_len;
		unsigned int depth_search;
		string start_FEN;
		unsigned int num_of_rounds;

		board* board_ptr;
		tournament():board_ptr(NULL){};

		void set_judge(const shared_eval_data& judge_arg);
		shared_eval_data get_judge();

		//checks all the internal state of engines in search of errors
		bool is_good();
		bool init_play();

		void two_engine_battle(shared_eval_data& first,shared_eval_data& second);
	public:
		tournament(board& board_arg,const shared_eval_data& judge_arg);

		void set_margin_win(int margin_arg);
		int get_margin_win() const;

		void set_max_game_len(unsigned int full_moves);
		unsigned int get_max_game_len() const;

		void set_depth_search(unsigned int half_moves);
		unsigned int get_depth_search() const;

		void set_num_of_rounds(unsigned int rounds);
		unsigned int get_num_of_rounds() const;

		void set_start_FEN(const string& fen);
		string get_start_FEN() const;

		void add_engine(const shared_eval_data& engine_arg);
		void add_engine(const list<shared_eval_data>& engines_arg);

		void remove_all_engines();
		void play();
		list<shared_eval_data> get_best(unsigned int num);
		void save_log();
	};

	bool operator < (const shared_eval_data& first,const shared_eval_data& second);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//main class of genetic testing
	///////////////////////////////////////////////////////////////////////////////////////////////////

	const string mutation_ranges_filename = "autotuning//mutation_ranges_eval.txt";
	const string judge_eval_filename = "autotuning//judge_eval.txt";
	const string epd_filename = "autotuning//autotuning_positions.epd";
	const string log_filename = "autotuning//log.txt";
	const unsigned int Default_Priveleged_Num = 1;
	const unsigned int Default_Mutated_Num = 1;

	class evolution
	{
	private:
		std::list <shared_eval_data> total_engines;

		shared_eval_data judge_eval;
		shared_eval_data mutation_ranges_eval;
		epd_container epd;

		unsigned int priveleged_num;
		unsigned int mutated_num;
		int margin_win;
		bool is_use_epd_flag;

		board* board_ptr;

		//each engine used in this class must be registered
		bool register_engine(const shared_eval_data& engine_arg);

		void set_judge(shared_eval_data judge_eval_arg);
		shared_eval_data get_judge();

		void set_mutation_ranges(const shared_eval_data& mutation_ranges_arg );
		shared_eval_data get_mutation_ranges();

		std::list<shared_eval_data> get_all_privileged();
		std::list<shared_eval_data> get_mutated_from(std::list<shared_eval_data> source);

		evolution():board_ptr(NULL){} //default constructor is disabled
	public:
		evolution(board& board_arg);

		void set_num_of_privileged(unsigned int num);
		unsigned int get_num_of_privileged() const;

		void set_num_of_mutated(unsigned int num);
		unsigned int get_num_of_mutated() const; 

		void set_margin_win(int margin_arg);
		int get_margin_win() const;

		void set_epd_usage(bool status);
		bool is_use_epd() const;

		bool user_input();
		bool evolution_cycle();

		void save_log();
	};
}



#endif