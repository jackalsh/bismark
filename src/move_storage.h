#ifndef MOVE_STORAGE_H
#define	MOVE_STORAGE_H

#include <vector>
using namespace std;

#include "basic.h"
#include "piece.h"

///////////////////// move_storage ////////////////////////////////////////////

class hist_el
{
private:
	BIT hash;                //for repetition draw heuristic
	unsigned int fifty;      //storing last capture of draw heuristic
	piece *capture;		     //piece captured on some depth
	bool is_moved_at_past_stored_flag;
	coord* double_pawn_move; //holds coord of double pawn move from previous move
	BIT restriction_bit;     //bit holds bit spaces for locations, which makes all this position illegal if any piece can capture on
						     //usually kings locations and castling over bits
public:
	hist_el():hash(0),fifty(0),restriction_bit(0){ capture = NULL ;
												   set_moved_at_past(true);
								                   double_pawn_move = NULL;}
	void set_hash(BIT hash_arg){hash = hash_arg;}
	void xor_hash(BIT hash_arg){hash^=hash_arg;}
	BIT get_hash() {return hash;}
	void set_fifty(unsigned int fifty_arg){fifty = fifty_arg;}
	unsigned int get_fifty() {return fifty;}
	void set_capture(piece* capture_arg) { capture = capture_arg; }
	piece* get_capture() { return capture; }
	void set_moved_at_past(bool flag) {is_moved_at_past_stored_flag = flag;}
	bool is_moved_at_past() {return is_moved_at_past_stored_flag;}
	void set_double_pawn_move(coord* double_pawn_move_arg) { double_pawn_move = double_pawn_move_arg; }
	coord* get_double_pawn_move() { return double_pawn_move; }
	void set_restriction_bit(BIT restriction_bit_arg){restriction_bit = restriction_bit_arg;}
	void or_restriction_bit(BIT restriction_bit_arg){restriction_bit |= restriction_bit_arg;}
	BIT get_restriction_bit(){return restriction_bit;}

	void to_console();
};

///////////////////////////////////////////////////////////////////////////////////////////
//We found useful to use State Machine Mechanism for the search and this class provides
//interface to do so (Like OPENGL)
//
//You can use SET_STORE_DEPTH,INC_STORE_DEPTH,DEC_STORE_DEPTH to set current ply depth
//and all other functions will work and store information in structures relevant for
//this depth. We will mark all those functions with SM for clearance
//0 is a root tree depth and higher numbers is higher depth
//
///////////////////////////////////////////////////////////////////////////////////////////
class move_storage
{
private:
	//here move generator generates moves for current SM depth
    vector< vector< scored_move_el > > moves_d;	  //SM
	//here we store other data relevant for this SM depth
	vector<hist_el> hist_d;						  //SM
	//contains the upper limit for SM depth. All structures allocated to hold exact
	//this number of depths in the search
    unsigned int moves_d_size;					  //SM
	//reserved space to contain this number of moves each SM depth generated by move generator
	//if not enough data will be reallocated with speed loss so need be large enough
    unsigned int reserved_for_moves;			  //SM
    
	//current SM depth to work with. 0 is root depth or positive integer on deeper depths
    unsigned int cur_depth;						  //SM

	//store root depth of the search
	unsigned int root_depth;
    
	//if move generator generates move which possibly can capture the king(ILLEGAL)
	//this flag turns true to prevent it
    bool alert_check;
public:
    move_storage();

	//manipulate SM depth
    void set_store_depth(unsigned int depth);			//SM
	void inc_store_depth();								//SM
	void dec_store_depth();								//SM

	void set_root_depth(unsigned int depth);
	void set_this_depth_as_root();
	unsigned int get_root_depth() const;

	//clear previous generated moves for this SM depth
    void clear_stored();								//SM
	//deletes all stored data from last search on ALL SM depths
	void total_clear_data();

	//get current SM depth
    unsigned int get_store_depth();						//SM
	//add move to this SM depths. Used by move generator
    void add_move(move_el* el);							//SM
    void add_move(move_el* el,int score_arg);			//SM
	//get move with index INDEX from current SM depth
    scored_move_el& get(unsigned int index);			//SM
	//upper limit of stored moves index
    unsigned int num_moves_this_depth();				//SM
    
	//sets state of flag of CHECK alert. If after calling move generator for some SM
	//depth this flag turns true then previous move was illegal because
	//one of moves for current player can capture the king
    void enable_alert_check();
    void disable_alert_check();
    bool is_alert_checked();
    
	//sorting in descending order moves in current SM depth
    void sort_current_depth();							//SM
	//returns hist data stored for current SM depth
	hist_el& get_hist();								//SM
	//returns hist data stored for one ply upper from
	//current SM depth. User must NOT call this function
	//for root(zero) depth.
	hist_el& get_hist_previous();						//SM
	//and one SM ply deeper
	hist_el& get_hist_next();
	//returns for current SM depth if position like this was
	//met in this variation before from root of the search
	bool is_hash_reps();								//SM

	void to_console();
};

#endif	/* MOVE_STORAGE_H */

