#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

#include "board.h"
#include "rules.h"


/////////////////////rules////////////////////////////////////////////////////

rules::rules()
{
	 for(int i = 0;i<8;i++)
	  for(int j = 0;j<8;j++)
	  {
		  generate_list_Rooks(all_comb[WHITE|ROOK][i][j],j,i);
                  generate_list_Rooks(all_comb[BLACK|ROOK][i][j],j,i);     
                  generate_list_Bishops(all_comb[WHITE|BISHOP][i][j],j,i);   
                  generate_list_Bishops(all_comb[BLACK|BISHOP][i][j],j,i);  
                  generate_list_Queens(all_comb[WHITE|QUEEN][i][j],j,i);   
                  generate_list_Queens(all_comb[BLACK|QUEEN][i][j],j,i);      
                  generate_list_Knights(all_comb[WHITE|KNIGHT][i][j],j,i);   
                  generate_list_Knights(all_comb[BLACK|KNIGHT][i][j],j,i);        
                  generate_list_Kings(all_comb[WHITE|KING][i][j],j,i,WHITE);   
                  generate_list_Kings(all_comb[BLACK|KING][i][j],j,i,BLACK);  
                  generate_list_Pawns(all_comb[WHITE|PAWN][i][j],j,i,WHITE);   
                  generate_list_Pawns(all_comb[BLACK|PAWN][i][j],j,i,BLACK);                  
	  }
}


void rules::generate_list_Rooks(moves_list& list,int x,int y)
{
        assert(x>=0 && x<8 && y>=0 && y<8) ;
	vector<moves_list> ret;
        ret.resize(4);

	move_el* marker;

	for(int i = 1;i<8;i++)
	{
        if(x+i < 8)
	 {
            marker = new move_el(new coord(x,y),new coord(x+i,y),FOLLOWING_ALLOWED);
            ret[0].push_back(marker);
	 }
        if(x-i >= 0)
	 {
		 marker = new move_el(new coord(x,y),new coord(x-i,y),FOLLOWING_ALLOWED);
		 ret[1].push_back(marker);
	 }
	 if(y+i < 8)
	 {
		 marker = new move_el(new coord(x,y),new coord(x,y+i),FOLLOWING_ALLOWED);
		 ret[2].push_back(marker);
	 }
	 if(y-i >=0)
	 {
		 marker = new move_el(new coord(x,y),new coord(x,y-i),FOLLOWING_ALLOWED);
		 ret[3].push_back(marker);
	 }
	}
        
	for(int i = 0;i<4;i++)
        {
            list.push_back(ret[i],LINK_TO_END);
        }

}

void rules::generate_list_Bishops(moves_list& list,int x,int y)
{
    assert(x>=0 && x<8 && y>=0 && y<8) ;
	vector<moves_list> ret;
    ret.resize(4);

	move_el* marker;

	for(int i = 1;i<8;i++)
	{
        if(x+i < 8 && y+i < 8)
	 {
            marker = new move_el(new coord(x,y),new coord(x+i,y+i));
            ret[0].push_back(marker);
	 }
        if(x-i >= 0 && y-i >= 0)
	 {
		 marker = new move_el(new coord(x,y),new coord(x-i,y-i));
		 ret[1].push_back(marker);
	 }
	 if(x+i < 8 && y-i >= 0)
	 {
		 marker = new move_el(new coord(x,y),new coord(x+i,y-i));
		 ret[2].push_back(marker);
	 }
	 if(x-i >= 0 && y+i < 8)
	 {
		 marker = new move_el(new coord(x,y),new coord(x-i,y+i));
		 ret[3].push_back(marker);
	 }
	}
        
	for(int i = 0;i<4;i++)
        {
            list.push_back(ret[i],LINK_TO_END);
        }
}

void rules::generate_list_Queens(moves_list& list,int x,int y)
{
    assert(x>=0 && x<8 && y>=0 && y<8) ;
    moves_list diagonals;
    moves_list updowns;
    generate_list_Bishops(diagonals,x,y);
    generate_list_Rooks(updowns,x,y);
    list.push_back(diagonals,NOTHING_SPECIAL);
    list.push_back(updowns,NOTHING_SPECIAL);
}

void rules::generate_list_Knights(moves_list& list,int x,int y)
{
    assert(x>=0 && x<8 && y>=0 && y<8) ;
    moves_list temp_list;
    move_el* marker;
    
    for(int i = -2;i<=2;i++)
        for(int j = -2;j<=2;j++)
           if(i*j == 2 || i*j==-2) 
               if(x+j>=0 && x+j<8 && y+i>=0 && y+i<8)
                {
                  marker = new move_el(new coord(x,y),new coord(x+j,y+i));
                  temp_list.push_back(marker);
                }
    list.push_back(temp_list,LINK_TO_NEXT);
}

void rules::generate_list_Kings(moves_list& list,int x,int y,int color)
{
    assert(x>=0 && x<8 && y>=0 && y<8) ;
	assert(color==WHITE || color==BLACK);
    moves_list temp_list;
    move_el* marker;
    
    for(int i = -1;i<=1;i++)
        for(int j = -1;j<=1;j++)
        {
                if(i==0 && j==0) continue;
                if(x+j>=0 && x+j<8 && y+i>=0 && y+i<8)
                {
                  marker = new move_el(new coord(x,y),new coord(x+j,y+i),FOLLOWING_ALLOWED);
                  temp_list.push_back(marker);
                }
        }

    //make LINKED!!!!
	if(x == 3 && y == 0 && color==WHITE) //white king side castling
	{
	  marker = new move_el(new coord(3,0),new coord(1,0),SILENT_ONLY);
	  marker->castle_info = new castling(new coord(0,0),new coord(2,0),(BIT)14, //....00001110
																	   (BIT)6); //....00000110
	  temp_list.push_back(marker);
	}
	if(x == 3 && y == 0 && color==WHITE) //white queen side castling
	{
	  marker = new move_el(new coord(3,0),new coord(5,0),SILENT_ONLY);
	  marker->castle_info = new castling(new coord(7,0),new coord(4,0),(BIT)56,  //...00111000
																	   (BIT)112);//...01110000
	  temp_list.push_back(marker);
	}
	if(x == 3 && y == 7 && color==BLACK) //black king side castling
	{
	  marker = new move_el(new coord(3,7),new coord(1,7),SILENT_ONLY);
	  marker->castle_info = new castling(new coord(0,7),new coord(2,7),(BIT)1008806316530991104, //00001110....
																	   (BIT)432345564227567616); //00000110....

	  temp_list.push_back(marker);
	}
	if(x == 3 && y == 7 && color==BLACK) //black queen side castling
	{
	  marker = new move_el(new coord(3,7),new coord(5,7),SILENT_ONLY);
	  marker->castle_info = new castling(new coord(7,7),new coord(4,7),(BIT)4035225266123964416, //00111000...
																	   (BIT)8070450532247928832);//01110000...
	  temp_list.push_back(marker);
	}


    list.push_back(temp_list,LINK_TO_NEXT);
}

void rules::register_as_promotion( moves_list& list,const move_el& marker,int mode )
{
	moves_list temp_list;
	move_el* temp_el;

	//promotion to queen
	temp_el = new move_el(marker);
	temp_el->pawn_promoting = QUEEN;
	temp_list.push_back(temp_el);

	//you can comment or uncomment promotions
	//to enable/disable them
	
	//promotion to knight
	temp_el = new move_el(marker);
	temp_el->pawn_promoting = KNIGHT;
	temp_list.push_back(temp_el);

	//this promotion code removed for now but is necessary for perft calculation

	////promotion to bishop
	//temp_el = new move_el(marker);
	//temp_el->pawn_promoting = BISHOP;
	//temp_list.push_back(temp_el);

	////promotion to rook
	//temp_el = new move_el(marker);
	//temp_el->pawn_promoting = ROOK;
	//temp_list.push_back(temp_el);

	list.push_back(temp_list,mode);
}

void rules::generate_list_Pawns(moves_list& list,int x,int y,int color)
{
    assert(x>=0 && x<8 && y>=0 && y<8) ;
    assert(color == WHITE || color == BLACK);
    
    if(color == WHITE && y == 7) return; 
    if(color == BLACK && y == 0) return; 
    
    int special_y;int dir;int enpassan_y;int promoting_y;
    if(color == WHITE) {special_y = 1;dir = +1;enpassan_y = 4;promoting_y = 6;}
    if(color == BLACK) {special_y = 6;dir = -1;enpassan_y = 3;promoting_y = 1;}

    moves_list go_forward;
    moves_list capture;
	moves_list enpassan;
	moves_list promotion;
    move_el* marker;
    
    int forward_dist = (special_y==y)?2:1;
    for(int i = 1;i <= forward_dist;i++) //pawn silent move
        if(y+i*dir >= 0 && y+i*dir < 8) 
        {
          marker = new move_el(new coord(x,y),new coord(x,y+i*dir),SILENT_ONLY);  
		  if(i==2) marker->pawn_double_move = marker->to; //when pawn double move holds TO position
		  if(y==promoting_y) 
			  register_as_promotion(promotion,*marker,LINK_TO_END);
		  else
			  go_forward.push_back(marker);
        }
    for(int j = -1;j <= 1;j+=2) //pawn capturing
        if(x+j >= 0 && x+j < 8)
        {
          marker = new move_el(new coord(x,y),new coord(x+j,y+1*dir),CAPTURE_ONLY); 
		  if(y==promoting_y) 
			  register_as_promotion(promotion,*marker,LINK_TO_NEXT);
		  else
			  capture.push_back(marker);
        }
	for(int j = -1;j <= 1;j+=2)//enpassan
        if(x+j >= 0 && x+j < 8)
        {
		  if(y != enpassan_y) break;
			//enpassan is silent only move by definition. It means we can't capture piece on TO location. Instead of this
			//there is ENPASSAN_MOVE coord holds the captured's piece location
          marker = new move_el(new coord(x,y),new coord(x+j,y+1*dir),SILENT_ONLY);  
		  marker->enpassan_info = new coord(x+j,y);
          capture.push_back(marker);
        }
    list.push_back(capture,LINK_TO_NEXT);
    list.push_back(go_forward,LINK_TO_END);
	list.push_back(enpassan,LINK_TO_NEXT);
	list.push_back(promotion,NOTHING_SPECIAL);
}

void print_moves_list(moves_list &obj)
{
   moves_list::iterator it;
   moves_list::iterator it_copy_for_next;
   moves_list::iterator it_copy_for_stable;
   for(it = obj.begin();it!=obj.end();++it)
   {
      it_copy_for_next = it;
      it_copy_for_stable = it;
      
      it_copy_for_next.next();
      it_copy_for_stable.next_stable();
      
      cout << *(it.get()) << "->" ;
      if(it_copy_for_next!=obj.end())
          cout << *(it_copy_for_next.get());
      else
          cout << "0";
      
      
      cout << "(" ;
      if(it_copy_for_stable!=obj.end())
          cout << *(it_copy_for_stable.get());
      else
          cout << "0";
      cout << ")" <<endl;
   }
}