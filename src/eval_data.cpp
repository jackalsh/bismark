#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
using namespace std;

#include <math.h>
extern "C"
{
  #include "mt64.h"
}

#include "eval_data.h"

void eval_data::init_default()
{
  //We are copy standard values stored in CONFIG_DEFAULT to internal state, initialize GROUP 1 valriables
    DPP = double_pawn_penalty;          
	for(unsigned int i = 0;i<5;i++) 
		PIP[i] = pawn_islands_penalty[i] ;															
    PPB = passed_pawn_bonus;         
    UPB = unstopable_pawn_bonus;        
	CTEKB = close_to_enemy_king_bonus; 
	KnOB = knight_outpost_bonus;         
	RSOFB = rook_on_semi_opened_file;       
	ROFB = rook_on_opened_file;         
    BPB = bishop_pair_bonus;      

	//initialize piece-value array
	for(unsigned int i = 0;i<PIECE_ID_MAX;++i)
	{
	  switch(i)
	  { //KING value is zero
		case (BLACK | PAWN):piece_values[i] = BP_value;break;
		case (WHITE | PAWN):piece_values[i] = WP_value;break;
		case (BLACK | KNIGHT):piece_values[i] = BKn_value;break;
		case (WHITE | KNIGHT):piece_values[i] = WKn_value;break;
		case (BLACK | BISHOP):piece_values[i] = BB_value;break;
		case (WHITE | BISHOP):piece_values[i] = WB_value;break;
		case (BLACK | ROOK):piece_values[i] = BR_value;break;
		case (WHITE | ROOK):piece_values[i] = WR_value;break;
		case (BLACK | QUEEN):piece_values[i] = BQ_value;break;
		case (WHITE | QUEEN):piece_values[i] = WQ_value;break;
		default:
			piece_values[i] = 0;break;
	  }
	}

	for(int i =0;i<BOARD_SIZE;i++)
     	for(int j =0;j<BOARD_SIZE;j++)
		{
			MG_bonuses[0     ][i][j] = 0; //unused index
			MG_bonuses[PAWN  ][i][j] = /*advance_bonuses[7-i][7-j]+*/pawn_bonuses_MG[7-i][7-j];
			MG_bonuses[KNIGHT][i][j] = /*advance_bonuses[7-i][7-j]+*/knight_bonuses_MG[7-i][7-j];
			MG_bonuses[BISHOP][i][j] = /*advance_bonuses[7-i][7-j]+*/bishop_bonuses_MG[7-i][7-j];
			MG_bonuses[ROOK  ][i][j] = /*advance_bonuses[7-i][7-j]+*/rook_bonuses_MG[7-i][7-j];
			MG_bonuses[QUEEN ][i][j] = /*advance_bonuses[7-i][7-j]+*/queen_bonuses_MG[7-i][7-j];
			MG_bonuses[KING  ][i][j] = /*advance_bonuses[7-i][7-j]+*/king_bonuses_MG[7-i][7-j];

			EG_bonuses[0     ][i][j] = 0; //unused index
			EG_bonuses[PAWN  ][i][j] = /*advance_bonuses[7-i][7-j]+*/pawn_bonuses_EG[7-i][7-j];
			EG_bonuses[KNIGHT][i][j] = /*advance_bonuses[7-i][7-j]+*/knight_bonuses_EG[7-i][7-j];
			EG_bonuses[BISHOP][i][j] = /*advance_bonuses[7-i][7-j]+*/bishop_bonuses_EG[7-i][7-j];
			EG_bonuses[ROOK  ][i][j] = /*advance_bonuses[7-i][7-j]+*/rook_bonuses_EG[7-i][7-j];
			EG_bonuses[QUEEN ][i][j] = /*advance_bonuses[7-i][7-j]+*/queen_bonuses_EG[7-i][7-j];
			EG_bonuses[KING  ][i][j] = /*advance_bonuses[7-i][7-j]+*/king_bonuses_EG[7-i][7-j];

		}
  
	sync();
}

void eval_data::sync()
{
	//here we build GROUP 2 variables which are depend on GROUP 1
	for(int i =0;i<BOARD_SIZE;i++)
     	for(int j =0;j<BOARD_SIZE;j++)
		{
            //build pythagorean matrix
		    for(int from_y = 0;from_y<8;++from_y)
		      for(int from_x = 0;from_x<8;++from_x)
			  {
				  double r_sqr = (from_y-i)*(from_y-i) + (from_x-j)*(from_x-j);
				  //pithagorean_dist[i][j][from_y][from_x] = int(sqrt(r_sqr));
				  tropizm_to_enemy_king[i][j][from_y][from_x] = int(get_close_to_enemy_king_bonus()*(1-sqrt(r_sqr/49)));
			  }

			BIT right = (j > 0) ? bit_files[j-1] : BIT(0);
			BIT left = (j < 7) ? bit_files[j+1] : BIT(0);
			outpost_side_bits[i][j][0 /*WHITE*/] = up_bits[i] & (left | right);
			outpost_side_bits[i][j][1 /*BLACK*/] = down_bits[i] & (left | right);
			outpost_front_bits[i][j][0/*WHITE*/] = up_bits[i] & bit_files[j];
			outpost_front_bits[i][j][1/*BLACK*/] = down_bits[i] & bit_files[j];
		}

	max_material =  8*piece_values[WHITE|PAWN] + 2*piece_values[WHITE|KNIGHT] + 2*piece_values[WHITE|BISHOP] +
			        2*piece_values[WHITE|ROOK] + 1*piece_values[WHITE|QUEEN] +
					8*piece_values[BLACK|PAWN] + 2*piece_values[BLACK|KNIGHT] + 2*piece_values[BLACK|BISHOP] +
			        2*piece_values[BLACK|ROOK] + 1*piece_values[BLACK|QUEEN] ;
}

eval_data::eval_data()
{
	init_default();
}

int eval_data::get_value_by_id(unsigned int id) 
{
	assert(id < PIECE_ID_MAX);
	return piece_values[id];
}

int eval_data::get_value(piece* p) 
{
	assert(p != NULL);
	assert(p->is_no_piece() == false);
	return (piece_values[p->get_ID()]);
}

//this function is local and needed only for CREATE_CONFIG_FILE function
void write_to_file_table(ofstream& output_file,string name_table,int table[8][8])
{
  output_file << "const int " << name_table << "[8][8] = {   " << endl;
  for(unsigned int i = 0;i<8;++i)
  {
	  output_file << "{";
	  for(unsigned int j = 0;j<8;j++)
	  {
		  output_file << setw(4) << table[7-i][7-j];
		  if(j!=7) output_file<<",";
	  }
	  output_file << "}" ;
	  if(i!=7) output_file << "," << endl;
  }
  output_file << "  };" << endl;
}

bool eval_data::save_to_configure_file(string filename)
{
  ofstream output_file;
  output_file.open(filename.c_str());
  if(!output_file)
  {
	//cout <<"IO error! Can't generate file " << filename << endl;
	return false;
  }
  output_file << "//this is auto build configured file" << endl << endl;
  output_file << "//////////////////////material values//////////////////////////////" << endl << endl;

  output_file << "const int BP_value = " << piece_values[BLACK|PAWN] << ";" << endl;
  output_file << "const int WP_value = " << piece_values[WHITE|PAWN] << ";" << endl;
  output_file << "const int BKn_value = " << piece_values[BLACK|KNIGHT] << ";" << endl;
  output_file << "const int WKn_value = " << piece_values[WHITE|KNIGHT] << ";" << endl;
  output_file << "const int BB_value = " << piece_values[BLACK|BISHOP] << ";" << endl;
  output_file << "const int WB_value = " << piece_values[WHITE|BISHOP] << ";" << endl;
  output_file << "const int BR_value = " << piece_values[BLACK|ROOK] << ";" << endl;
  output_file << "const int WR_value = " << piece_values[WHITE|ROOK] << ";" << endl;
  output_file << "const int BQ_value = " << piece_values[BLACK|QUEEN] << ";" << endl;
  output_file << "const int WQ_value = " << piece_values[WHITE|QUEEN] << ";" << endl;

  output_file << endl;
  output_file << "//////////////followed constants relevant only for Middle Game purposes/////////////////" << endl << endl;

  write_to_file_table(output_file,"pawn_bonuses_MG",MG_bonuses[PAWN]); output_file << endl;
  write_to_file_table(output_file,"knight_bonuses_MG",MG_bonuses[KNIGHT]); output_file << endl;
  write_to_file_table(output_file,"bishop_bonuses_MG",MG_bonuses[BISHOP]); output_file << endl;
  write_to_file_table(output_file,"rook_bonuses_MG",MG_bonuses[ROOK]); output_file << endl;
  write_to_file_table(output_file,"queen_bonuses_MG",MG_bonuses[QUEEN]); output_file << endl;
  write_to_file_table(output_file,"king_bonuses_MG",MG_bonuses[KING]); output_file << endl;

  output_file << endl;
  output_file << "//////////////followed constants relevant only for End Game purposes/////////////////" << endl << endl;

  write_to_file_table(output_file,"pawn_bonuses_EG",EG_bonuses[PAWN]); output_file << endl;
  write_to_file_table(output_file,"knight_bonuses_EG",EG_bonuses[KNIGHT]); output_file << endl;
  write_to_file_table(output_file,"bishop_bonuses_EG",EG_bonuses[BISHOP]); output_file << endl;
  write_to_file_table(output_file,"rook_bonuses_EG",EG_bonuses[ROOK]); output_file << endl;
  write_to_file_table(output_file,"queen_bonuses_EG",EG_bonuses[QUEEN]); output_file << endl;
  write_to_file_table(output_file,"king_bonuses_EG",EG_bonuses[KING]); output_file << endl;

  output_file << endl;
  output_file << "const int double_pawn_penalty = " << get_double_pawn_penalty() << ";" << endl;

  output_file << "const int pawn_islands_penalty[5] = {" << get_pawn_islands_penalty(0) << "," <<
														    get_pawn_islands_penalty(1) << "," <<
														    get_pawn_islands_penalty(2) << "," <<
														    get_pawn_islands_penalty(3) << "," <<
														    get_pawn_islands_penalty(4) << "};" <<  endl;
  output_file << "const int passed_pawn_bonus = " << get_pawn_passed_bonus() << ";" << endl;
  output_file << "const int unstopable_pawn_bonus = " << get_unstopable_pawn_bonus() << ";" << endl;
  output_file << endl;
  output_file << "const int close_to_enemy_king_bonus = " << get_close_to_enemy_king_bonus() << ";" << endl;
  output_file << endl;
  output_file << "const int knight_outpost_bonus = " << get_knight_outpost_bonus() << ";" << endl;
  output_file << endl;
  output_file << "const int rook_on_semi_opened_file = " << get_rook_on_semi_opened_bonus() << ";" << endl;
  output_file << "const int rook_on_opened_file = " << get_rook_on_opened_bonus() << ";" << endl;
  output_file << endl;
  output_file << "const int bishop_pair_bonus = " << get_bishop_pair_bonus() << ";" <<  endl;


  output_file.close();
  return true;
}

//finds NAME_TABLE parameter in the OUTPUT_FILE and parsing it to TABLE[8][8]
//TODO: this function should be rewritten for better code.
bool parse_table(string& input,string name_table,int table[8][8])
{
	size_t loc = input.find(name_table);
	if(loc == string::npos)
	{
		cout << "Error! not found '" << name_table << "' in the config file" << endl;
		return false;
	}
	unsigned int brackets = 0;
	unsigned int y = 0,x = 0;
	unsigned int last_special = 0;
	bool entered_block = false;
	for(size_t scan = loc+name_table.size();scan<input.size();++scan)
	{
		if(input[scan] == '{') {entered_block = true;brackets++;}
		if(input[scan] == '}') {brackets--;}

		if(brackets == 0 && entered_block)
		{
			if(y!=7 || x!=7)
			{
				cout << "Error! while parsing '" << name_table << "' in the config file" << endl;
				return false;
			}
		    break;
		}

		if((input[scan] == '}' || (input[scan] == ',') && brackets != 1))
		{
			table[7-y][7-x] = string_to_int(input.substr(last_special+1,scan-last_special-1));
			//cout << "(" << y << "," <<x<<";"<<table[7-y][7-x]<<")"<<endl;
		}
		if(input[scan] == '{' || input[scan] == ',')
		{
			last_special = scan;
		}
		if(input[scan] == ',')
		{
			if(brackets == 1) {y++;x=0;}
			if(brackets == 2) x++;
		}

		if(brackets>2 || y>7 || x>7)
		{
			cout << "Error! while parsing '" << name_table << "' in the config file" << endl;
			return false;
		}
	}

	return true;
}

//this function searches for ...NAME_PARAMETER...=VALUE;
bool parse_parameter(string& input,string name_parameter,int &value)
{
	size_t loc = input.find(name_parameter);
	if(loc == string::npos)
	{
		cout << "Error! not found '" << name_parameter << "' in the config file" << endl;
		return false;
	}
	size_t eq=string::npos;
	size_t stop=string::npos;
	bool everething_is_found = false;
	for(size_t scan = loc+name_parameter.size();scan<input.size();++scan)
	{
		if(input[scan] == '=') eq = scan;
		if(input[scan] == ';') stop = scan;

		if(eq!=string::npos && stop!=string::npos)
		{
			everething_is_found = true;
			break;
		}
	}
	if(!everething_is_found || stop<=eq)
	{
		cout << "Error! while parsing '" << name_parameter << "' in the config file" << endl;
		return false;
	}
	value = string_to_int(input.substr(eq+1,stop-eq-1));
	//cout << value<<endl;
	return true;
}

//this function searches for ...NAME_PARAMETER...{ROW[0],ROW[1],etc}
bool parse_row(string& input,string name_parameter,int row[],unsigned int row_size)
{
	size_t loc = input.find(name_parameter);
	if(loc == string::npos)
	{
		cout << "Error! not found '" << name_parameter << "' in the config file" << endl;
		return false;
	}
	bool found_open = false;
	bool found_close = false;
	size_t last_special = string::npos;
	unsigned int x = 0;
	char ch;
	for(size_t scan = loc+name_parameter.size();scan<input.size();++scan)
	{
		ch = input[scan];
		if(input[scan] == '{') {found_open = true;last_special = scan;}
		if(input[scan] == '}') found_close = true;

		if((input[scan] == ',' || input[scan] == '}') && found_open)
		{
			row[x] = string_to_int(input.substr(last_special+1,scan-last_special-1));
			//cout << row[x] << ",";
			last_special = scan;
			if((++x)>=row_size) break;
		}
	}
	if(!found_open || !found_close || x!=row_size)
	{
		cout << "Error! while parsing '" << name_parameter << "' in the config file" << endl;
		return false;
	}
	return true;
}

bool eval_data::load_from_configure_file(string filename)
{
  ifstream input_file;
  input_file.open(filename.c_str());
  if(!input_file)
  {
	//cout <<"IO error! Can't open file " << filename << endl;
	return false;
  }
  string input;
  char c;
  while(!(input_file.eof()))
  {
	c = input_file.get();
	input+=c;
  }

  bool success = true;
  success &= parse_parameter(input,"BP_value ",piece_values[BLACK|PAWN]);
  success &= parse_parameter(input,"WP_value ",piece_values[WHITE|PAWN]);
  success &= parse_parameter(input,"BKn_value ",piece_values[BLACK|KNIGHT]);
  success &= parse_parameter(input,"WKn_value ",piece_values[WHITE|KNIGHT]);
  success &= parse_parameter(input,"BB_value ",piece_values[BLACK|BISHOP]);
  success &= parse_parameter(input,"WB_value ",piece_values[WHITE|BISHOP]);
  success &= parse_parameter(input,"BR_value ",piece_values[BLACK|ROOK]);
  success &= parse_parameter(input,"WR_value ",piece_values[WHITE|ROOK]);
  success &= parse_parameter(input,"BQ_value ",piece_values[BLACK|QUEEN]);
  success &= parse_parameter(input,"WQ_value ",piece_values[WHITE|QUEEN]);

  success &= parse_table(input,"pawn_bonuses_MG",MG_bonuses[PAWN]);
  success &= parse_table(input,"knight_bonuses_MG",MG_bonuses[KNIGHT]);
  success &= parse_table(input,"bishop_bonuses_MG",MG_bonuses[BISHOP]);
  success &= parse_table(input,"rook_bonuses_MG",MG_bonuses[ROOK]);
  success &= parse_table(input,"queen_bonuses_MG",MG_bonuses[QUEEN]);
  success &= parse_table(input,"king_bonuses_MG",MG_bonuses[KING]);

  success &= parse_table(input,"pawn_bonuses_EG",EG_bonuses[PAWN]);
  success &= parse_table(input,"knight_bonuses_EG",EG_bonuses[KNIGHT]);
  success &= parse_table(input,"bishop_bonuses_EG",EG_bonuses[BISHOP]);
  success &= parse_table(input,"rook_bonuses_EG",EG_bonuses[ROOK]);
  success &= parse_table(input,"queen_bonuses_EG",EG_bonuses[QUEEN]);
  success &= parse_table(input,"king_bonuses_EG",EG_bonuses[KING]);

  success &= parse_parameter(input,"double_pawn_penalty",DPP);
  success &= parse_row(input,"pawn_islands_penalty",PIP,5);
  success &= parse_parameter(input,"passed_pawn_bonus",PPB);
  success &= parse_parameter(input,"unstopable_pawn_bonus",UPB);
  success &= parse_parameter(input,"close_to_enemy_king_bonus",CTEKB);
  success &= parse_parameter(input,"knight_outpost_bonus",KnOB);
  success &= parse_parameter(input,"rook_on_semi_opened_file",RSOFB);
  success &= parse_parameter(input,"rook_on_opened_file",ROFB);
  success &= parse_parameter(input,"bishop_pair_bonus ",BPB);

  sync();
  return success;
}

void eval_data::mutate(eval_data& mutate_ranges_factor)
{
	/*for(unsigned int i = 0;i<PIECE_ID_MAX;++i)
		piece_values[i] += random_pos_neg_range(mutate_ranges_factor.piece_values[i]);*/
	for(int digni = PAWN; digni <= QUEEN;++digni)
	{
		if(mutate_ranges_factor.piece_values[BLACK | digni] == 
		   mutate_ranges_factor.piece_values[WHITE | digni] )
		{//if mutation ranges are equal use synetrical method
			int range_temp = random_pos_neg_range(mutate_ranges_factor.piece_values[BLACK | digni]); //they are equal
			piece_values[BLACK | digni] += range_temp;
			piece_values[WHITE | digni] += range_temp;
		}
		else
		{
			piece_values[BLACK | digni] += random_pos_neg_range(mutate_ranges_factor.piece_values[BLACK | digni]);
			piece_values[WHITE | digni] += random_pos_neg_range(mutate_ranges_factor.piece_values[WHITE | digni]);
		}
	}

    DPP += random_pos_neg_range(mutate_ranges_factor.DPP);
	for(unsigned int i = 0;i<5;++i)				                    
		PIP[i] += random_pos_neg_range(mutate_ranges_factor.PIP[i]);												
    PPB += random_pos_neg_range(mutate_ranges_factor.PPB);
    UPB +=  random_pos_neg_range(mutate_ranges_factor.UPB);   
	CTEKB += random_pos_neg_range(mutate_ranges_factor.CTEKB);
	KnOB += random_pos_neg_range(mutate_ranges_factor.KnOB);      
	RSOFB += random_pos_neg_range(mutate_ranges_factor.RSOFB);
	ROFB += random_pos_neg_range(mutate_ranges_factor.ROFB);       
    BPB += random_pos_neg_range(mutate_ranges_factor.BPB);

	for(unsigned int dign = 0;dign<PIECE_DIGNITY_MAX;++dign)
		for(unsigned int i = 0;i<BOARD_SIZE;++i)
			for(unsigned int j = 0;j<BOARD_SIZE;++j)
			{
				MG_bonuses[dign][i][j] += random_pos_neg_range(mutate_ranges_factor.MG_bonuses[dign][i][j]);
				EG_bonuses[dign][i][j] += random_pos_neg_range(mutate_ranges_factor.EG_bonuses[dign][i][j]);
			}

	sync();
}