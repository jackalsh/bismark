//This file is a default configuration file for engine. All the "magic numbers" should
//be defined in it. 
//          

/////////////////////material values//////////////////////////////

const int BP_value =  100;
const int WP_value =  100;
const int BKn_value = 300;
const int WKn_value = 300;
const int BB_value =  300;
const int WB_value =  300;
const int BR_value =  500;
const int WR_value =  500;
const int BQ_value =  900;
const int WQ_value =  900;

//those values are added to all piece's PST to encourage the advance
//const int advance_bonuses[8][8] = { { 7 , 7 , 7 , 7 , 7 , 7 , 7 , 7},
//									{ 6 , 6 , 6 , 6 , 6 , 6 , 6 , 6},
//									{ 5 , 5 , 6 , 6 , 6 , 6 , 5 , 5},
//									{ 4 , 4 , 5 , 5 , 5 , 5 , 4 , 4},
//									{ 3 , 3 , 4 , 4 , 4 , 4 , 3 , 3},
//									{ 2 , 2 , 3 , 3 , 3 , 3 , 2 , 2},
//									{ 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1},
//									{ 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0}  };

//////////////followed constants relevant only for Middle Game purposes/////////////////

const int pawn_bonuses_MG[8][8] = {     { 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  },
										{ 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 },
										{ 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 },
										{ 5  , 5  , 10 , 25 , 25 , 10 , 5  , 5  },
										{ 0  , 0  , 0  , 20 , 20 , 0  , 0  , 0  },
										{ 5  ,-5  , 10 , 0  , 0  ,-10 ,-5  , 5  },
										{ 5  , 10 , 10 ,-20 ,-20 , 10 , 10 , 5  },
										{ 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  }  };

const int knight_bonuses_MG[8][8] =   { {-50 ,-40 ,-30 ,-30 ,-30 ,-30 ,-40 ,-50 },
										{-40 ,-20 , 0  , 0  , 0  , 0  ,-20 ,-40 },
										{-30 , 0  , 10 , 15 , 15 , 10 , 0  ,-30 },
										{-30 , 5  , 15 , 20 , 20 , 15 , 5  ,-30 },
										{-30 , 0  , 15 , 20 , 20 , 15 , 0  ,-30 },
										{-30 , 5  , 10 , 15 , 15 , 10 , 5  ,-30 },
										{-40 ,-20 , 0  , 5  , 5  , 0  ,-20 ,-40 },
										{-50 ,-40 ,-30 ,-30 ,-30 ,-30 ,-40 ,-50 }  };

const int bishop_bonuses_MG[8][8] =   { {-20 ,-10 ,-10 ,-10 ,-10 ,-10 ,-10 ,-20 },
										{-10 , 0  , 0  , 0  , 0  , 0  , 0  ,-10 },
										{-10 , 0  , 5  , 10 , 10 , 5  , 0  ,-10 },
										{-10 , 5  , 5  , 10 , 10 , 5  , 5  ,-10 },
										{-10 , 0  , 10 , 10 , 10 , 10 , 0  ,-10 },
										{-10 , 10 , 10 , 10 , 10 , 10 , 10 ,-10 },
										{-10 , 5  , 0  , 0  , 0  , 0  , 5  ,-10 },
										{-20 ,-10 ,-10 ,-10 ,-10 ,-10 ,-10 ,-20 }  };

const int rook_bonuses_MG[8][8] =     { { 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  },
										{ 5  , 10 , 10 , 10 , 10 , 10 , 10 , 5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{ 0  , 0  , 0  , 5  , 5  , 0  , 0  , 0  }  };

const int queen_bonuses_MG[8][8] =    { {-20 ,-10 ,-10 ,-5  ,-5  ,-10 ,-10 ,-20 },
										{-10 , 0  , 0  , 0  , 0  , 0  , 0  ,-10 },
										{-10 , 0  , 5  , 5  , 5  , 5  , 0  ,-10 },
										{-5  , 0  , 5  , 5  , 5  , 5  , 0  ,-5  },
										{ 0  , 0  , 5  , 5  , 5  , 5  , 0  , 0  },
										{-10 , 5  , 5  , 5  , 5  , 5  , 0  ,-10 },
										{-10 , 0  , 5  , 0  , 0  , 0  , 0  ,-10 },
										{-20 ,-10 ,-10 ,-5  ,-5  ,-10 ,-10 ,-20 }  };

const int king_bonuses_MG[8][8] =     { {-30 ,-40 ,-40 ,-50 ,-50 ,-40 ,-40 ,-30 }, //middlegame only!
										{-30 ,-40 ,-40 ,-50 ,-50 ,-40 ,-40 ,-30 },
										{-30 ,-40 ,-40 ,-50 ,-50 ,-40 ,-40 ,-30 },
										{-30 ,-40 ,-40 ,-50 ,-50 ,-40 ,-40 ,-30 },
										{-20 ,-30 ,-30 ,-40 ,-40 ,-30 ,-30 ,-20 },
										{-10 ,-20 ,-20 ,-20 ,-20 ,-20 ,-20 ,-10 },
										{ 20 , 20 , 0  , 0  , 0  , 0  , 20 , 20 },
										{ 20 , 30 , 10 , 0  , 0  , 10 , 30 , 20 }  };

//////////////followed constants relevant only for End Game purposes/////////////////

const int pawn_bonuses_EG[8][8] =     { { 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  },
										{ 150, 150, 150, 150, 150, 150, 150, 150},
										{ 75 , 75 , 75 , 75 , 75 , 75 , 75 , 75 },
										{ 30 , 30 , 30 , 30 , 30 , 30 , 30 , 30 },
										{ 0  , 0  , 0  , 20 , 20 , 0  , 0  , 0  },
										{ 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  },
										{ 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  },
										{ 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  }  };

const int knight_bonuses_EG[8][8] =   { {-50 ,-40 ,-30 ,-30 ,-30 ,-30 ,-40 ,-50 },
										{-40 ,-20 , 0  , 0  , 0  , 0  ,-20 ,-40 },
										{-30 , 0  , 10 , 15 , 15 , 10 , 0  ,-30 },
										{-30 , 5  , 15 , 20 , 20 , 15 , 5  ,-30 },
										{-30 , 0  , 15 , 20 , 20 , 15 , 0  ,-30 },
										{-30 , 5  , 10 , 15 , 15 , 10 , 5  ,-30 },
										{-40 ,-20 , 0  , 5  , 5  , 0  ,-20 ,-40 },
										{-50 ,-40 ,-30 ,-30 ,-30 ,-30 ,-40 ,-50 }  };

const int bishop_bonuses_EG[8][8] =   { {-20 ,-10 ,-10 ,-10 ,-10 ,-10 ,-10 ,-20 },
										{-10 , 0  , 0  , 0  , 0  , 0  , 0  ,-10 },
										{-10 , 0  , 5  , 10 , 10 , 5  , 0  ,-10 },
										{-10 , 5  , 5  , 10 , 10 , 5  , 5  ,-10 },
										{-10 , 0  , 10 , 10 , 10 , 10 , 0  ,-10 },
										{-10 , 10 , 10 , 10 , 10 , 10 , 10 ,-10 },
										{-10 , 5  , 0  , 0  , 0  , 0  , 5  ,-10 },
										{-20 ,-10 ,-10 ,-10 ,-10 ,-10 ,-10 ,-20 }  };

const int rook_bonuses_EG[8][8] =     { { 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  },
										{ 5  , 10 , 10 , 10 , 10 , 10 , 10 , 5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{-5  , 0  , 0  , 0  , 0  , 0  , 0  ,-5  },
										{ 0  , 0  , 0  , 5  , 5  , 0  , 0  , 0  }  };

const int queen_bonuses_EG[8][8] =    { {-20 ,-10 ,-10 ,-5  ,-5  ,-10 ,-10 ,-20 },
										{-10 , 0  , 0  , 0  , 0  , 0  , 0  ,-10 },
										{-10 , 0  , 5  , 5  , 5  , 5  , 0  ,-10 },
										{-5  , 0  , 5  , 5  , 5  , 5  , 0  ,-5  },
										{ 0  , 0  , 5  , 5  , 5  , 5  , 0  , 0  },
										{-10 , 5  , 5  , 5  , 5  , 5  , 0  ,-10 },
										{-10 , 0  , 5  , 0  , 0  , 0  , 0  ,-10 },
										{-20 ,-10 ,-10 ,-5  ,-5  ,-10 ,-10 ,-20 }  };

const int king_bonuses_EG[8][8] =     { {-50 ,-40 ,-30 ,-20 ,-20 ,-30 ,-40 ,-50 },
										{-30 ,-20 ,-10 ,  0 ,  0 ,-10 ,-20 ,-30 },
										{-30 ,-10 , 20 , 30 , 30 , 20 ,-10 ,-30 },
										{-30 ,-10 , 30 , 40 , 40 , 30 ,-10 ,-30 },
										{-30 ,-10 , 30 , 40 , 40 , 30 ,-10 ,-30 },
										{-30 ,-10 , 20 , 30 , 30 , 20 ,-10 ,-30 },
										{-30 ,-30 ,  0 ,  0 ,  0 ,  0 ,-30 ,-30 },
										{-50 ,-30 ,-30 ,-30 ,-30 ,-30 ,-30 ,-50 } };

//////////////////////////////////////////////////////////////////////////////////////

const int double_pawn_penalty = -20;
const int pawn_islands_penalty[5] = {0, 0 , -5 , -10 , -15}; //for number of pawn islands. those numbers are penalties
															 //when 3 islands it P[3].when 4 islands it's P[4] and so on. P[0] is no used
															 //and P[1] is default state of the game begining
const int passed_pawn_bonus = 10; //there is no ANY pawns in front of the PAWNS
const int unstopable_pawn_bonus = 50; //there is no pawns in front and at the sides of it

const int close_to_enemy_king_bonus = 20; 

const int knight_outpost_bonus = 10;//10;

//if no own side pawn before the rook get the bonus
const int rook_on_semi_opened_file = 10;//10;
//if also no enemy pawn get another bonus
const int rook_on_opened_file = 20;//20;

const int bishop_pair_bonus = 20;//20;