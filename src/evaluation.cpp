#include <iostream> 
using namespace std;

#include "board.h"

#define _DEBUG_EVAL false

int board::position_evaluation()
{
  vector<piece*>::iterator it;
  piece* p_p;
  int score = 0;
  int score_pawn = 0;
  int score_PST_MG = 0;
  int score_PST_EG = 0;
  int tropizm = 0;

  m_eval.pawns.clear();
  m_eval.material.clear();
  for(it = pieces.begin();it!=pieces.end();++it)
  {
      p_p = *it;
      if(p_p->is_no_piece()) continue;
      //int y_for_side = p_p->get_color()==WHITE ? p_p->get_y_position() : 7-p_p->get_y_position();
	  //piece* enemy_king = (p_p->get_color() == WHITE ? black_king : white_king);
      if(p_p->get_color() == WHITE)
      {
		  m_eval.material.add_to_white(m_eval.get_value(p_p));
		  score_PST_MG += m_eval.MG_bonuses[p_p->get_digni()][p_p->get_y_position()][p_p->get_x_position()];
		  score_PST_EG += m_eval.EG_bonuses[p_p->get_digni()][p_p->get_y_position()][p_p->get_x_position()];
		  tropizm+=m_eval.tropizm_to_enemy_king[black_king->get_y_position()][black_king->get_x_position()][p_p->get_y_position()][p_p->get_x_position()];
      }
      else
      {
		  m_eval.material.add_to_black(m_eval.get_value(p_p));
		  score_PST_MG-= m_eval.MG_bonuses[p_p->get_digni()][7-p_p->get_y_position()][p_p->get_x_position()];
		  score_PST_EG-= m_eval.EG_bonuses[p_p->get_digni()][7-p_p->get_y_position()][p_p->get_x_position()];
		  tropizm-=m_eval.tropizm_to_enemy_king[white_king->get_y_position()][white_king->get_x_position()][p_p->get_y_position()][p_p->get_x_position()];
      }

	  if(p_p->get_ID() == (WHITE|PAWN))
	  {
		  m_eval.pawns.or_white_with(p_p->get_bit_position());//building pawns bitboards
	  }
	  if(p_p->get_ID() == (BLACK|PAWN))
	  {
		  m_eval.pawns.or_black_with(p_p->get_bit_position());
	  }
          
  }

  score+=m_eval.material.get_white()-m_eval.material.get_black();
  if(_DEBUG_EVAL) 
	  cout << "material only score: " << m_eval.material.get_white()-m_eval.material.get_black()<<endl;

  float game_state = float(m_eval.material.get_white()+m_eval.material.get_black())/float(m_eval.max_material);
  if(game_state > 1) game_state = 1; //may happen when promoting pawn
  assert(game_state>=0); 
  score += int((score_PST_MG + tropizm)*(game_state) + score_PST_EG*(1-game_state));
  if(_DEBUG_EVAL) 
  {
	  cout << "game phase> middlegame : " <<  game_state << ", endgame : " << 1-game_state << endl;
	  cout << "positional PST> middlegame : " << ((turn == BLACK) ? -(score_PST_MG + tropizm) : (score_PST_MG + tropizm)) <<
						    ", endgame : " <<  ((turn == BLACK) ? -score_PST_EG : score_PST_EG) << endl;
	  cout << "king tropizm:"<<((turn == BLACK) ? -tropizm : tropizm) << endl;
  }

  bool scan_white,scan_black;

  bool islands_flag_white = false;
  bool islands_flag_black = false;
  unsigned int islands_count_white = 0;
  unsigned int islands_count_black = 0;

  unsigned int bishop_count_white = 0;
  unsigned int bishop_count_black = 0;

  for(unsigned int i= 0;i<8;i++)
  {
	  scan_white = m_eval.pawns.is_white_pawn_on_file(i);
	  scan_black = m_eval.pawns.is_black_pawn_on_file(i);

	  if(scan_white)                     //pawn islands
	  {
		  if(islands_flag_white == false) { islands_count_white ++; islands_flag_white = true; }
	  }
	  else
		  islands_flag_white = false;

	  if(scan_black)
	  {
		  if(islands_flag_black == false) { islands_count_black ++; islands_flag_black = true; }
	  }
	  else
		  islands_flag_black = false;
  }

  score_pawn+=m_eval.get_pawn_islands_penalty(islands_count_white)-m_eval.get_pawn_islands_penalty(islands_count_black);

  if(_DEBUG_EVAL) {
	cout << "white have " << islands_count_white << " islands " <<endl;
	cout << "black have " << islands_count_black << " islands " << endl;
  }

  for(it = pieces.begin();it!=pieces.end();++it)
  {
      p_p = *it;
      if(p_p->is_no_piece()) continue;
	  if(p_p->get_ID() == (WHITE|PAWN))
	  {
		  if(outpost_front_bits[p_p->get_y_position()][p_p->get_x_position()][0 /*WHITE*/] & m_eval.pawns.get_white_bit())
		  { //there is friendly pawn in front of this pawn. So this pawn get's doubled pawn penalty and can't be passed
			  score_pawn += m_eval.get_double_pawn_penalty();
			  if(_DEBUG_EVAL) {cout << "white have double pawn on " ; p_p->to_console_chess_position() ; cout << endl;}
		  }
		  else if( (outpost_front_bits[p_p->get_y_position()][p_p->get_x_position()][0 /*WHITE*/] & m_eval.pawns.get_black_bit()) == BIT(0) )
		  {//check if it's passed pawn candidate
			  if( (outpost_side_bits[p_p->get_y_position()][p_p->get_x_position()][0 /*WHITE*/] & m_eval.pawns.get_black_bit()) == BIT(0) )
			  {
				  score_pawn += m_eval.get_unstopable_pawn_bonus();
				  if(_DEBUG_EVAL) {cout << "white have unstopable pawn on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
			  else
			  {
				  score_pawn += m_eval.get_pawn_passed_bonus();
				  if(_DEBUG_EVAL) {cout << "white have passed pawn candidate on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
		  }
	  }
	  else if(p_p->get_ID() == (BLACK|PAWN))
	  {
		  if(outpost_front_bits[p_p->get_y_position()][p_p->get_x_position()][1 /*BLACK*/] & m_eval.pawns.get_black_bit())
		  { //there is friendly pawn in front of this pawn. So this pawn get's doubled pawn penalty and can't be passed
			  score_pawn -= m_eval.get_double_pawn_penalty();
			  if(_DEBUG_EVAL) {cout << "black have double pawn on " ; p_p->to_console_chess_position() ; cout << endl;}
		  }
		  else if( (outpost_front_bits[p_p->get_y_position()][p_p->get_x_position()][1 /*BLACK*/] & m_eval.pawns.get_white_bit()) == BIT(0) )
		  {//check if it's passed pawn candidate
			  if( (outpost_side_bits[p_p->get_y_position()][p_p->get_x_position()][1 /*BLACK*/] & m_eval.pawns.get_white_bit()) == BIT(0) )
			  {
				  score_pawn -= m_eval.get_unstopable_pawn_bonus();
				  if(_DEBUG_EVAL) {cout << "black have unstopable pawn on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
			  else
			  {
				  score_pawn -= m_eval.get_pawn_passed_bonus();
				  if(_DEBUG_EVAL) {cout << "black have passed pawn candidate on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
		  }
	  }
	  else if(p_p->get_ID() == (WHITE|KNIGHT))
	  {
		  if( (outpost_side_bits[p_p->get_y_position()][p_p->get_x_position()][0 /*WHITE*/] & m_eval.pawns.get_black_bit()) == BIT(0) )
		  {
			  score_pawn += m_eval.get_knight_outpost_bonus();
			  if(_DEBUG_EVAL) {cout << "white have knight outpost on " ; p_p->to_console_chess_position() ; cout << endl;}
		  }
	  }
	  else if(p_p->get_ID() == (BLACK|KNIGHT))
	  {
		  if( (outpost_side_bits[p_p->get_y_position()][p_p->get_x_position()][1 /*BLACK*/] & m_eval.pawns.get_white_bit()) == BIT(0) )
		  {
			  score_pawn -= m_eval.get_knight_outpost_bonus();
			  if(_DEBUG_EVAL) {cout << "black have knight outpost on " ; p_p->to_console_chess_position() ; cout << endl;}
		  }
	  }
	  else if(p_p->get_ID() == (WHITE|ROOK)) 
	  {
		  if( ( bit_files[p_p->get_x_position()] & m_eval.pawns.get_white_bit()) == BIT(0) )
		  {		  
			  if((bit_files[p_p->get_x_position()] & m_eval.pawns.get_black_bit()) == BIT(0))
			  {
				  score_pawn += m_eval.get_rook_on_opened_bonus();
				  if(_DEBUG_EVAL) {cout << "white rook on opened file on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
			  else
			  {
				  score_pawn += m_eval.get_rook_on_semi_opened_bonus();
				  if(_DEBUG_EVAL) {cout << "white rook on semi opened file on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
		  }
	  }
	  else if(p_p->get_ID() == (BLACK|ROOK)) 
	  {
		  if( ( bit_files[p_p->get_x_position()] & m_eval.pawns.get_black_bit()) == BIT(0) )
		  {
			  if((bit_files[p_p->get_x_position()] & m_eval.pawns.get_white_bit()) == BIT(0))
			  {
				  score_pawn -= m_eval.get_rook_on_opened_bonus();
				  if(_DEBUG_EVAL) {cout << "black rook on opened file on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
			  else
			  {
				  score_pawn -= m_eval.get_rook_on_semi_opened_bonus();
				  if(_DEBUG_EVAL) {cout << "black rook on semi opened file on " ; p_p->to_console_chess_position() ; cout << endl;}
			  }
		  }
	  }
	  else if(p_p->get_ID() == (WHITE|BISHOP))
	  {
		  bishop_count_white++;
	  }
	  else if(p_p->get_ID() == (BLACK|BISHOP))
	  {
		  bishop_count_black++;
	  }
  }

  if(bishop_count_white>=2)//HACK: we suppose that two bishops can't be on the same color
  {
	  score += m_eval.get_bishop_pair_bonus();
	  if(_DEBUG_EVAL) {cout << "white have bishop pair" << endl;}
  }

  if(bishop_count_black>=2)//HACK: we suppose that two bishops can't be on the same color
  {
	  score -= m_eval.get_bishop_pair_bonus();
	  if(_DEBUG_EVAL) {cout << "black have bishop pair" << endl;}
  } 

  if(_DEBUG_EVAL)
  {
	  cout << "pawn structure bonus : " << ((turn == BLACK) ? -score_pawn:score_pawn) <<endl;
  }
  score += score_pawn;

  if(turn == BLACK)
	  score = -score;

  return score;
}