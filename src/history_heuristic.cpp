#include <iostream>
using namespace std;

#include "history_heuristic.h"

//clears old data
void history_heuristic::clear()
{
	for(unsigned int i = 0;i<BOARD_SIZE;++i)
		for(unsigned int j = 0;j<BOARD_SIZE;++j)
			for(unsigned int m = 0;m<BOARD_SIZE;++m)
				for(unsigned int n = 0;n<BOARD_SIZE;++n)
				{
					mat[i][j][m][n] = 0;
				}
}

history_heuristic::history_heuristic(void)
{
	clear();
}

//add good history heuristic move to table
void history_heuristic::add_good_move(move_el* move,int depth)
{
	assert(move!=NULL && depth > 0);
	mat[move->from->y][move->from->x][move->to->y][move->to->x] += depth; //TODO: other scemes? 

	if(mat[move->from->y][move->from->x][move->to->y][move->to->x] >= (INFINITY_EVAL-MAX_TREE_DEPTH))
		overflow_repair();
}

//get's a history heuristic value of this move
unsigned int history_heuristic::get_move_value(move_el* move)
{
	assert(move!=NULL);
	assert(mat[move->from->y][move->from->x][move->to->y][move->to->x] < (INFINITY_EVAL-MAX_TREE_DEPTH));

	return (mat[move->from->y][move->from->x][move->to->y][move->to->x]);
}

void history_heuristic::to_console_info()
{
	cout << "history heuristic:" << endl;

	unsigned int max_val=0;
	move_el max(NULL,NULL);

	unsigned int min_val = INFINITY_EVAL - MAX_TREE_DEPTH;
	move_el min(NULL,NULL);

	bool corrupted = false;
	bool is_max_found = false;
	bool is_min_found = false;

	for(unsigned int i = 0;i<BOARD_SIZE;++i)
		for(unsigned int j = 0;j<BOARD_SIZE;++j)
			for(unsigned int m = 0;m<BOARD_SIZE;++m)
				for(unsigned int n = 0;n<BOARD_SIZE;++n)
				{
					if(mat[i][j][m][n] >= (INFINITY_EVAL-MAX_TREE_DEPTH))
					{
						corrupted = true;
						continue;
					}
					if(mat[i][j][m][n] > max_val)
					{
						max_val = mat[i][j][m][n];
						max = move_el(new coord(j,i),new coord(n,m));
						is_max_found = true;
					}
					if(mat[i][j][m][n] <= min_val && mat[i][j][m][n]!=0)
					{
						min_val = mat[i][j][m][n];
						min = move_el(new coord(j,i),new coord(n,m));
						is_min_found = true;
					}
				}
	if(corrupted)
		cout << " history array is corrupted!!!" << endl;
	else
	{
		if(is_max_found)
			cout << " best move in history heuristic array " << max << endl;
		if(is_min_found)
			cout << " poorest move in history heuristic array " << min << endl;
	}
}

//To prevent overflow we devide all table's elements be 2
//Overflow occurs very rarely in the program
void history_heuristic::overflow_repair()
{
	for(unsigned int i = 0;i<BOARD_SIZE;++i)
		for(unsigned int j = 0;j<BOARD_SIZE;++j)
			for(unsigned int m = 0;m<BOARD_SIZE;++m)
				for(unsigned int n = 0;n<BOARD_SIZE;++n)
				{
					mat[i][j][m][n] >>= 1;
				}

	//just to get feeling when it's happen
	//delete in the future this line
	cout << "history table overflow" << endl;
}