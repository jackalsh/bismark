#include <iostream>
#include <algorithm>
#include <iomanip>
#include <string>
#include <math.h>
#include <map>
using namespace std;

//#include "uci_protocol.h"
#include "board.h"

board::board()
{
  for(int i = 0;i<BOARD_SIZE;i++)
	for(int j = 0;j<BOARD_SIZE;j++)
	{
		mat[i][j].full_coord = new coord(j,i);
	}

 clear_game();

 if(!m_hash.rebuild_table(DEFAULT_HASH_BITSIZE))
	exit(1);
 m_pv.clear();
 m_hh.clear();
 m_kh.clear();

 if(load_user_custom_eval())
	 cout << "PAY ATTENTION : " << default_custom_eval_filename << " file found in the directory and custom evaluation is loaded "  <<
		     "from it by default. If this isn't what you want delete it from the directory or rename" << endl;
}


BIT board::bit_from_xy(int x,int y)
{
	return mat[y][x].full_coord->bit;
}

void board::add_piece(int color,int digni,int x,int y)
{
 if(mat[y][x].p_piece!=NULL)
	 return; //another piece on this pos
 if(color == WHITE && digni == KING && white_king!=NULL) return;
 if(color == BLACK && digni == KING && black_king!=NULL) return;

 piece *temp = new piece();
 temp->set_color(color);
 temp->set_digni(digni);
 coord* pos = mat[y][x].full_coord;
 temp->set_position(pos);
 mat[y][x].p_piece = temp; //put piece on board
 pieces.push_back(temp); //put piece to pieces list

 if(color == WHITE)
	 turn_side_bit = turn_side_bit | pos->bit;
  if(color == BLACK)
	 not_turn_bit = not_turn_bit | pos->bit;

  if(color == WHITE && digni == KING) white_king = temp;
  if(color == BLACK && digni == KING) black_king = temp;

}

void board::clear_disabled_pieces()
{
	unsigned int index = 0;
	while(index < pieces.size())
	{
		if(pieces[index]->is_no_piece())
		{
			pieces.erase(pieces.begin()+index);
			continue;
		}
		index++;
	}
}

void board::clear_game()
{
	set_turn_color(WHITE);
	for(unsigned int i=0;i<pieces.size();i++) delete pieces[i];
	pieces.clear();
	turn_side_bit = (BIT)0;
	not_turn_bit = (BIT)0;
	for(int i = 0;i<BOARD_SIZE;i++)
		for(int j = 0;j<BOARD_SIZE;j++)
		{
			mat[i][j].p_piece = NULL;
		}
    white_king = NULL;
    black_king = NULL;
}

void board::allow_castling_rights(int side,int direction)
{
	//by default all pieces's moved at once flag to true by default
	//this flag is relevant only to king and rook to prevent castling
	//so we want to set this flag to false to allow castling
	piece *king = NULL,*rook = NULL;

	if(side == WHITE && direction == KING_SIDE)
	{
		king = mat[0][3].p_piece;
		rook = mat[0][0].p_piece;
	}
	if(side == WHITE && direction == QUEEN_SIDE)
	{
		king = mat[0][3].p_piece;
		rook = mat[0][7].p_piece;
	}
	if(side == BLACK && direction == KING_SIDE)
	{
		king = mat[7][3].p_piece;
		rook = mat[7][0].p_piece;
	}
	if(side == BLACK && direction == QUEEN_SIDE)
	{
		king = mat[7][3].p_piece;
		rook = mat[7][7].p_piece;
	}
	if(king == NULL || rook == NULL)
		cout << "error: enabling castling rights on this position !" << endl;
	assert(king != NULL && rook != NULL);

	king->disable_moved_at_past_flag();
	rook->disable_moved_at_past_flag();

}

bool board::check_castling_rights(int side,int direction)
{
    piece *king = NULL,*rook = NULL;

	if(side == WHITE && direction == KING_SIDE)
	{
		king = mat[0][3].p_piece;
		rook = mat[0][0].p_piece;
	}
	if(side == WHITE && direction == QUEEN_SIDE)
	{
		king = mat[0][3].p_piece;
		rook = mat[0][7].p_piece;
	}
	if(side == BLACK && direction == KING_SIDE)
	{
		king = mat[7][3].p_piece;
		rook = mat[7][0].p_piece;
	}
	if(side == BLACK && direction == QUEEN_SIDE)
	{
		king = mat[7][3].p_piece;
		rook = mat[7][7].p_piece;
	}
	if(king == NULL || rook == NULL)
		return false;
	if(king->is_moved_at_past() || rook->is_moved_at_past() )
		return false;

	//another rules
	//TODO: another rules?

	return true;
}

void board::init_game()
{
	string initial_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
	init_game_FEN(initial_FEN);
}

void board::init_game_FEN( const string& FEN_str )
{
	clear_game();
	//vector <piece> pieces_to_add;
	string pos_fen1 = get_Nth_word(FEN_str,1);
	int position_on_board = 0;
	for(unsigned int i = 0;i<pos_fen1.size();i++)
	{
		if(pos_fen1[i] == ' ') continue;
		int x = (63-position_on_board)%8;
		int y = (63-position_on_board)/8;

		int num = pos_fen1[i]-'0';
		if(num>=1 && num<=8) 
		{
			position_on_board+=num;
			continue;
		}

		switch(pos_fen1[i])
		{
		case 'R':add_piece(WHITE,ROOK,x,y);position_on_board++;break;
		case 'N':add_piece(WHITE,KNIGHT,x,y);position_on_board++;break;
		case 'B':add_piece(WHITE,BISHOP,x,y);position_on_board++;break;
		case 'Q':add_piece(WHITE,QUEEN,x,y);position_on_board++;break;
		case 'K':add_piece(WHITE,KING,x,y);position_on_board++;break;
		case 'P':add_piece(WHITE,PAWN,x,y);position_on_board++;break;
		case 'r':add_piece(BLACK,ROOK,x,y);position_on_board++;break;
		case 'n':add_piece(BLACK,KNIGHT,x,y);position_on_board++;break;
		case 'b':add_piece(BLACK,BISHOP,x,y);position_on_board++;break;
		case 'q':add_piece(BLACK,QUEEN,x,y);position_on_board++;break;
		case 'k':add_piece(BLACK,KING,x,y);position_on_board++;break;
		case 'p':add_piece(BLACK,PAWN,x,y);position_on_board++;break;
		case '\\':position_on_board+=8-x;break;
		}

	}
	string pos_fen2 = get_Nth_word(FEN_str,2);
	if(pos_fen2 == "w") 	set_turn_color(WHITE);
	if(pos_fen2 == "b") 	{
		set_turn_color(WHITE);
		swap_sides();
	}

	string pos_fen3 = get_Nth_word(FEN_str,3);
	if(pos_fen3.find("K")!= string::npos)  //White can castle king side
		allow_castling_rights(WHITE,KING_SIDE);
	if(pos_fen3.find("k")!= string::npos)  //Black can castle king side
		allow_castling_rights(BLACK,KING_SIDE);
	if(pos_fen3.find("Q")!= string::npos)  //White can castle queen side
		allow_castling_rights(WHITE,QUEEN_SIDE);
	if(pos_fen3.find("q")!= string::npos)  //Black can castle queen side
		allow_castling_rights(BLACK,QUEEN_SIDE);

	string pos_fen4 = get_Nth_word(FEN_str,4);
	coord* start_pos_double_move_coord = NULL;
	if(pos_fen4 != "-")
	{ //it's a string in format "d6", for example
		unsigned int vfile = letter_to_file_index(pos_fen4[0]);
		start_pos_double_move_coord = find_double_moved_pawn(vfile);    
	}

	//delete all stored data from previous search
	//this is the only place we set depth to zero.
	m_store.total_clear_data();	
	m_store.get_hist().set_hash(calculate_hashkey_for_current_position()); //store hash for root
	m_store.get_hist().set_double_pawn_move(start_pos_double_move_coord);
	m_store.get_hist().set_fifty(0);
	//this can cause a problem if starting position is illegal?
	m_store.get_hist().set_restriction_bit(0); 
}

void board::to_console()
{
	cout << endl;
	if(get_turn_color()==WHITE)
		cout << "It's whites turn"<<endl ;
	else if(get_turn_color() == BLACK)
		cout << "It's blacks turn"<<endl ;
	else cout << "Turn undeclared";
	if(check_castling_rights(WHITE,KING_SIDE))
		cout << "White can castle kingside" << endl; 
	if(check_castling_rights(WHITE,QUEEN_SIDE))
		cout << "White can castle queenside" << endl; 
	if(check_castling_rights(BLACK,KING_SIDE))
		cout << "Black can castle kingside" << endl; 
	if(check_castling_rights(BLACK,QUEEN_SIDE))
		cout << "Black can castle queenside" << endl; 

	cout << endl << "---------------------------------" << endl;
	for(int i = BOARD_SIZE-1;i>=0;i--)
	{
		cout << "|";
		for(int j = BOARD_SIZE-1;j>=0;j--)
		{
			if(mat[i][j].p_piece!=NULL)
			  mat[i][j].p_piece->to_console();
			else
			  cout << "   ";
			cout << "|";
		}
		cout << endl << "---------------------------------" << endl;
	}
}

void board::to_console_info()
{
	cout << "best found line ";
	m_pv.to_console_current();cout << endl;
	m_hash.to_console_info();
	m_hh.to_console_info();
	m_kh.to_console_info();
	m_store.to_console();
}

void board::swap_sides()
{
	set_turn_color( (get_turn_color()==WHITE)?BLACK:WHITE);
	//turn = turn ^ COLOR_BITS;
	turn_side_bit = turn_side_bit ^ not_turn_bit;
    not_turn_bit = not_turn_bit ^ turn_side_bit;
	turn_side_bit = turn_side_bit ^ not_turn_bit;
}

int board::get_turn_color()
{
	return turn;
}

void board::set_turn_color(int color)
{
	turn = color;
}

void board::do_move_low(move_el* move)
{
	//we suppose there is correct piece of turn side color on from square and
	//if this move is capture captured piece is of opposed sign
	piece* moved = mat[move->from->y][move->from->x].p_piece;
	assert(moved!=NULL && moved->is_no_piece() == false && moved->get_color() == turn);
	//generating hash update because of moving only piece and changing side to move 
	BIT update_hash = m_hash.random_table[moved->get_ID()][move->from->y][move->from->x][moved->get_color()==WHITE ? 0 : 1] ^
					  m_hash.random_table[moved->get_ID()][move->to->y][move->to->x][moved->get_color()==WHITE ? 0 : 1] ^
				      m_hash.black_turn_random;

	piece* captured;
	//captured store captured piece anywhere it is - on TO position or ENPASSANT one
	if(move->enpassan_info == NULL) //can't be both because enpassant is a SILENT_ONLY move
		captured = mat[move->to->y][move->to->x].p_piece; //store captured piece as ordinary capture
	else
		captured = mat[move->enpassan_info->y][move->enpassan_info->x].p_piece; //store captured piece as enpassant capture

	if(captured != NULL) //now we can operate with CAPTURED even if it ENPASSAN(can't assume it on TO position)
	{//on capture  
		assert(captured->get_color() != turn && captured->is_no_piece() == false);

		not_turn_bit = not_turn_bit ^ captured->get_bit_position();		   //invert bits for player
		//generating hash update because of capturing piece
		update_hash ^=  m_hash.random_table[captured->get_ID()][captured->get_y_position()][captured->get_x_position()][captured->get_color()==WHITE ? 0 : 1];
		mat[captured->get_y_position()][captured->get_x_position()].p_piece = NULL; //need only on enpassan capture

		captured->set_no_piece(); //disable captured piece for future calculations(we can't get it's X,Y from here)
	}
	mat[move->to->y][move->to->x].p_piece = moved; //move pointers in MAT structure
    mat[move->from->y][move->from->x].p_piece = NULL;

	moved->set_position(move->to);     //update position inside PIECE
	turn_side_bit = turn_side_bit ^ (move->from->bit | move->to->bit); //invert bits for opponent

	if(move->castle_info!=NULL) //if this move is castling
	{ //move rook also
		piece* rook = mat[move->castle_info->rook_from->y][move->castle_info->rook_from->x].p_piece;
		mat[move->castle_info->rook_to->y][move->castle_info->rook_to->x].p_piece = rook;
		mat[move->castle_info->rook_from->y][move->castle_info->rook_from->x].p_piece = NULL;
		rook->set_position(move->castle_info->rook_to);
		turn_side_bit = turn_side_bit ^ (move->castle_info->rook_from->bit | move->castle_info->rook_to->bit);

		//if castling is allowed from move generator we can assume rook didn't moved yet
		rook->enable_moved_at_past_flag();
		//generating hash update because of rook moving
		update_hash ^= m_hash.random_table[rook->get_ID()][move->castle_info->rook_from->y][move->castle_info->rook_from->x][rook->get_color()==WHITE ? 0 : 1] ^
			           m_hash.random_table[rook->get_ID()][move->castle_info->rook_to->y][move->castle_info->rook_to->x][rook->get_color()==WHITE ? 0 : 1];
	}

	if(move->pawn_promoting != NO_PIECE)
	{
		update_hash ^= m_hash.promoting_pawn_inverse[move->pawn_promoting][move->to->y][move->to->x];
		moved->set_digni(move->pawn_promoting);
	}

    swap_sides(); //from here TURN variable no more the same color as moved piece!!

    //get hist of current SM depth to store information about this move
	hist_el& history = m_store.get_hist(); 
	//store information about captured piece(if it is) to correctly
	//restore it by UNDO_MOVE_LOW function
	history.set_capture(captured);

	hist_el& history_next = m_store.get_hist_next();
	//update hash of this position for draw repetition heuristic and other
	//we keep on each depth hash of the position which changes inclemently
	history_next.set_hash(history.get_hash() ^ update_hash);
	assert((history.get_hash() ^ update_hash) == calculate_hashkey_for_current_position());
	history_next.set_double_pawn_move(move->pawn_double_move);
	history_next.set_restriction_bit(white_king->get_bit_position() | black_king->get_bit_position());
	if(move->castle_info!=NULL)
		history_next.or_restriction_bit(move->castle_info->attack_restr_bit); //TODO: can we make it in move->castle_info!=NULL block upstairs

	if(captured || moved->is_digni(PAWN))
	{   //capture or pawn move makes positions before theoretical unreachable for future variations(proof?)
		//so we don't need to look for reps on depth before current which we store as FIFTY data
		history_next.set_fifty(m_store.get_store_depth()+1);
	}
	else
	{
		//otherwise we don't change last FIFTY depth and leaves it as on previous depth
		history_next.set_fifty(history.get_fifty());
	}

	//only if this mechanism is enabled - ROOK or KING only
	if(move->is_enable_move_following)
	{
		history.set_moved_at_past(moved->is_moved_at_past());
		moved->enable_moved_at_past_flag();
	}

	m_store.inc_store_depth(); //must be at the function's end
}

void board::undo_move_low(move_el* move)
{
	 m_store.dec_store_depth(); //must be the first instruction

    //redo all operations DO_MOVE_LOW(...) do with same parameters
    swap_sides();
	hist_el& history = m_store.get_hist(); 
	piece *moved = mat[move->to->y][move->to->x].p_piece;
	piece *captured = history.get_capture();

	if(move->pawn_promoting != NO_PIECE)
	{
		moved->set_digni(PAWN);
	}

	mat[move->from->y][move->from->x].p_piece = moved;
	mat[move->from->y][move->from->x].p_piece->set_position(move->from);
	mat[move->to->y][move->to->x].p_piece = NULL; 

	coord* capture_restored_coord;
	//restore captured piece no mater where it was - ordinary capture or enpassant 
	if(move->enpassan_info == NULL)
		capture_restored_coord = move->to;
	else
		capture_restored_coord = move->enpassan_info;

	if(captured != NULL) 
	{
		mat[capture_restored_coord->y][capture_restored_coord->x].p_piece = captured;
		captured->set_position(capture_restored_coord);//activates piece after SET_NO_PIECE()
		not_turn_bit = not_turn_bit ^ capture_restored_coord->bit;
	}
    turn_side_bit = turn_side_bit ^ (move->from->bit | move->to->bit);

	if(move->castle_info!=NULL)
	{
		mat[move->castle_info->rook_from->y][move->castle_info->rook_from->x].p_piece =
			mat[move->castle_info->rook_to->y][move->castle_info->rook_to->x].p_piece;
		mat[move->castle_info->rook_to->y][move->castle_info->rook_to->x].p_piece = NULL;
		mat[move->castle_info->rook_from->y][move->castle_info->rook_from->x].p_piece->set_position(move->castle_info->rook_from);
		turn_side_bit = turn_side_bit ^ (move->castle_info->rook_from->bit | move->castle_info->rook_to->bit);

		//if castling is allowed from move generator we can assume rook was'nt woved before
		mat[move->castle_info->rook_from->y][move->castle_info->rook_from->x].p_piece->disable_moved_at_past_flag();
		//enable_castle(mat[move->from->y][move->from->x].p_piece->get_color());//restore future castlings for that side
	}

	//only if this mechanism is enabled - ROOK KING
    if(move->is_enable_move_following)
	{
		mat[move->from->y][move->from->x].p_piece->set_moved_at_past_flag(history.is_moved_at_past());
	}

}

void board::move(int from_x,int from_y,int to_x,int to_y)
{
    //this function realizes higher logic of moving.
    //It checks legality of move by the rules of chess
	if(mat[from_y][from_x].p_piece == NULL) return;//if there is no piece on
                                                       //move is illegal
	if((mat[from_y][from_x].p_piece->get_color())!=get_turn_color()) return;
                                //only piece of move color can move

	if(mat[to_y][to_x].p_piece != NULL)//can't capture same color piece or the king
		if((mat[to_y][to_x].p_piece->get_color())==get_turn_color() || 
		   (mat[to_y][to_x].p_piece->is_digni(KING))) return;

	generate_moves();

	bool found = false;
	unsigned int size = m_store.num_moves_this_depth();
    for(unsigned int i = 0;i<size;i++)
	{
		if(m_store.get(i).get_move_el()->from->x == from_x &&
		   m_store.get(i).get_move_el()->from->y == from_y &&
		   m_store.get(i).get_move_el()->to  ->x == to_x   &&
		   m_store.get(i).get_move_el()->to  ->y == to_y   )
		{
		    do_move_low(m_store.get(i).get_move_el());
			found = true;
			break;
		}
	}

	move_el move = move_el(mat[from_y][from_x].full_coord,mat[to_y][to_x].full_coord);

	if(!found || m_store.is_alert_checked())
		std::cout << "error: illegal move " 
				<< move
				<< std::endl;
}

void board::move(move_el* move_arg)
{
	assert(move_arg != NULL);
	int from_x = move_arg->from->x;
	int from_y = move_arg->from->y;
	int to_x = move_arg->to->x;
	int to_y = move_arg->to->y;
    
	move(from_x,from_y,to_x,to_y);
}

void board::generate_moves()
{
    m_store.clear_stored();			//delete old moves stored in this depth before
    m_store.disable_alert_check();	//if alert_check will be enabled after building those moves this position is
									//illegal cause opponent didn't prevented the check
    
    unsigned int num_pieces = pieces.size();
    piece* p_p = NULL;
    move_el *m_p = NULL;
    for(unsigned int i = 0;i<num_pieces;i++) //loop over all pieces
    {
        
        p_p = pieces[i];
        if(p_p->is_no_piece()) continue; //only enabled(not captured) pieces
        if(p_p->is_color(turn) == false) continue;//and only of current player's move
        
		//getting list structure
        moves_list& list_for_piece = m_rules.all_comb[p_p->get_ID()][p_p->get_y_position()][p_p->get_x_position()];
        moves_list::iterator it ;
        
        for(it = list_for_piece.begin();it!=list_for_piece.end();)
        {
           m_p = it.get();
           
           if(m_p->to->bit & turn_side_bit) //can't capture your own piece
           {
             it.next_stable();
             continue;
           }          
           
           if( (  (m_p->silent_only  &  not_turn_bit) ||				//ONLY silent move can't be capture enemy's piece
                  (m_p->capture_only & ~not_turn_bit) ) == false   )	//ONLY capture move can't be silent
           {//all moves in this block are pseudo legal chess moves
                      if(m_p->to->bit &									//capturing king move!!! enable flag for illegal position			
                        (m_store.get_hist().get_restriction_bit()))
                      {
                          m_store.enable_alert_check();
                          return;
                      }

					  if(m_p->enpassan_info!=NULL)
					  {
						  coord* pawn_double_move_coord = m_store.get_hist().get_double_pawn_move();
						  if(pawn_double_move_coord==NULL) {it.next();continue;}
						  BIT ep_allowed_bit =   m_p->enpassan_info->bit &
							  pawn_double_move_coord->bit &
							  not_turn_bit;
						  if(ep_allowed_bit == (BIT)0)  {it.next();continue;} //is really needed (may by for nullmove heuristic)
					  }
					  
					  if(m_p->castle_info!=NULL)
					  {
						  piece* rook = mat[m_p->castle_info->rook_from->y][m_p->castle_info->rook_from->x].p_piece;
						  if((m_p->castle_info->move_restr_bit & (not_turn_bit | turn_side_bit)) ||
							 (rook==NULL) ||
						     (rook->get_ID() != (p_p->get_color() | ROOK)) ||
							 (p_p->is_moved_at_past()) || (rook->is_moved_at_past()/*must rook!=NULL check before*/ )) 
						  {
							  it.next();
							  continue;
						  }
					  }

					  //and here is totally legal moves
                      m_store.add_move(m_p);//store this move in move generator for use
           }
		   else if(m_p->capture_only & m_store.get_hist().get_restriction_bit())
		   {//special case when king castling when pawn checks him and avoiding this check.
			//ordinary mechanism don't work here.
				   m_store.enable_alert_check();
				   return;
		   }

           if(m_p->to->bit & not_turn_bit)
               it.next_stable();//capture
           else
               it.next(); //silent move
        }
    }
}

void board::generate_moves_captures_only()
{
    m_store.clear_stored();
    m_store.disable_alert_check();
    
    unsigned int num_pieces = pieces.size();
    piece* p_p = NULL;
    move_el *m_p = NULL;
    for(unsigned int i = 0;i<num_pieces;i++)
    {
        
        p_p = pieces[i];
        if(p_p->is_no_piece()) continue;
        if(p_p->is_color(turn) == false) continue;
        
        moves_list& list_for_piece = m_rules.all_comb[p_p->get_ID()][p_p->get_y_position()][p_p->get_x_position()];
        moves_list::iterator it ;
        
        for(it = list_for_piece.begin();it!=list_for_piece.end();)
        {
           m_p = it.get();
           
           if(m_p->to->bit & turn_side_bit) //can't capture your own piece
           {
             it.next_stable();
             continue;
           }           
           
           if( m_p->to->bit & not_turn_bit )  //on capture only
               if( (m_p->to->bit & m_p->silent_only)==false)
                {
                      if(m_p->to->bit &		  
                         (m_store.get_hist().get_restriction_bit()))
                      {
                          m_store.enable_alert_check();
                          return;
                      }
                   
                      m_store.add_move(m_p);  
                }

           if(m_p->to->bit & not_turn_bit)
               it.next_stable();//capture
           else
               it.next(); //silent move
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////
//try to sort moves generated by move generator for current SM depth using all
//relevant information to ensure better alphabeta cutting
//If IS_PV is allowed, trying to find the move from PV line in all possible moves list.
//IMPORTANT: Nowhere what, score this pv-node gets MUST be the greatest to be on the top of
//moves stack. Possible bugs if not.
//Is pv changes to false this means we out of main PV line 
//
/////////////////////////////////////////////////////////////////////////////////////////////////
void board::sort_moves_by_importance(bool& is_pv,move_el* hash_best_move) 
{
    unsigned int size = m_store.num_moves_this_depth();
	bool is_found_pv = false;
    int score_temp;
	//all over all moves generated by move generator for this depth
    for(unsigned int i=0;i<size;i++)
    {
        score_temp = 0;
        scored_move_el &cur_move = m_store.get(i);

		//if we gave to function information about this node from previous search
		if(is_pv && m_pv.compare_with_followed(m_store.get_store_depth(),cur_move))
		{ //first priority is PV moves from previous iteration
				score_temp += 7*INFINITY_EVAL;
				is_found_pv = true;
		}
		else if(is_equal(cur_move.get_move_el(),hash_best_move))
		{ //second are moves from hash
			    score_temp += 6*INFINITY_EVAL;
		}
		else if(cur_move.get_move_el()->to->bit & not_turn_bit)
		{ //captures. This scheme is MVA/LVV is quite primitive and should be divided to winning captures analyzes, good and other else
				 int lva_mvv = (get_absolute_value(mat[cur_move.get_move_el()->to->y][cur_move.get_move_el()->to->x].p_piece->get_ID()) << 4) -
						        get_absolute_value(mat[cur_move.get_move_el()->from->y][cur_move.get_move_el()->from->x].p_piece->get_ID());
				 assert(abs(lva_mvv) < INFINITY_EVAL);
				 score_temp+=4*INFINITY_EVAL + lva_mvv;
		}
		else if( m_kh.is_first_killer(cur_move,m_store.get_store_depth()))
		{// from silent moves we first look at those from killer table. The first killer
			  score_temp+=2*INFINITY_EVAL + 1 ;
		}
		else if( m_kh.is_second_killer(cur_move,m_store.get_store_depth()))
		{// from silent moves we first look at those from killer table. The second killer
			  score_temp+=2*INFINITY_EVAL ;
		}
	    else
	    {//other silent moves are sorted by history heuristic
			      assert(m_hh.get_move_value(cur_move.get_move_el()) < INFINITY_EVAL);
			      score_temp+=m_hh.get_move_value(cur_move.get_move_el());
				  //score_temp+= //old and non effective method can be used instead.
				  //		mat[cur_move.get_move_el()->from->y][cur_move.get_move_el()->from->x].p_piece->value();
		}

        cur_move.set_score(score_temp);   
    }

	if(is_found_pv == false)
		is_pv = false;

	//sort moves that first will go the strongest
    m_store.sort_current_depth();
}

void board::sort_moves_by_importance_captures_only()
{
    unsigned int size = m_store.num_moves_this_depth();
    int lva_mvv;
    for(unsigned int i=0;i<size;i++)
    {
        lva_mvv = 0;
        scored_move_el &cur_move = m_store.get(i);        
        if(cur_move.get_move_el()->to->bit & not_turn_bit)
		{
			lva_mvv =	(get_absolute_value(mat[cur_move.get_move_el()->to->y][cur_move.get_move_el()->to->x].p_piece->get_ID()) << 4) -
						 get_absolute_value(mat[cur_move.get_move_el()->from->y][cur_move.get_move_el()->from->x].p_piece->get_ID());
			assert(abs(lva_mvv) < INFINITY_EVAL);
			//score_temp+=m_eval.get_value(mat[cur_move.get_move_el()->to->y][cur_move.get_move_el()->to->x].p_piece);
		}

        cur_move.set_score(lva_mvv);   
    }
    m_store.sort_current_depth();
}

void board::print_available_moves()
{
	unsigned int score = fw_search(false,1,INFINITE_SEARCH);

	if(score == CHECK_INVALID)
	{//check for legality of current position
		cout << "error: this position is illegal chess position" << endl;
		return;
	}

	unsigned int c_enter = 0;
    for(unsigned int i=0;i<m_store.num_moves_this_depth();i++)
    {
        scored_move_el &cur_move = m_store.get(i);

		//this move is illegal because king is under check
		if(cur_move.get_score() == - CHECK_INVALID)
			continue;

        cout << *(cur_move.get_move_el())  << "   ";
		if((++c_enter) % 4 == 0) cout << endl;
    }
	cout << endl << "------" << endl;
	cout << " total " << c_enter << " moves in this position " << endl;
}


//slow function we use only for ASSERT checking and root hash initializing.
//Zobrist key of the position updated inclemently
BIT board::calculate_hashkey_for_current_position()
{
	vector<piece*>::iterator it;
	BIT hash_key = BIT(0);
	for(it = pieces.begin();it!=pieces.end();++it)
	{
		if((*it)->is_no_piece()) continue;
		hash_key^=m_hash.random_table[(*it)->get_ID()][(*it)->get_y_position()][(*it)->get_x_position()][(*it)->get_color()==WHITE ? 0 : 1];
	}
	if(turn == BLACK) hash_key^=m_hash.black_turn_random;
	return hash_key;
}

string board::get_fen_notation()
{
	string res;
	res.reserve(80);
	unsigned count_last = 0;
	char ch_p=0;char diff_upper = 'A'-'a';
	for(int i = 7;i>=0;--i)
	{
		for(int j = 7;j>=0;--j)
		{
			if(mat[i][j].p_piece == NULL)
				count_last++;
			else
			{
				if(count_last!=0) res+=char('0'+count_last);
				count_last = 0;
				switch(mat[i][j].p_piece->get_digni())
				{
					case PAWN: ch_p = 'p';break;
					case KNIGHT: ch_p = 'n';break;
					case BISHOP: ch_p = 'b';break;
					case ROOK: ch_p = 'r';break;
					case QUEEN: ch_p = 'q';break;
					case KING: ch_p = 'k';break;
				}
				if(mat[i][j].p_piece->get_color() == WHITE) ch_p+=diff_upper;
		        res+=ch_p;
			}
		}
		if(count_last!=0) res+=char('0'+count_last);
		count_last = 0;
		if(i!=0) res+=char('/');
	}

	res += " ";
	if(turn == WHITE) 
		res+="w";
	else
		res+="b";
	res+=" ";

	if(check_castling_rights(WHITE,KING_SIDE))
		res+="K";
	if(check_castling_rights(WHITE,QUEEN_SIDE))
		res+="Q";
	if(check_castling_rights(BLACK,KING_SIDE))
		res+="k";
	if(check_castling_rights(BLACK,QUEEN_SIDE))
		res+="q";

	return res;
}

coord* board::find_double_moved_pawn(unsigned int vfile)
{
	if(vfile >= 8) return NULL;
	unsigned int y = (turn == WHITE)? 4 : 3;
	int dir_back = (turn == WHITE)?-1 : 1;
	//if user want to enable double pawn move for this file to enable enpassent move
	//this move must be legal in some way
	//it means there is pawn on correct square and there is no other pieces 2 squares backward.
	//if it fails, inform user, this position is illegal to set.
	if(mat[y][vfile].p_piece != NULL && mat[y][vfile].p_piece->get_color() != turn && 
		mat[y][vfile].p_piece->is_digni(PAWN) &&
		mat[y+1*dir_back][vfile].p_piece == NULL && 
		mat[y+2*dir_back][vfile].p_piece == NULL     )
	{
		return (mat[y][vfile].full_coord);
	}
	else
	{
		cout << "error: pawn double moved on previous move was illegal" << endl;
	}
	return NULL;
}

void board::do_null_move_low()
{
	hist_el& history = m_store.get_hist();
	hist_el& history_next = m_store.get_hist_next();

	history_next.set_hash(history.get_hash() ^ m_hash.black_turn_random);
	history_next.set_fifty(m_store.get_store_depth()+1);
	history_next.set_double_pawn_move(NULL); //enpassant not allowed at NULL MOVE search root
	history_next.set_restriction_bit(white_king->get_bit_position() | black_king->get_bit_position());

	swap_sides();
	assert((history_next.get_hash()) == calculate_hashkey_for_current_position());

	m_store.inc_store_depth();  //must be at end
}

void board::undo_null_move_low()
{
	m_store.dec_store_depth(); //must be at begin
	swap_sides();
}

bool board::is_score_checkmate(int score)
{
	return(abs(score) >= (INFINITY_EVAL-MAX_TREE_DEPTH)) ;
}


//to keep it as simple as we can, there is no zungzwang if side to move have at
//least on non pawn piece
bool board::is_zungzwang_danger()
{
	vector<piece*>::iterator it;
	for(it = pieces.begin();it!=pieces.end();++it)
	{
		piece* p = (*it);
		if(p->is_no_piece() || p->get_digni() == KING || p->get_digni() == PAWN)
			continue;
		if(p->get_color() == turn)
			return false;
	}
	return true;
}

void board::store_in_hash_table(int score_arg,int depth_arg,move_el* move_arg,score_mode mode_arg)
{
	assert(int(m_store.get_hist().get_hash() & m_hash.mask) < m_hash.table.size());
	hash_table_el& hash = m_hash.table[m_store.get_hist().get_hash() & m_hash.mask];

	if(depth_arg >= hash.get_depth()) //TODO: check other schemes.
	{

		//main purpose of this table is for move sorting. So don't replace valuable beta and exact
		//hash scores by alpha scores without best move!
		if(move_arg == NULL && hash.get_move()!=NULL) 
			return;

		//checkmate value correction
		if(score_arg >= (INFINITY_EVAL-MAX_TREE_DEPTH))
			score_arg += m_store.get_store_depth();
		else if(score_arg <= (-INFINITY_EVAL+MAX_TREE_DEPTH))
			score_arg -= m_store.get_store_depth();

		hash.set_key(m_store.get_hist().get_hash());
		hash.set_score(score_arg);
		hash.set_depth(depth_arg);
		hash.set_mode(mode_arg);
		hash.set_move(move_arg);
	}

}

void board::restore_from_hash_table(int &alpha,int &beta,int depth_arg,move_el* &move_arg)
{
	assert(int(m_store.get_hist().get_hash() & m_hash.mask) < m_hash.table.size());
	hash_table_el& hash = m_hash.table[m_store.get_hist().get_hash() & m_hash.mask];
	if(hash.get_key() == m_store.get_hist().get_hash() )
	{
		move_arg = hash.get_move();//if found in hash, use stored best move for sorting

		if(hash.get_depth()>=depth_arg) //if it also good enough to use it for cutting
		{
			int score_corrected = hash.get_score();
			if(score_corrected >= (INFINITY_EVAL-MAX_TREE_DEPTH))
			    score_corrected -= m_store.get_store_depth();
		    else if(score_corrected <= (-INFINITY_EVAL+MAX_TREE_DEPTH))
			    score_corrected += m_store.get_store_depth();

			if (hash.get_mode() == inside) 
				alpha = beta = score_corrected;
			else if (hash.get_mode() == lower_eq) 
			{
				if (score_corrected < beta)
				{
					if (score_corrected <= alpha) alpha = beta = score_corrected;
					else beta = score_corrected;
				}
			}
			else
			{
				if (score_corrected > alpha)
				{
					if (hash.get_score() >= beta) alpha = beta = score_corrected;
					else alpha = score_corrected;
				}
			}
		}
	}
	else
		move_arg = NULL;
}

void board::change_hash_size(unsigned int new_size)
{
	if(!m_hash.rebuild_table(new_size))
		exit(1);
}

bool board::save_to_configure_file(string filename)
{
	return m_eval.save_to_configure_file(filename);
}

bool board::load_from_configure_file(string filename)
{
	return m_eval.load_from_configure_file(filename);
}


//this function uses current board position to parse SAN notation.
//TODO: teach function to understand castling notation
bool board::is_best_move_SAN(string SAN_arg)
{
   remove_spaces_from(SAN_arg);

   if(SAN_arg.size() == 0 || m_pv.is_any_lines() == false) return false;
   move_el* play = m_pv.get_last_line().get_first_move();
   if(play==_NULL_EL) return false;

   //now we will parse SAN notation.
   //we will devide to 4 categories
   int p_ch;
   size_t cap_sign;
   switch(SAN_arg[0])
   {
	case('N'):p_ch = KNIGHT;break;
	case('B'):p_ch = BISHOP;break;
	case('R'):p_ch = ROOK;break;
	case('Q'):p_ch = QUEEN;break;
	case('K'):p_ch = KING;break;
	default:
		p_ch = PAWN;break;
   }
   cap_sign = SAN_arg.find('x');
   unsigned int from_x = 8;unsigned int from_y = 8;
   unsigned int to_x = 8;unsigned int to_y = 8;

   for(int i = SAN_arg.size()-1;i>=0;--i)
   {
	   if(is_file(SAN_arg[i]))
	   {
		   if(to_x == 8)
			   to_x = letter_to_file_index(SAN_arg[i]);
		   else if(from_x == 8)
			   from_x = letter_to_file_index(SAN_arg[i]);
		   else
		   {
			   cout << "Error!";
			   return false;
		   }
	   }
	   if(is_rank(SAN_arg[i]))
	   {
		   if(to_y == 8)
			   to_y = letter_to_rank_index(SAN_arg[i]);
		   else if(from_y == 8)
			   from_y = letter_to_rank_index(SAN_arg[i]);
		   else
		   {
			   cout << "Error!";
			   return false;
		   }
	   }
   }

   //TODO: castling! O-O-O , O-O

   //now when we know the best move from EPD solution, we compare it to best move from the search
   m_store.set_store_depth(0);   // TODO: need refactoring because of new way we work with trees 
   unsigned int size = m_store.num_moves_this_depth();
   move_el* filter_move = NULL;
   unsigned int count_filtered = 0;
   for(unsigned int i = 0;i<size;i++)
   {
	   scored_move_el& cur_move  = m_store.get(i);
	   if(cur_move.get_score() == MINUS_CHECK_INVALID) continue;
	   assert(mat[cur_move.get_move_el()->from->y][cur_move.get_move_el()->from->x].p_piece != NULL);// if fails then board was changed
	   if(mat[cur_move.get_move_el()->from->y][cur_move.get_move_el()->from->x].p_piece->get_digni() != p_ch) continue;
	   if((mat[cur_move.get_move_el()->to->y][cur_move.get_move_el()->to->x].p_piece != NULL && cap_sign == string::npos) ||
		  (mat[cur_move.get_move_el()->to->y][cur_move.get_move_el()->to->x].p_piece == NULL && cap_sign != string::npos) ) continue;
	   if(to_x != 8 && cur_move.get_move_el()->to->x != to_x) continue;
	   if(to_y != 8 && cur_move.get_move_el()->to->y != to_y) continue;
	   if(from_x != 8 && cur_move.get_move_el()->from->x != from_x) continue;
	   if(from_y != 8 && cur_move.get_move_el()->from->y != from_y) continue;

	   filter_move = cur_move.get_move_el();
	   count_filtered++;
   }

   if(count_filtered == 0) return false;
   //cout << "database: " << *(filter_move)<<"("<<SAN_arg<<")" << ", engine report:" << *(play) <<";" ;
   if(count_filtered == 1) return is_equal(filter_move,play);

   cout << "Error: "<< SAN_arg << " - is'nt enougth information";
   return false;
}

bool board::load_user_custom_eval()
{
  eval_data custom;
  bool success = custom.load_from_configure_file(default_custom_eval_filename);

  if(success)
	  this->set_eval_data(custom);

  return success;
}
