#include <algorithm>
#include <iostream>
#include <sstream>

#include "board.h"
#include "uci_protocol.h"

//////////////////////////////////////////////////////////////////////////////////
//classical PVS. Checks,checkmates are checked.
//returning best line
//////////////////////////////////////////////////////////////////////////////////
int board::alphabeta_best_line(int depth,bool is_make_move,int alpha,int beta,bool is_pv,int mode)
{
   assert( alpha < beta ); //algorithm may enter infinite cycle when it happen

   m_control.checkup();
   if(m_control.is_stop_alerted())
	  return CHECK_INVALID;   

   //generates moves for current position
   generate_moves();
   if(m_store.is_alert_checked())
   {//if true then previous move was illegal and we return special flag bigger than any
	//other evaluation value met in the search to ensure that this move will be worse
	//then any other and no way any side will choose it
	   m_control.dec_from_nods(1);
       return (CHECK_INVALID); //check
   }

   //on check let's see one more ply
   bool check_to_turn_side = false;
   do_null_move_low();
   generate_moves();
   if(m_store.is_alert_checked())
   {
	     check_to_turn_side = true;
	     ++depth;
   }
   undo_null_move_low();

   //if reach terminal depth return static evaluation value

   if(depth<=0) 
   {
		  return forced_alphabeta(alpha,beta) ;
   }

   //cut by checkmate length
   int cut_mate = m_store.get_store_depth() - INFINITY_EVAL;
   if (alpha < cut_mate)
   {
	   if (beta <= cut_mate) return beta;
	   alpha = cut_mate;
   }
   if (beta > (-cut_mate))
   {
	   if (alpha >= (-cut_mate)) return alpha;
	   beta = (-cut_mate);
   }

   //hash table
   move_el *hash_best_move;
   restore_from_hash_table(alpha,beta,depth,hash_best_move);
   if(alpha == beta) 
	   return alpha;

   //sorting moves to ensure better alpha-beta cutting
   sort_moves_by_importance(is_pv,hash_best_move); //if IS_PV stay true we are still on principal variation

      ////null move , remove comments for NULL move
   if(IS_PV_NODE(mode)==false &&        //PV node will rarelly fail high in NULL move heuristics. So we gain ~ 30-50 Knodes/sec speed up.
	  IS_NULL_MOVE_ROOT(mode)==false &&
	  check_to_turn_side==false &&		  //we don't need this line for algorithm to work correctly. As is seems to be - leading to 
										  //illegal position. Because checking of legality (because of king capturing threat) is the first
										  //thing is checked in alphabeta. If so, CHECK_INVALID will be returned, transforming here to
										  //MINUS_CHECK_INVALID and this really big negative value by defenition can't high fall.
										  //but any way, it saves us some recalculation powers.
	  m_store.get_store_depth()>=1 && depth>=2 && //depth_stop-1 is better?
	  position_evaluation()>beta &&
	  is_zungzwang_danger() == false)
   {
	   do_null_move_low();
	   //it's a little bit tricky. 
	   //Lack of m_pv.init_at_node(m_store.get_store_depth()) code say don't build PV for Null Move
	   int null_res ;
	   if(depth > 3) 
		   null_res = - alphabeta_best_line(depth-3,false,-beta,1 - beta,false,MAKE_NULL_MOVE(mode));
	   else
		   null_res = - forced_alphabeta(-beta,1 - beta);
	   undo_null_move_low();

	   if(null_res >= beta) 
		   return null_res;
   }

   DISABLE_NULL_MOVE_ROOT(mode);
   
   unsigned int size = m_store.num_moves_this_depth();
   
   //if this flag remains false after cycling all the moves it must be
   //checkmate or stalemate
   bool any_legal_move = false;
   int old_alpha = alpha;
   scored_move_el best(NULL,alpha);

   for(unsigned int i = 0;i<size;i++)
   {
	   //CUR_MOVE will hold the current working move
       scored_move_el& cur_move  = m_store.get(i);
	   //make a move
       do_move_low(cur_move.get_move_el());

	   m_pv.init_at_node(m_store.get_store_depth());
	   if(m_store.is_hash_reps()) //if this move was already met in this variation then return a draw
		    cur_move.set_score(0);
	   else
	   {
		   if(!any_legal_move || !IS_PV_NODE(mode))
			cur_move.set_score( - alphabeta_best_line(depth-1,false,-beta,-alpha, i==0 ? is_pv : false,mode) ); 
		   else
		   {
			   cur_move.set_score( - alphabeta_best_line(depth-1,false,-alpha-1,-alpha, i==0 ? is_pv : false,MAKE_PVS_NULL(mode)) ); 
			   if(cur_move.get_score()>alpha)
				   cur_move.set_score( - alphabeta_best_line(depth-1,false,-beta,-alpha, i==0 ? is_pv : false,mode) ); 
		   }
	   }

	   //undo the move
       undo_move_low(cur_move.get_move_el());
	   //if there is any legal move(remember that if moves are illegal search return special flag)
	   //set a flag true for later checkmate checking
	   if(cur_move.get_score() != MINUS_CHECK_INVALID) any_legal_move = true;

	   //if this move is better then privious calculated on this depth
       if(cur_move.get_score()>alpha)
       {
           alpha = cur_move.get_score();
		   best = cur_move;

		   //can check for lock on null move variations
		   if(IS_PV_NODE(mode)) //can work also without this condition
		      m_pv.store(m_store.get_store_depth(),cur_move.get_move_el());

		   //on the root make some info to UCI interface
           if(is_make_move && m_control.is_stop_alerted() == false)
           {
			   if(m_control.is_communication_mode_enabled())
			   {
				   cout << "info score " ; uci::output_score(alpha,m_store.get_root_depth()) ; cout << " depth " << depth << " pv " ;
				   m_pv.to_console_current();cout <<endl;

				   cout << "info nodes " << m_control.get_nods_searched() << endl;
				   cout << "info nps " << m_control.get_nods_per_second()<< endl;
				   cout << "info time " << m_control.get_time_from_begin()/1000 << endl;
			   }

			   m_pv.add_to_container(cur_move.get_score(),depth);
           }
		   //alpha beta cutting
           if(alpha>=beta)  
			   {   
				   if((cur_move.get_move_el()->to->bit & not_turn_bit) == BIT(0))
				   {//this move is so good that we can store it to history and killer tables
					//only silent, non capture moves are stored in history and killer tables
					   	 if(depth>2) //don't fill the table with trash cuts from low depths
							 m_hh.add_good_move(cur_move.get_move_el(),depth); //depth>2
						 m_kh.add_good_move(cur_move,m_store.get_store_depth());
				   }
				   break;   
			   }
       }
   }
   
   m_control.add_to_nods(size);
   int ret_val = alpha;
   if(!any_legal_move) //check for checkmate
   {
	   if(check_to_turn_side)
			ret_val =  (-INFINITY_EVAL + m_store.get_store_depth());//checkmate
	   else
		    ret_val = 0;
   }
   else
   {
	   if(best.get_score() >= beta) //move found that fall high
		   store_in_hash_table(beta,depth,best.get_move_el(),bigger_eq);
	   else if(best.get_score() <= old_alpha)//has not risen at all
		   store_in_hash_table(old_alpha,depth,NULL,lower_eq);
	   else
		   store_in_hash_table(best.get_score(),depth,best.get_move_el(),inside);
   }

   //return the best score
   return ret_val;
}
