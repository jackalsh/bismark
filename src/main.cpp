
#include <iostream>
#include <iomanip>
#include <string>
#include <numeric>

using namespace std;

#include "uci_protocol.h"
#include "board.h"
#include "genetic.h"

board game;

//////////////console start///////////////////

bool single_perft(string fen,unsigned int depth,unsigned int theoretical)
{
	bool succeed = true;
	cout << "position: " << fen << endl;

	game.init_game_FEN(fen);
	unsigned long count = game.pf_search(depth);

	//nps calculation
	game.get_control_data().add_to_nods(theoretical);
	unsigned int nps = game.get_control_data().get_nods_per_second();

	cout << std::setw(5) << depth << setw(15) << count << setw(15) << theoretical << setw(15) << nps ;
	if(count == theoretical)
		cout << "    PASS" << endl;
	else
	{
		cout << "    FAIL" << endl;
		succeed = false;
		game.pf_search(depth,true); //on error make research with full info
	}

	cout << endl;
	return succeed;
}

bool single_perft(string fen, vector<unsigned int> theoretical)
{
	bool succeed = true;
	cout << "position: " << fen << endl;
	for(unsigned int depth = 1;depth<=theoretical.size();++depth)
	{
		game.init_game_FEN(fen);
		unsigned long count = game.pf_search(depth);

		//nps calculation
		for(unsigned int i = 1; i <= depth;++i)
			game.get_control_data().add_to_nods(theoretical.at(i-1));
		unsigned int nps = game.get_control_data().get_nods_per_second();

		cout << std::setw(5) << depth << setw(15) << count << setw(15) << theoretical.at(depth-1) << setw(15) << nps ;
		if(count == theoretical.at(depth-1))
			cout << "    PASS" << endl;
		else
		{
			cout << "    FAIL" << endl;
			succeed = false;
			game.pf_search(depth,true); //on error make research with full info

			break;
		}
	}
	cout << endl;
	return succeed;
}

//this is debug function to test perft of the engine
void auto_perft()
{
	cout << "beginning perft automatic test" << endl;
	cout << std::setw(5) << "depth" << setw(15) << "leafs" << setw(15) << "theoretical" << setw(15) << "nps" << endl;

	//test 1: initial position
	unsigned int t_1[]= {20,400,8902,197281,4865609,119060324/*,3195901860*/};
	single_perft("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",vector<unsigned int>(t_1,t_1+6));

	//test 2 position: testing king castling
	unsigned int t_2[]= {48,2039,97862,4085603,193690690};
	single_perft("r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -",vector<unsigned int>(t_2,t_2+5));		

	//test 3 position
	unsigned int t_3[]= {14,191,2812,43238,674624,11030083,178633661};
	single_perft("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -",vector<unsigned int>(t_3,t_3+7));	

	//test 4: white/black pieces symmetry
	unsigned int t_4[] = {6, 264, 9467, 422333, 15833292, 706045033 };
	single_perft("r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",vector<unsigned int>(t_4,t_4+5));	
	single_perft("r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",vector<unsigned int>(t_4,t_4+5));

	//test 5 position: testing promotions
	unsigned int t_5[]= {24,496,9483,182838,3605103,71179139};
	single_perft("n1n5/PPPk4/8/8/8/8/4Kppp/5N1N b - - 0 1",vector<unsigned int>(t_5,t_5+6));	

	//test 6 position
	unsigned int t_6[]= {50, 279};
	single_perft("8/3K4/2p5/p2b2r1/5k2/8/8/1q6 b - 1 67",vector<unsigned int>(t_6,t_6+2));

	//test 7 position
	single_perft("8/7p/p5pb/4k3/P1pPn3/8/P5PP/1rB2RK1 b - d3 0 28",6,38633283);

	//test 8 position
	single_perft("rnbqkb1r/ppppp1pp/7n/4Pp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3",5,11139762);

	//test 9 position
	single_perft("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -",6,11030083);
	single_perft("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -",7,178633661);

}

void main_loop()
{
 static unsigned int main_depth = 6;

 string input;bool flag = true;

 cout << "chess command line" << endl;
 //game.to_console();
 do
 {
     game.to_console();
	 cout << endl << ">>";
	 cin >> input;
	 if(input == "exit") flag = false;
	 if(input == "uci")
	 {
		 uci::uci_protocol p(game);
	 }
	 if(input == "move")
	 {
		 string temp_move;
		 cin >> temp_move;
		 int from_x = letter_to_file_index(temp_move[0]);
		 int from_y = letter_to_rank_index(temp_move[1]);
		 int to_x = letter_to_file_index(temp_move[2]);
		 int to_y = letter_to_rank_index(temp_move[3]);
		 game.move(from_x,from_y,to_x,to_y);
	 }
	 if(input == "eval")
	 {
             cout << "position evaluation :" << game.position_evaluation() << endl;
	 }
	 if(input == "ai")
	 {
		 game.fw_search_and_move(true,main_depth,INFINITE_SEARCH);
	 }
	 if(input == "info")
	 {
		cout << "Info:" << endl;
		cout << "main depth : " << main_depth  << endl;
		cout << "Available moves :"<<endl;
		game.print_available_moves();
		cout << "FEN: " << game.get_fen_notation()<<endl;
		cout << endl;
		game.to_console_info();
	 }         
	 if(input == "fen")
	 {
		 game.init_game_FEN("r3k2r/p1ppqNb1/bn2pnp1/3P4/1p2P3/2N2Q1p/PPPBBPPP/R3K2R b KQkq - 0 1 ");
		 //game.init_game_FEN("8/2P2P1K/8/8/8/8/8/k7 w - - 0 1");
		 
           //new_FEN_game(string("8/1k6/1N6/1QK5/8/8/8/8 w - - 0 1 "));
		   //string fen_str;
		   //getline(cin, fen_str);
		   //new_FEN_game(fen_str);
	 }    
	 if(input == "depth")
	 {
		 cin >> main_depth ;
	 }
	 if(input == "load")
	 {
		 unsigned int index;
		 cin >> index;
		 string filename = string("autotuning\\best_conf_")+int_to_string(index)+string(".txt");
		 if( game.load_from_configure_file(filename))
			 cout << "custom evaluation is loaded" << endl;
		 else
		 {
			 game.get_eval_data().init_default();
			 cout << "something went wrong while parsing the file" << endl;
			 cout << "initial evaluation is loaded" << endl;
		 }
	 }
	 if(input == "test")
	 {

		 genetic::evolution evol(game);
		 evol.user_input();
		 evol.evolution_cycle();
		 //eval_test();
		 //self_educated_battle();
	 }
	 if(input == "perft")
	 {
		 auto_perft();
	 }
	 
 }while(flag);

}

///////////////////////console end/////////////////////////////

int main()
{
  init();
  game.init_game();

  //main_loop();  //for pseudo graphical mode - can play in text mode. Can cause problem when connecting engine to Chessbase
  uci::uci_protocol p(game);
  
  return 0;
}