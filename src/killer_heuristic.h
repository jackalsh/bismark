
#include "basic.h"

#ifndef _KILLER_HEURISTIC
#define _KILLER_HEURISTIC

//////////////////////////////////////////////////////////////////
//This class handless all the killer heuristic routines
//
//////////////////////////////////////////////////////////////////
class killer_heuristic
{
private:
	move_el *first_killers[MAX_TREE_DEPTH]; //The first killer for each depth
	move_el *second_killers[MAX_TREE_DEPTH];//The second killer for each depth
public:
	void clear();
    void add_good_move(scored_move_el& move,unsigned int ply);
	bool is_first_killer(scored_move_el& move,unsigned int ply);
	bool is_second_killer(scored_move_el& move,unsigned int ply);

	void to_console_info();

	killer_heuristic(void);
};

#endif