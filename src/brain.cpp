
#include <iostream>
using namespace std;

#include "board.h"

///////////////////////////////////////////////////////////////////////////////////////////
//this function initializes all data for new search and begin it iteratively till depth
//MAIN_DEPTH and TIME_TO_THINK seconds. 
//if IS_COMMUNICATION enabled, engine will output data to GUI and check for GUI commands
///////////////////////////////////////////////////////////////////////////////////////////
int board::fw_search(bool is_communication,unsigned int main_depth,unsigned int time_to_think)
{
	assert (m_store.get_hist().get_hash() == calculate_hashkey_for_current_position());

        //initialize nods ant time counters
	m_control.init_new_search(time_to_think);
	if(is_communication)
		m_control.enable_communication_mode();
	else
		m_control.disable_communication_mode();

	unsigned int root_tree_depth = m_store.get_store_depth();


	m_hash.clear_table();
	m_hh.clear();
	m_kh.clear();

	//those structures need to know the relative root depth to correctly maintain and output the data
	m_store.set_this_depth_as_root();
	m_pv.init_new_search(root_tree_depth); 

	int score;
	line last_line;
	//begin iterative search
	for(unsigned int depth = 1;depth<=main_depth;depth++)
	{
		assert(m_store.get_store_depth() == root_tree_depth);

		m_pv.init_at_node(m_store.get_store_depth());

		//begin alpha-beta search with full window till depth i
		score = alphabeta_best_line(depth,true,-INFINITY_EVAL,INFINITY_EVAL,true,INITIAL);

		if(m_control.is_stop_alerted()) 
		{//TODO: more wise use of search
			break;
		}

		last_line = m_pv.get_last_line();
		m_pv.set_followed(last_line);
		if(depth >= 8 && m_control.is_communication_mode_enabled())
			cout << "info hashfull " << m_hash.get_fill_promill()<<endl;

		//on checkmate found don't iterate to the next depth 
		if(is_score_checkmate(score)) break;
	}
	return score;
}

///////////////////////////////////////////////////////////////////////////////////////////
//all interaction with search should be done throughout this function
//search for best move and plays it
///////////////////////////////////////////////////////////////////////////////////////////
int board::fw_search_and_move(bool is_communication,unsigned int main_depth,unsigned int time_to_think)
{
   //THE SEARCH
   int score = fw_search(is_communication,main_depth,time_to_think);

   if(m_pv.is_any_lines() == false ) return score;
   move_el* play = m_pv.get_last_line().get_first_move();
   
   if(play!=_NULL_EL)
   {
	//make it
	move(play); 
	if(m_control.is_communication_mode_enabled())
	{
		//some info to UCI interface
		cout << "info nodes " << m_control.get_nods_searched() << endl;
		cout << "info nps " << m_control.get_nods_per_second() << endl;
		cout << "info time " << m_control.get_time_from_begin()/1000 << endl;
		cout << "info hashfull " << m_hash.get_fill_promill()<<endl;
		cout << "bestmove " << *play << endl;
	}
   }
   return score;
}


///////////////////////////////////////////////////////////////////////////////////////////
//alphabeta search - only captures - till the end
///////////////////////////////////////////////////////////////////////////////////////////
int board::forced_alphabeta(int alpha,int beta)
{
   int cur_pos_eval =  position_evaluation();
   if (cur_pos_eval >= beta)
	  return beta;
   if (cur_pos_eval > alpha)
	  alpha = cur_pos_eval;

   generate_moves_captures_only();
   if(m_store.is_alert_checked())
   {
       m_control.dec_from_nods(1);
       return (CHECK_INVALID);
   }
   sort_moves_by_importance_captures_only();
   
   unsigned int size = m_store.num_moves_this_depth();

   for(unsigned int i = 0;i<size;i++)
   {
       scored_move_el& cur_move  = m_store.get(i);
       
       do_move_low(cur_move.get_move_el());
       
       cur_move.set_score( - forced_alphabeta(-beta,-alpha) );

       undo_move_low(cur_move.get_move_el());

       if(cur_move.get_score()>alpha)
       {
           alpha = cur_move.get_score();
           if(alpha>=beta) break;
       }
   }
   
   m_control.add_to_nods(size);

   return alpha;
}