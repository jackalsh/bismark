#ifndef PIECE_H
#define	PIECE_H

#include "basic.h"

//////////////////////////////////////////////////////////////////////////////////////////
//This class encapsulates all information for the chess piece object
//////////////////////////////////////////////////////////////////////////////////////////

class piece
{
private:
  int info;       //info stores data about color and dignity of piece
  coord *position;//pointer to current position. On case of NULL piece is
                  //deactivated so IS_NO_PIECE() will return true.
                  //SET_NO_PIECE() set piece in deactivated mode
  
  bool is_moved_at_past_flag;
  coord* get_position();
public:
 piece();
 piece(int color,int digni,coord* pos);
 piece(int color,int digni,int x,int y);

 void set_color(int color);
 void set_digni(int digni);
 void set(int color,int digni);
 void set_no_piece();

 bool is_color(int color);
 bool is_digni(int digni);
 bool is(int color,int digni);
 bool is_no_piece();

 int get_color();
 int get_digni();

 void set_moved_at_past_flag(bool flag) {is_moved_at_past_flag = flag;}
 void swap_moved_at_past_flag(){is_moved_at_past_flag = !is_moved_at_past_flag;}
 bool is_moved_at_past() {return is_moved_at_past_flag;}
 void enable_moved_at_past_flag() { is_moved_at_past_flag = true;} //for critical performance
 void disable_moved_at_past_flag() { is_moved_at_past_flag = false;}

 BIT get_bit_position();
 int get_x_position();
 int get_y_position();

 void set_position(coord* pos);

 void to_console();
 void to_console_chess_position();

 int get_ID();
};

//according to engine's philosophy, weights for pieces are inside eval_data class object,
//so they can be initialized to any value. But sometimes we need use those weights to 
//use for sorting. To make this sorting independent from current weights (nodes cutting
//will be the same for all of them) we use absolute scale for weights in the next way
//PAWN = 1
//KNIGHT = 3
//BISHOP = 3
//ROOK = 5
//QUEEN = 9
//KING = 0
int get_absolute_value(int ID);


#endif	/* PIECE_H */

