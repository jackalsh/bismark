#include <sys/timeb.h>

#include "control.h"
#include "uci_protocol.h"

unsigned int control::get_ms()
{
 	timeb time_buffer;
 	ftime(&time_buffer);
 	return (unsigned int)(time_buffer.time * 1000 + time_buffer.millitm);
}

control::control()
{
   	nods_searched = 0;
   	begin_time = 0;
   	end_think_time = 0;
   	stop_now_flag = false;
   	disable_communication_mode();
}

//by default communication with the GUI is disabled. Enable it by ENABLE_COMMUNICATION_MODE()
void control::init_new_search(unsigned int time_to_think) //0 is infinite (in seconds)
{
   nods_searched = 0;
   begin_time = get_ms();
   end_think_time = begin_time + time_to_think;
   stop_now_flag = false;
   disable_communication_mode();
}

void control::checkup()
{
	if(nods_searched % 1024) return;

	//if time is up also stop_now_flag = true, only if not infinite;

	if(begin_time != end_think_time) //not on infinite time control
		if(get_ms()>=end_think_time)
			stop_now_flag = true;

	//check for GUI commands only when communication is enabled
	if(is_communication_mode_enabled())
		if(uci::is_search_interrupted())
			stop_now_flag = true;
}

unsigned int control::get_nods_per_second()
{
	unsigned int now_ms = get_ms();
	if(now_ms == begin_time) return 0; //divide by zero

	return(( nods_searched / (now_ms - begin_time))*1000);
} 

unsigned int control::get_time_from_begin()
{
	return ((get_ms() - begin_time));
}

void control::disable_communication_mode()
{
	communication_search_mode = false;
}

void control::enable_communication_mode()
{
	communication_search_mode = true;
}

bool control::is_communication_mode_enabled()
{
	return communication_search_mode;
}