#if defined(_WIN32) || defined(_WIN64)
	#include <Windows.h>
#elif defined(__unix__) || defined(unix) || defined(__unix)
	#include <unistd.h>
#endif

#include <iostream>
#include <stdlib.h>


#include "basic.h"
#include "uci_protocol.h"

using namespace std;

uci::uci_protocol::uci_protocol( board& board_arg ):board_ptr(&board_arg)
{
	cycle();
}

void uci::uci_protocol::output_identification()
{
	cout << "id name " << uci::engine_name << " v. " << uci::engine_version << endl;
	cout << "id author " <<  uci::author_name << endl;
}

void uci::uci_protocol::output_options()
{
	cout << "option name hash type spin default " << DEFAULT_HASH_BITSIZE << " min 0 max 64" << endl;
}

void uci::uci_protocol::output_uciok()
{
	cout << "uciok" << endl;
}

void uci::uci_protocol::output_readyok()
{
	cout << "readyok" << endl;
}

void uci::uci_protocol::quit_program()
{
	exit(0);
}

void uci::uci_protocol::input_ucinewgame()
{
	//TODO:?
}

void uci::uci_protocol::input_position_startpos(const string& input_str)
{
	board_ptr->init_game();
	if(get_Nth_word(input_str,3) == "moves")
	{
		for(int i = 4;i<=get_N_of_words(input_str);i++)
		{
			string temp = get_Nth_word(input_str,i);
			int from_x = letter_to_file_index(temp[0]);
			int from_y = letter_to_rank_index(temp[1]);
			int to_x = letter_to_file_index(temp[2]);
			int to_y = letter_to_rank_index(temp[3]);
			board_ptr->move(from_x,from_y,to_x,to_y);
		}
	}
	board_ptr->clear_disabled_pieces();
}

void uci::uci_protocol::input_position_fen(const string& input_str)
{
	size_t start = input_str.find("fen")+4;
	size_t len = input_str.length()-start-1;
	//cout << input_str.substr(start,len);
	board_ptr->init_game_FEN(input_str.substr(start,len));

	if(get_Nth_word(input_str,9) == "moves")
	{
		for(int i = 10;i<=get_N_of_words(input_str);i++)
		{
			string temp = get_Nth_word(input_str,i);
			int from_x = letter_to_file_index(temp[0]);
			int from_y = letter_to_rank_index(temp[1]);
			int to_x = letter_to_file_index(temp[2]);
			int to_y = letter_to_rank_index(temp[3]);
			board_ptr->move(from_x,from_y,to_x,to_y);
		}
	}
	board_ptr->clear_disabled_pieces();
}

void uci::uci_protocol::input_go(const string& input_str)
{
	unsigned int main_depth = MAX_TREE_DEPTH; //quite infinite search
	unsigned int movetime  = INFINITE_SEARCH;
	unsigned int wtime = INFINITE_SEARCH;
	unsigned int btime = INFINITE_SEARCH;
	unsigned int winc = 0;
	unsigned int binc = 0;
	bool is_infinite_search = false;
	unsigned int moves_to_go = 0;

	for(int i = 2;i<=get_N_of_words(input_str);i++)
	{
		if(get_Nth_word(input_str,i) == "wtime" )
			wtime = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "btime")
			btime = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "winc" )
			winc = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "binc")
			binc = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "movetime")
			movetime = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "movestogo")
			moves_to_go = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "depth")
			main_depth = string_to_int(get_Nth_word(input_str,i+1) );
		if(get_Nth_word(input_str,i) == "infinite")
			is_infinite_search = true;
	}

	if(is_infinite_search)
		board_ptr->fw_search_and_move(true,100,INFINITE_SEARCH);
	else
	{
		unsigned int player_time = (board_ptr->get_turn_color()==WHITE)? wtime:btime;
		unsigned int player_inc = (board_ptr->get_turn_color()==WHITE)? winc:binc;
		//now we have information about time parameters and we need some model
		//to answer the one simple question: how much time we need to search 

		unsigned int search_time_modelled;
		if(movetime != INFINITE_SEARCH) //user want constant time control
		{
			search_time_modelled = movetime;
		}
		else if(moves_to_go != 0) //user wants time per moves control
		{
			search_time_modelled = int(float(player_time)/float(moves_to_go))+player_inc;
		}
		else
		{ //suppose 40 moves
			search_time_modelled = int(float(player_time)/float(40))+player_inc;  //TODO: more wise choice
		}

		board_ptr->fw_search_and_move(true,main_depth,search_time_modelled);
	}
}

void uci::uci_protocol::input_options(const string& input_str)
{
	if(get_Nth_word(input_str,2) == "name" && get_Nth_word(input_str,3) == "hash" && get_Nth_word(input_str,4) == "value" )
		board_ptr->change_hash_size(string_to_int(get_Nth_word(input_str,5)));
	//if(get_Nth_word(input_str,2) == "name" && get_Nth_word(input_str,3) == "main_depth" && get_Nth_word(input_str,4) == "value" )
	//		main_depth =  string_to_int(get_Nth_word(input_str,5));
	//if(get_Nth_word(input_str,2) == "name" && get_Nth_word(input_str,3) == "sec_per_move" && get_Nth_word(input_str,4) == "value" )
	//		sec_per_move =  string_to_int(get_Nth_word(input_str,5));
}

void uci::uci_protocol::cycle_step()
{
	string input_str;
	getline(cin, input_str);
	if(input_str == "uci")
	{
		output_identification();
		output_options();

		output_uciok();
	}
	if(input_str == "isready")
	{
		output_readyok();
	}
	if(input_str == "quit")
	{
		quit_program();
	}
	if(get_Nth_word(input_str,1) == "ucinewgame") 
	{
		// searching on a game that is hasn't searched on before

	}
	if(get_Nth_word(input_str,1) == "position")
	{
		if(get_Nth_word(input_str,2) == "startpos")  
		{
			input_position_startpos(input_str);
		}
		else if(get_Nth_word(input_str,2) == "fen")
		{
			input_position_fen(input_str);
		}
		else
			cout << "error in uci format" << endl;
	}
	if(get_Nth_word(input_str,1) == "go")
	{
		input_go(input_str);
	}
	if(get_Nth_word(input_str,1) == "setoption")
	{
		input_options(input_str);
	}
	if(get_Nth_word(input_str,1) == "info") //it's not UCI command.
	{
		cout << "Info:" << endl;
		cout << "Available moves :"<<endl;
		board_ptr->print_available_moves();
		cout << "FEN: " << endl << board_ptr->get_fen_notation()<<endl;
		board_ptr->to_console();
		board_ptr->to_console_info();
	}
	if(get_Nth_word(input_str,1) == "eval") //it's not UCI command.
	{
		cout << "position evaluation :" << board_ptr->position_evaluation() << endl;
	}

}

void uci::uci_protocol::cycle()
{
	output_identification();
	while(true)
	{
		cycle_step();
	}
}

////////////////////////////////global functions///////////////////////////////////////

bool uci::is_input_available() {

#if defined(_WIN32) || defined(_WIN64)

	static bool init = false, is_pipe;
	static HANDLE stdin_h;
	DWORD val, error;
	bool UseDebug = false;

	// val = 0; // needed to make the compiler happy?

	// have a look at the "local" buffer first, *this time before init (no idea if it helps)*

	if (UseDebug && !init) printf("info string init=%d stdin->_cnt=%d\n",int(init),stdin->_cnt);

	if (stdin->_cnt > 0) return true; // HACK: assumes FILE internals

	// input init (only done once)

	if (!init) {

		init = true;

		stdin_h = GetStdHandle(STD_INPUT_HANDLE);

		if (UseDebug && (stdin_h == NULL || stdin_h == INVALID_HANDLE_VALUE)) {
			error = GetLastError();
			printf("info string GetStdHandle() failed, error=%d\n",error);
		}

		is_pipe = !GetConsoleMode(stdin_h,&val); // HACK: assumes pipe on failure

		if (UseDebug) printf("info string init=%d is_pipe=%d\n",int(init),int(is_pipe));

		if (UseDebug && is_pipe) { // GetConsoleMode() failed, everybody assumes pipe then
			error = GetLastError();
			printf("info string GetConsoleMode() failed, error=%d\n",error);
		}

		if (!is_pipe) {
			SetConsoleMode(stdin_h,val&~(ENABLE_MOUSE_INPUT|ENABLE_WINDOW_INPUT));
			FlushConsoleInputBuffer(stdin_h); // no idea if we can lose data doing this
		}
	}

	// different polling depending on input type
	// does this code work at all for pipes?

	if (is_pipe) {

		if (!PeekNamedPipe(stdin_h,NULL,0,NULL,&val ,NULL)) {

			if (UseDebug) { // PeekNamedPipe() failed, everybody assumes EOF then
				error = GetLastError();
				printf("info string PeekNamedPipe() failed, error=%d\n",error);
			}

			return true; // HACK: assumes EOF on failure
		}

		if (UseDebug && val < 0) printf("info string PeekNamedPipe(): val=%d\n",val);

		return val > 0; // != 0???

	} else {

		GetNumberOfConsoleInputEvents(stdin_h,&val);
		return val > 1; // no idea why 1
	}

	return false;

#else // assume POSIX

	int val;
	fd_set set[1];
	struct timeval time_val[1];

	FD_ZERO(set);
	FD_SET(STDIN_FILENO,set);

	time_val->tv_sec = 0;
	time_val->tv_usec = 0;

	val = select(STDIN_FILENO+1,set,NULL,NULL,time_val);
	/*if (val == -1 && errno != EINTR) {
	my_fatal("input_available(): select(): %s\n",strerror(errno));
	}*/

	return val > 0;

#endif
} 

bool uci::is_search_interrupted()
{
	if(uci::is_input_available())
	{
		string input_str;
		getline(cin, input_str);
		if(input_str == "stop")
			return true;
	}
	return false;
}


//this function makes correction to score and output it in UCI format
//If this is ordinary score, CP 123 is printed, if this score means mate,
//MATE 7 is printed
void uci::output_score(int score,unsigned int root_depth)
{
	int mate_tmp = (score<0)? -score:score;
	if(mate_tmp >= (INFINITY_EVAL-MAX_TREE_DEPTH))
	{   
		//if mate, get distance to mate in half plies
		mate_tmp = -mate_tmp+INFINITY_EVAL;

		//correct mate depth relative to root depth of the tree
		mate_tmp -= int(root_depth);

		//output UCI mate score
		std::cout << "mate ";
		if(score<0) 
			std::cout << "-";
		std::cout << int(mate_tmp/2) + mate_tmp%2;
	}
	else
		std::cout << "cp " << score;
}