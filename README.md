Here you can find source code for Chess engine Bismark I wrote in purpose of learning chess programming.

At time of writing, this chess engine is rated 2003 by CCRL 40/4 list.

I believe it have a very clear code, no hacks or some experimental heurisics. I tried to combine only well understood and proven techniques. 

*  PV search, hashing, all features you expect from modern chess engine
*  Stable game, all chess rules implemented.
*  Very fast move generator built on improved linked list techniques. No bitboard magics. This is better for learning and not as common as magic.
*  Integrated code for optimizing parameters with genetics algorithms. Create tournaments with thousand of Bismark clones with different parameters.